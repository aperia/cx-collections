import React from 'react';
import TextTruncateCharacter from './index';
import { act, screen } from '@testing-library/react';
import { queryByClass, renderMockStoreId } from 'app/test-utils';

describe('Test TextTruncateCharacter component', () => {
  it('Should have render UI with text', () => {
    const text = 'test-truncate-chracter';
    renderMockStoreId(<TextTruncateCharacter text={text} maxLength={30} />);
    const queryText = screen.queryByText(text);
    expect(queryText).toBeInTheDocument();
  });

  it('Should have render UI with moreText', () => {
    const text = 'test-truncate-chracter';
    const { wrapper } = renderMockStoreId(
      <TextTruncateCharacter text={text} maxLength={15} />
    );
    const queryText = screen.queryByText('txt_more');
    const queryClass = queryByClass(
      wrapper.container,
      'link text-decoration-none'
    );
    expect(queryClass).toBeInTheDocument();
    expect(queryText).toBeInTheDocument();
  });

  it('Should have render UI with lessText', () => {
    const text = 'test-truncate-chracter';
    const { wrapper } = renderMockStoreId(
      <TextTruncateCharacter text={text} maxLength={15} />
    );
    const queryTextMore = screen.queryByText('txt_more');
    const queryClass = queryByClass(
      wrapper.container,
      'link text-decoration-none'
    );
    act(() => {
      queryTextMore!.click();
    });
    const queryTextLess = screen.queryByText('txt_less');
    expect(queryClass).toBeInTheDocument();
    expect(queryTextLess).toBeInTheDocument();
  });

  it('Should have render UI with maxLength = 0', () => {
    const text = 'test-truncate-chracter';
    renderMockStoreId(<TextTruncateCharacter text={text} />);
    const queryText = screen.queryByText(text);
    expect(queryText).not.toBeInTheDocument();
  });

  it('Should have render UI with text = " " ', () => {
    const { wrapper } = renderMockStoreId(<TextTruncateCharacter />);
    const queryClass = queryByClass(
      wrapper.container,
      'link text-decoration-none'
    );
    expect(queryClass).not.toBeInTheDocument();
  });
});
