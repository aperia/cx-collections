import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React, { useState, useEffect, useCallback } from 'react';

export interface TextTruncateCharacterProps {
  text?: string;
  maxLength?: number;
  isShowMore?: boolean;
  textMore?: string;
  textLess?: string;
  id?: string;
  dataTestId?: string;
}

const TextTruncateCharacter: React.FC<TextTruncateCharacterProps> = ({
  text = '',
  maxLength = 0,
  isShowMore = false,
  textMore = 'txt_more',
  textLess = 'txt_less',
  id,
  dataTestId,
  ...props
}) => {
  const [show, setShow] = useState(false);
  const { t } = useTranslation();
  useEffect(() => {
    setShow(isShowMore);
  }, [isShowMore]);

  const onHandleShow = useCallback(() => {
    setShow(!show);
  }, [show]);

  const testId = genAmtId(dataTestId!, 'text-truncate-character', 'TextTruncateCharacter');
  const testIdLess = genAmtId(dataTestId!, 'text-truncate-character_less', 'TextTruncateCharacter');
  const testIdMore = genAmtId(dataTestId!, 'text-truncate-character_more', 'TextTruncateCharacter');

  const moreText = show ? (
    <React.Fragment>
      {text.trim()}
      <span
        className="link text-decoration-none"
        onClick={onHandleShow}
        data-testid={testIdLess}
      >
        {t(textLess)}
      </span>
    </React.Fragment>
  ) : (
    <React.Fragment>
      {`${text.trim().slice(0, maxLength)}...`}
      <span
        className="link text-decoration-none"
        onClick={onHandleShow}
        data-testid={testIdMore}
      >
        {t(textMore)}
      </span>
    </React.Fragment>
  );

  return (
    <span id={id} data-testid={testId}>
      {text.trim().length > maxLength ? moreText : text}
    </span>
  );
};

export default TextTruncateCharacter;
