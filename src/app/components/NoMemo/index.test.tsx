import React from 'react';
import { render, screen } from '@testing-library/react';
import NoMemo from './index';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test NoMemo', () => {
  it('render to UI', () => {
    render(<NoMemo />);
    expect(screen.getByText('txt_memos_no_memos_display')).toBeInTheDocument();
  });
});
