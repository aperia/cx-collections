import React from 'react';
import { Icon } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

const NoMemo: React.FC = props => {
  const { t } = useTranslation();

  return (
    <div
      className="mt-40 d-flex justify-content-center"
      data-testid={genAmtId('', 'no-memo', 'NoMemo')}
    >
      <div className="d-flex flex-column align-items-center">
        <Icon
          className="color-light-l12 fs-64"
          name="comment"
          dataTestId={'no-memo-icon'}
        />
        <p className="fs-14 color-grey mt-16">
          {t('txt_memos_no_memos_display')}
        </p>
      </div>
    </div>
  );
};

export default NoMemo;
