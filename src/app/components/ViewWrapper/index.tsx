import React from 'react';
import { View } from 'app/_libraries/_dof/core';
import isEqual from 'lodash.isequal';

export default React.memo(View, isEqual);
