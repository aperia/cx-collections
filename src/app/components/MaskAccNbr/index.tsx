import { formatMaskNumber } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';

export interface MaskAccNbrProps {
  accNbr: string;
  dataTestId?: string;
}

export const MaskAccNbr: React.FC<MaskAccNbrProps> = ({
  accNbr,
  dataTestId
}) => {
  const mask = formatMaskNumber(accNbr);

  if (!mask)
    return (
      <span
        className="form-group-static__text"
        data-testid={genAmtId(dataTestId, 'mask', 'MaskAccNbr')}
      />
    );

  return (
    <div
      className="data-value"
      data-testid={genAmtId(dataTestId, 'mask', 'MaskAccNbr')}
    >
      <span className="custom-account-number">
        {mask.firstText}
        <span className="hide-account-icon">{mask.maskText}</span>
        {mask.lastText}
      </span>
    </div>
  );
};
