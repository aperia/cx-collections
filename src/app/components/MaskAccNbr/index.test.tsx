import React from 'react';
import { MaskAccNbr } from './index';
import { render } from '@testing-library/react';
import { queryByClass } from 'app/test-utils';

describe('MaskAccNbr component', () => {
  it('should not show data', () => {
    const wrapper = render(<MaskAccNbr accNbr={''} />);
    expect(
      queryByClass(wrapper.container, 'form-group-static__text')
    ).toBeInTheDocument();
  });

  it('should show data', () => {
    const wrapper = render(<MaskAccNbr accNbr={'123***123'} />);
    const ele = queryByClass(wrapper.container, 'custom-account-number');

    expect(ele?.innerHTML).toEqual(
      '123<span class="hide-account-icon">···</span>123'
    );
  });
});
