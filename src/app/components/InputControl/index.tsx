import React from 'react';

import { InputControl as InputControlDLS } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';

const InputControl: React.FC<any> = ({
  placeholder,
  maxLength,
  mode,
  dataTestId,
  ...props
}) => {
  const { t } = useTranslation();
  return (
    <InputControlDLS
      dataTestId={dataTestId}
      placeholder={t(placeholder)}
      maxLength={maxLength}
      mode={mode}
      onRemoveField={null}
      onNeedFocusInputFilter={null}
      onSearch={null}
      {...props}
    />
  );
};

export default InputControl;
