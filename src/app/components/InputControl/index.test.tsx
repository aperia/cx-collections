import React from 'react';
import InputControl from './index';
import { render } from '@testing-library/react';

jest.mock('app/_libraries/_dls/hooks', () => ({
  ...jest.requireActual('app/_libraries/_dls/hooks'),
  useTranslation: () => ({ t: (text: string) => text })
}));

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test InputControl component', () => {
  it('render default', () => {
    const wrapper = render(<InputControl />);
    expect(wrapper.container.querySelector('input'));
  });

  it('render with full props', () => {
    const wrapper = render(
      <InputControl placeholder="Placeholder" maxLength="20" mode="text" />
    );

    expect(wrapper.container.innerHTML).toEqual(
      '<input maxlength="20" type="text" placeholder="Placeholder" class="input-control" value="">'
    );
  });
});
