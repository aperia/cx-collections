import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React, { useMemo } from 'react';

// images
import SrcImg from './blank-page.svg';

export interface NoResultPageProps {
  id?: string;
  imgUrl?: string;
  title?: string;
  description?: string;
  className?: string;
}

const NoResult: React.FC<NoResultPageProps> = ({
  id = 'No_Result',
  imgUrl,
  title,
  description,
  ...props
}) => {
  const { t } = useTranslation();
  const element = useMemo(() => {
    return (
      <div
        id={`${id}_results`}
        className="results-found"
        data-testid={genAmtId(id, 'no-result', 'NoResult')}
      >
        <img
          id={`${id}_img_`}
          src={imgUrl || SrcImg}
          title={title || t('txt_pass_title_prop_for_component')}
          alt={title}
        />
        <h4 className="text-results" id={`${id}_title_`}>
          {title || t('txt_no_results_found')}
        </h4>
        <p id={`${id}_description_`} className="text-helper">
          {description || t('txt_no_search_results_found')}
        </p>
      </div>
    );
  }, [description, id, imgUrl, t, title]);

  return element;
};

export default NoResult;
