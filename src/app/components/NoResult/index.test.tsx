import React from 'react';
import NoResult from './index';
import { screen } from '@testing-library/react';
import { queryAllBy, queryBy, renderMockStoreId } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text || '' });
});

describe('Test NoResult component', () => {
  it('Should have render UI default', () => {
    const { wrapper } = renderMockStoreId(<NoResult />);

    const queryTextNoFound = screen.queryByText('txt_no_results_found');
    const queryTextNoResultFound = screen.queryByText(
      'txt_no_search_results_found'
    );
    const queryClassPropId = queryAllBy(wrapper.container, 'id', /No_Result/);
    const queryTitleImg = queryBy(
      wrapper.container,
      'title',
      'txt_pass_title_prop_for_component'
    );
    const querySrcImg = queryBy(wrapper.container, 'src', 'blank-page.svg');

    expect(querySrcImg).toBeInTheDocument();
    expect(queryTitleImg).toBeInTheDocument();
    expect(queryTextNoFound).toBeInTheDocument();
    expect(queryTextNoResultFound).toBeInTheDocument();
    expect(queryClassPropId.length).toEqual(4);
  });

  it('Should have render with title, description, imgURL and id ', () => {
    const title = 'test-title-component';
    const description = 'test-description-component';
    const imgUrl = 'imgUrl';
    const { wrapper } = renderMockStoreId(
      <NoResult
        imgUrl={imgUrl}
        description={description}
        title={title}
        id="test-no-result"
      />
    );

    const queryTextNoFound = screen.getByText(title);
    const queryTextNoResultFound = screen.queryByText(description);
    const queryClassPropId = queryAllBy(
      wrapper.container,
      'id',
      /test-no-result/
    );
    const queryTitleImg = queryBy(wrapper.container, 'title', title);
    const querySrcImg = queryBy(wrapper.container, 'src', imgUrl);

    expect(querySrcImg).toBeInTheDocument();
    expect(queryTitleImg).toBeInTheDocument();
    expect(queryTextNoFound).toBeInTheDocument();
    expect(queryTextNoResultFound).toBeInTheDocument();
    expect(queryClassPropId.length).toEqual(4);
  });
});
