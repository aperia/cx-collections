import React, { FC } from 'react';
import { useField } from 'formik';
import pickBy from 'lodash.pickby';
import { isUndefined } from 'app/_libraries/_dls/lodash';
import classNames from 'classnames';
import { NumericProps, NumericTextBox } from 'app/_libraries/_dls/components';

export interface FormikNumericControlProps extends NumericProps {
  name: string;
  forceError?: boolean;
}

export const FormikNumericControl: FC<FormikNumericControlProps> = ({
  className,
  name,
  forceError,
  ...props
}) => {
  const [field, { error, touched }] = useField(name);

  const errorTooltipProps = {
    opened: !forceError ? undefined : !!error && forceError
  };

  return (
    <div className={classNames(className)}>
      <NumericTextBox
        error={
          (!props.disabled && props.required && touched && error) || forceError
            ? {
                message: error,
                status: !!error || forceError
              }
            : undefined
        }
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
        {...field}
        {...props}
      />
    </div>
  );
};
