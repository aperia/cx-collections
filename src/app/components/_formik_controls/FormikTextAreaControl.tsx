import React, { FC } from 'react';
import { useField } from 'formik';
import classNames from 'classnames';
import { TextArea, TextAreaProps } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface FormikTextAreaControlProps extends TextAreaProps {
  name: string;
  maxLength: number;
  disabledEnter?: boolean;
}

export const FormikTextAreaControl: FC<FormikTextAreaControlProps> = ({
  className,
  name,
  maxLength,
  disabledEnter,
  ...props
}) => {
  const { t } = useTranslation();
  const [field, { error, touched }] = useField(name);

  const handleOnKeyPress: React.KeyboardEventHandler<HTMLTextAreaElement> =
    e => {
      if (e.key === 'Enter' && disabledEnter) e.preventDefault();
    };

  return (
    <div className={classNames(className)}>
      <TextArea
        maxLength={maxLength}
        className="resize-none h-100"
        onKeyPress={handleOnKeyPress}
        {...field}
        {...props}
        error={
          !props.disabled && props.required && touched && error
            ? {
                message: error,
                status: !!error
              }
            : undefined
        }
      />
      <p className="color-grey-l24 fs-12 mt-8">
        {t('txt_character_left', {
          count: maxLength - (field.value?.length || 0)
        })}
      </p>
    </div>
  );
};
