import React, { FC } from 'react';
import { useField } from 'formik';
import classNames from 'classnames';
import TimePicker from 'app/_libraries/_dls/components/TimePicker';
import {
  TimePickerChangeEvent,
  TimePickerProps,
  TimePickerValue
} from 'app/_libraries/_dls/components/TimePicker/types';

export interface FormikTimePickerControlProps extends TimePickerProps {
  name: string;
  className?: string;
  forceError?: boolean;
  changeCallback?: (time?: TimePickerValue) => void;
}

export const FormikTimePickerControl: FC<FormikTimePickerControlProps> = ({
  className,
  name,
  ...props
}) => {
  const [field, { error, touched }] = useField(name);
  const { changeCallback, ...restProps } = props;
  const { onBlur, ...restFields } = field;

  const handleBlur = (e: TimePickerChangeEvent) => {
    onBlur(e);
    if (changeCallback) {
      changeCallback(e.target.value);
    }
  };

  return (
    <div className={classNames(className)}>
      <TimePicker
        {...restProps}
        {...restFields}
        onBlur={handleBlur}
        error={
          !props.disabled && touched && error
            ? {
                message: error,
                status: !!error
              }
            : undefined
        }
      />
    </div>
  );
};
