import React, { FC, useMemo } from 'react';
import { useField } from 'formik';
import classNames from 'classnames';
import pickBy from 'lodash.pickby';
import { isUndefined } from 'app/_libraries/_dls/lodash';
import {
  DropdownList,
  DropdownListProps
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface FormikDropdownControlProps
  extends Omit<DropdownListProps, 'children'> {
  name: string;
  forceError?: boolean;
  data: RefDataValue[];
}

export const FormikDropdownControl: FC<FormikDropdownControlProps> = ({
  className,
  name,
  forceError,
  data: dataProp,
  ...props
}) => {
  const { t } = useTranslation();
  const [field, { error, touched }] = useField(name);

  const errorTooltipProps = {
    opened: !forceError ? undefined : false
  };

  const data = useMemo(
    () =>
      dataProp.map(item => ({
        ...item,
        description: t(item.description)
      })),
    [dataProp, t]
  );

  return (
    <div className={classNames(className)}>
      <DropdownList
        textField={'description'}
        popupBaseProps={{
          popupBaseClassName: 'inside-infobar'
        }}
        error={
          (props.required && touched && error) || forceError
            ? {
                message: error,
                status: !!error || forceError
              }
            : undefined
        }
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
        {...field}
        value={
          data.length
            ? data.find(val => val?.value === field.value?.value)
            : field.value
        }
        {...props}
        noResult={t('txt_no_results_found')}
      >
        {data.map((item, index) => {
          const dropDownLbl = item.description;
          return (
            <DropdownList.Item value={item} label={dropDownLbl} key={index} />
          );
        })}
      </DropdownList>
    </div>
  );
};
