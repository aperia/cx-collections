import React, { FC } from 'react';
import { useField } from 'formik';
import dateTime from 'date-and-time';
import classNames from 'classnames';
import { isUndefined } from 'app/_libraries/_dls/lodash';
import pickBy from 'lodash.pickby';
import {
  DatePicker,
  DatePickerProps,
  Tooltip
} from 'app/_libraries/_dls/components';

export interface FormikDatePickerControlProps extends DatePickerProps {
  name: string;
  forceError?: boolean;
  minDaysFromToday?: number;
  maxDaysFromToday?: number;
  pastDateDisabledText?: string;
  minDateDisabledText?: string;
  maxDateDisabledText?: string;
}

export const FormikDatePickerControl: FC<FormikDatePickerControlProps> = ({
  className,
  name,
  forceError,
  minDaysFromToday,
  maxDaysFromToday,
  pastDateDisabledText,
  minDateDisabledText,
  maxDateDisabledText,
  ...props
}) => {
  const [field, { error, touched }] = useField(name);

  const errorTooltipProps = {
    opened: !forceError ? undefined : false
  };

  const today = (function () {
    const toDay = new Date();
    toDay.setHours(0, 0, 0, 0);
    return toDay;
  })();

  const minDate = (function () {
    if (minDaysFromToday === undefined) return undefined;
    const returnDate = dateTime.addDays(new Date(), minDaysFromToday);
    returnDate.setHours(0, 0, 0, 0);
    return returnDate;
  })();

  const maxDate = (function () {
    if (maxDaysFromToday === undefined) return undefined;
    const returnDate = dateTime.addDays(new Date(), maxDaysFromToday - 1);
    returnDate.setHours(0, 0, 0, 0);
    return returnDate;
  })();

  const handleDateTooltip = (date: Date) => {
    if (pastDateDisabledText && date < today) {
      return <Tooltip element={pastDateDisabledText} />;
    }
    if (minDaysFromToday && minDate && date < minDate && minDateDisabledText) {
      return <Tooltip element={minDateDisabledText} />;
    }
    if (maxDate && date > maxDate && maxDateDisabledText) {
      return <Tooltip element={maxDateDisabledText} />;
    }
    return undefined;
  };

  return (
    <div className={classNames(className)}>
      <DatePicker
        error={
          (!props.disabled && props.required && touched && error) || forceError
            ? {
                message: error,
                status: !!error || forceError
              }
            : undefined
        }
        popupBaseProps={{
          popupBaseClassName: 'inside-infobar'
        }}
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
        minDate={minDate}
        maxDate={maxDate}
        dateTooltip={handleDateTooltip}
        {...field}
        {...props}
      />
    </div>
  );
};
