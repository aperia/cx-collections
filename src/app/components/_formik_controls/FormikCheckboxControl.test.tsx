import React from 'react';
import { render, screen } from '@testing-library/react';
import { FormikCheckboxControl } from './FormikCheckboxControl';
import { FormikContainer } from 'app/test-utils';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mName = 'checkbox';

describe('Test Formik Checkbox Control', () => {
  it('render', () => {
    render(
      <FormikContainer initialValues={{ [mName]: undefined }}>
        <FormikCheckboxControl name={mName} id={mName} label={mName} />
      </FormikContainer>
    );
    expect(screen.getByText(mName)).toBeInTheDocument();
  });
});
