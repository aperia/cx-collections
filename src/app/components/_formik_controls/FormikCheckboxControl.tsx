import React, { FC } from 'react';
import { useField } from 'formik';
import classNames from 'classnames';
import { CheckBox, CheckBoxProps } from 'app/_libraries/_dls/components';

export interface FormikCheckboxControlProps
  extends Omit<CheckBoxProps, 'children' | 'ref'> {
  name: string;
  label: string;
  status: boolean;
}

export const FormikCheckboxControl: FC<FormikCheckboxControlProps> = ({
  className,
  name,
  label,
  status = false,
  dataTestId,
  ...props
}) => {
  const [field] = useField(name);

  return (
    <div className={classNames(className)}>
      <CheckBox dataTestId={dataTestId}>
        <CheckBox.Input
          readOnly={status}
          checked={field.value}
          {...props}
          {...field}
        />
        <CheckBox.Label>{label}</CheckBox.Label>
      </CheckBox>
    </div>
  );
};
