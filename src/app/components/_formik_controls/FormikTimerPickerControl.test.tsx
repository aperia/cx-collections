import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { FormikTimePickerControl } from './FormikTimePickerControl';
import { FormikContainer } from 'app/test-utils';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mName = 'timerPicker';

describe('Test Formik Text Area Control', () => {
  it('render', () => {
    render(
      <FormikContainer initialValues={{ [mName]: {} }}>
        <FormikTimePickerControl name={mName} id={mName} label={mName} />
      </FormikContainer>
    );
    expect(screen.getByText(mName)).toBeInTheDocument();
  });

  it('render error', () => {
    const { container } = render(
      <FormikContainer
        initialValues={{ [mName]: {} }}
        validate={() => {
          return { [mName]: 'error' };
        }}
      >
        <FormikTimePickerControl name={mName} id={mName} label={mName} />
      </FormikContainer>
    );
    jest.useFakeTimers();
    const input = container.querySelector('input');
    input!.click();
    fireEvent.blur(input!);
    fireEvent.focus(input!);
    jest.runAllTimers();
    expect(screen.getByText(mName)).toBeInTheDocument();
  });

  it('has changeCallback function', () => {
    const changeCallback = jest.fn();
    const { container } = render(
      <FormikContainer
        initialValues={{ [mName]: {} }}
        validate={() => {
          return { [mName]: 'error' };
        }}
      >
        <FormikTimePickerControl
          changeCallback={changeCallback}
          name={mName}
          id={mName}
          label={mName}
        />
      </FormikContainer>
    );
    jest.useFakeTimers();
    const input = container.querySelector('input');
    input!.click();
    fireEvent.blur(input!);
    jest.runAllTimers();
    expect(changeCallback).toBeCalled();
  });
});
