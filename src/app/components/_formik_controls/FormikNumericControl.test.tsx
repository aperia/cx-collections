import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { FormikNumericControl } from './FormikNumericControl';
import { FormikContainer, queryByClass } from 'app/test-utils';
import * as yup from 'yup';
import userEvent from '@testing-library/user-event';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mName = 'numeric';
const mErrorMsg = 'errorMsg';

const validationSchema = yup
  .object()
  .shape({ [mName]: yup.number().min(5, mErrorMsg) });

describe('Test Formik Numeric Control', () => {
  it('render', () => {
    render(
      <FormikContainer initialValues={{ [mName]: undefined }}>
        <FormikNumericControl name={mName} id={mName} label={mName} />
      </FormikContainer>
    );
    expect(screen.getByText(mName)).toBeInTheDocument();
  });

  it('force Error', () => {
    const { container } = render(
      <FormikContainer initialValues={{ [mName]: undefined }}>
        <FormikNumericControl
          name={mName}
          id={mName}
          label={mName}
          forceError
        />
      </FormikContainer>
    );
    expect(queryByClass(container, /icon-error/)).toBeInTheDocument();
  });

  it('render error message', () => {
    const { container } = render(
      <FormikContainer
        initialValues={{ [mName]: undefined }}
        validationSchema={validationSchema}
      >
        <FormikNumericControl
          name={mName}
          id={mName}
          required
          label={mName}
          forceError
        />
      </FormikContainer>
    );
    const input = screen.getByText(mName);
    jest.useFakeTimers();
    userEvent.click(input);
    userEvent.type(input, '3');
    fireEvent.blur(input);
    fireEvent.focus(input);
    jest.runAllTimers();
    expect(queryByClass(container, /icon-error/)).toBeInTheDocument();
  });
});
