import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { FormikTextAreaControl } from './FormikTextAreaControl';
import { FormikContainer } from 'app/test-utils';
import * as yup from 'yup';
import userEvent from '@testing-library/user-event';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mName = 'textArea';

const validationSchema = yup.object().shape({
  [mName]: yup
    .string()
    .matches(/^[a-z]+$/)
    .required()
});

describe('Test Formik Text Area Control', () => {
  it('render', () => {
    render(
      <FormikContainer initialValues={{ [mName]: undefined }}>
        <FormikTextAreaControl
          name={mName}
          id={mName}
          label={mName}
          maxLength={20}
        />
      </FormikContainer>
    );
    const input = screen.getByRole('textbox');
    fireEvent.keyPress(input, { key: 'Enter', charCode: 13 });
    expect(screen.getByText(mName)).toBeInTheDocument();
  });

  it('render error message', () => {
    render(
      <FormikContainer
        initialValues={{ [mName]: '' }}
        validationSchema={validationSchema}
      >
        <FormikTextAreaControl
          name={mName}
          id={mName}
          required
          disabledEnter
          label={mName}
          maxLength={20}
        />
      </FormikContainer>
    );
    const input = screen.getByRole('textbox');
    jest.useFakeTimers();
    userEvent.click(input);
    jest.runAllTimers();
    fireEvent.change(input, { target: { value: '0' } });
    fireEvent.keyPress(input, { key: 'Enter', charCode: 13 });
    userEvent.click(input);
    fireEvent.blur(input);

    fireEvent.focus(input);

    jest.runAllTimers();
    expect(screen.getByText(mName)).toBeInTheDocument();
  });
});
