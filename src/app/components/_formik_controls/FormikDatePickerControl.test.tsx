import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { FormikDatePickerControl } from './FormikDatePickerControl';
import { FormikContainer, queryByClass } from 'app/test-utils';
import * as yup from 'yup';
import mockDate from 'app/_libraries/_dls/test-utils/mockDate';

import 'app/_libraries/_dls/test-utils/mocks/mockInnerText';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mName = 'numeric';
const mErrorMsg = 'errorMsg';

const todayString = '05/12/2021';

const today = new Date(todayString);

beforeEach(() => {
  jest.useFakeTimers();
  mockDate.set(today);
});

const validationSchema = yup
  .object()
  .shape({ [mName]: yup.date().required(mErrorMsg) });

describe('Test Formik Numeric Control', () => {
  it('render', () => {
    render(
      <FormikContainer initialValues={{ [mName]: undefined }}>
        <FormikDatePickerControl name={mName} id={mName} label={mName} />
      </FormikContainer>
    );
    expect(screen.getByText(mName)).toBeInTheDocument();
  });

  it('force Error', () => {
    const { container } = render(
      <FormikContainer initialValues={{ [mName]: undefined }}>
        <FormikDatePickerControl
          name={mName}
          id={mName}
          label={mName}
          forceError
        />
      </FormikContainer>
    );
    expect(queryByClass(container, /icon-error/)).toBeInTheDocument();
  });

  it('render error message', () => {
    const { container } = render(
      <FormikContainer
        initialValues={{ [mName]: undefined }}
        validationSchema={validationSchema}
        touched={{ [mName]: true }}
      >
        <FormikDatePickerControl
          name={mName}
          id={mName}
          required
          label={mName}
          forceError
        />
      </FormikContainer>
    );
    jest.runAllTimers();
    const input = container.querySelector('input');
    fireEvent.blur(input!);
    jest.runAllTimers();
    expect(queryByClass(container, /icon-error/)).toBeInTheDocument();
  });

  it('has min max date', () => {
    const MIN_DATE = 2;
    const MAX_DATE = 10;
    render(
      <FormikContainer
        initialValues={{ [mName]: undefined }}
        validationSchema={validationSchema}
      >
        <FormikDatePickerControl
          name={mName}
          id={mName}
          required
          label={mName}
          opened
          value={today}
          maxDaysFromToday={MAX_DATE}
          minDaysFromToday={MIN_DATE}
          minDateDisabledText="mockText"
          maxDateDisabledText="mockText"
        />
      </FormikContainer>
    );
    jest.runAllTimers();
    expect(screen.getByText(mName)).toBeInTheDocument();
  });

  it('has disable past date', () => {
    render(
      <FormikContainer
        initialValues={{ [mName]: undefined }}
        validationSchema={validationSchema}
      >
        <FormikDatePickerControl
          name={mName}
          id={mName}
          required
          label={mName}
          opened
          value={today}
          minDaysFromToday={0}
          pastDateDisabledText="mockText"
        />
      </FormikContainer>
    );
    jest.runAllTimers();
    expect(screen.getByText(mName)).toBeInTheDocument();
  });
});
