import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { FormikDropdownControl } from './FormikDropdownControl';
import { FormikContainer, queryByClass } from 'app/test-utils';
import * as yup from 'yup';
import userEvent from '@testing-library/user-event';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mName = 'dropDown';
const mErrorMsg = 'errorMsg';
const mData = [
  { value: '1', description: '1' },
  { value: '2', description: '2' }
];

const validationSchema = yup
  .object()
  .shape({ [mName]: yup.object().required(mErrorMsg) });

describe('Test Formik Dropdown Control', () => {
  it('render', () => {
    render(
      <FormikContainer initialValues={{ [mName]: undefined }}>
        <FormikDropdownControl
          data={mData}
          name={mName}
          id={mName}
          label={mName}
        />
      </FormikContainer>
    );
    expect(screen.getByText(mName)).toBeInTheDocument();
  });

  it('empty referent data', () => {
    render(
      <FormikContainer initialValues={{ [mName]: undefined }}>
        <FormikDropdownControl
          data={[]}
          name={mName}
          id={mName}
          label={mName}
        />
      </FormikContainer>
    );
    expect(screen.getByText(mName)).toBeInTheDocument();
  });

  it('force Error', () => {
    const { container } = render(
      <FormikContainer initialValues={{ [mName]: undefined }}>
        <FormikDropdownControl
          data={mData}
          name={mName}
          id={mName}
          label={mName}
          forceError
        />
      </FormikContainer>
    );
    expect(queryByClass(container, /icon-error/)).toBeInTheDocument();
  });

  it('render error message', () => {
    const { container } = render(
      <FormikContainer
        initialValues={{ [mName]: undefined }}
        validationSchema={validationSchema}
      >
        <FormikDropdownControl
          name={mName}
          id={mName}
          required
          data={mData}
          label={mName}
          forceError
        />
      </FormikContainer>
    );
    const input = screen.getByText(mName);
    jest.useFakeTimers();
    userEvent.click(input);
    fireEvent.blur(input);
    fireEvent.focus(input);
    jest.runAllTimers();
    expect(queryByClass(container, /icon-error/)).toBeInTheDocument();
  });
});
