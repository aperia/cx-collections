import React, {
  Fragment,
  useCallback,
  useLayoutEffect,
  useMemo,
  useRef,
  useState
} from 'react';

// component
import {
  Bubble,
  Button,
  Icon,
  Modal,
  ModalBody,
  ModalHeader
} from 'app/_libraries/_dls/components';
import DropdownEntitlement from 'pages/ConfigurationEntitlement/Components/DropdownEntitlement';
import ReportingNavigation from 'pages/Reporting';
import ExceptionHandlingNavigation from 'pages/ExceptionHandling';
import ClientConfigurationNavigation from 'pages/ClientConfiguration';
import TransactionAdjustment from 'pages/TransactionAdjustment';
import QueueRoleMappingNavigation from 'pages/ClientConfiguration/QueueAndRoleMapping/QueueRoleMappingNavigation';
import { Slide } from 'app/_libraries/_dls/components/Animations';

//hook
import classNames from 'classnames';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch } from 'react-redux';
import { tabActions } from 'pages/__commons/TabBar/_redux';

// helper
import { genAmtId } from 'app/_libraries/_dls/utils';
import { ChangeHistoryNavigation } from 'pages/ClientConfiguration/ChangeHistory/Navigation';
import { ModalTitle } from 'react-bootstrap';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { userSessionWarningActions } from 'pages/UserSessionWarning/_redux/reducer';
import DialerAgentLogin from '../DialerAgentLogin';
import LanguageDropdown from 'pages/__commons/LanguageDropdown';

interface HeaderProps {
  dataTestId?: string;
}

const Header: React.FC<HeaderProps> = ({ dataTestId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [isOpenModal, setOpenModal] = useState(false);
  const [isShowHamburger, setShowHamburger] = useState(false);
  const [isToggle, setToggle] = useState(false);

  const [widthWindow, setWidthWindow] = useState(0);

  const navigationRef = useRef<HTMLDivElement | null>(null);
  const modalRef = useRef<HTMLDivElement | null>(null);

  const {
    isAdminRole: isAdmin,
    isSupervisor: isSuperVisor,
    essUserName = ''
  } = window.appConfig?.commonConfig || {};

  const handleOnClickLogo = (
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>
  ) => {
    event.preventDefault();
    dispatch(tabActions.selectTab({ storeId: 'home' }));
  };

  const handleOpenModal = () => {
    setOpenModal(true);
    setToggle(false);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleLogout = useCallback(() => {
    dispatch(
      confirmModalActions.open({
        title: 'txt_confirm_log_out_title',
        body: 'txt_confirm_log_out_description',
        cancelText: 'txt_cancel',
        confirmText: 'txt_log_out',
        confirmCallback: () => {
          dispatch(userSessionWarningActions.setDisableBeforeUnload(true));
          // wait for update store
          process.nextTick(() => {
            sessionStorage.clear();
            location.replace(window.appConfig?.commonConfig?.loginURL);
          });
        },
        variant: 'primary',
        size: {
          sm: true
        }
      })
    );
  }, [dispatch]);

  const changeLanguageCallback = useCallback(() => {
    setImmediate(() => {
      const widthHeader =
        navigationRef.current && navigationRef.current.scrollWidth + 212;
      setWidthWindow(window.innerWidth);

      if (widthHeader! > widthWindow) {
        setShowHamburger(true);
      } else {
        setShowHamburger(false);
      }
    });
  }, [widthWindow]);

  const headerNavigation = useMemo(() => {
    return (
      <>
        {isAdmin && (
          <Fragment>
            <DropdownEntitlement />
            <ChangeHistoryNavigation />
            <ClientConfigurationNavigation />
            <ExceptionHandlingNavigation />
            <QueueRoleMappingNavigation />
          </Fragment>
        )}
        <ReportingNavigation />
        {(isSuperVisor || isAdmin) && <TransactionAdjustment />}
        <div className="d-flex align-items-center border-left ml-24 pl-16 mr-n8">
          <span
            className={classNames('mr-16', {
              'd-none': isShowHamburger
            })}
          >
            <LanguageDropdown onChange={changeLanguageCallback} />
          </span>
          <Bubble small name={essUserName} />
          <span className="bubble-name">{essUserName}</span>
          <Button
            onClick={handleLogout}
            className="ml-16"
            variant="outline-primary"
            size="sm"
          >
            {t('txt_log_out')}
          </Button>
        </div>
        <div className="d-flex align-items-center border-left ml-24 pl-16 mr-n8">
          <DialerAgentLogin></DialerAgentLogin>
        </div>
      </>
    );
  }, [
    changeLanguageCallback,
    essUserName,
    handleLogout,
    isAdmin,
    isShowHamburger,
    isSuperVisor,
    t
  ]);

  const hamburgerNavigation = useMemo(() => {
    const handleToggle = () => {
      setToggle(!isToggle);
    };
    const handleUserRoleTab = () => {
      handleCloseModal();
      dispatch(
        tabActions.addTab({
          id: 'User-Role-Entitlement',
          title: 'txt_user_role_entitlement',
          storeId: 'User-Role-Entitlement',
          tabType: 'userRoleEntitlement',
          iconName: 'file'
        })
      );
    };

    const handleContactAndAccountTab = () => {
      handleCloseModal();
      dispatch(
        tabActions.addTab({
          id: 'Contact-And-Account-Entitlement',
          title: 'txt_contact_account_entitlement',
          storeId: 'Contact-And-Account-Entitlement',
          tabType: 'contactAndAccountEntitlement',
          iconName: 'file'
        })
      );
    };
    return (
      <>
        {isAdmin && (
          <div
            className={classNames('accordion mt-24', {
              'accordion-collapse': isToggle
            })}
          >
            <div onClick={handleToggle} className="accordion-title">
              {t('txt_entitlement')}
            </div>
            <div className="accordion-toggle">
              <div className="nav-item">
                <span
                  onClick={handleContactAndAccountTab}
                  className="link-header"
                >
                  {t('txt_contact_account_entitlement')}
                </span>
              </div>
              <div className="nav-item">
                <span onClick={handleUserRoleTab} className="link-header">
                  {t('txt_user_role_entitlement')}
                </span>
              </div>
            </div>
          </div>
        )}
        {isAdmin && (
          <div className="nav-item" onClick={handleCloseModal}>
            <ChangeHistoryNavigation />
          </div>
        )}
        <div className="nav-item" onClick={handleCloseModal}>
          <ReportingNavigation />
        </div>
        {isAdmin && (
          <Fragment>
            <div className="nav-item" onClick={handleCloseModal}>
              <ExceptionHandlingNavigation />
            </div>
            <div className="nav-item" onClick={handleCloseModal}>
              <QueueRoleMappingNavigation />
            </div>
            <div className="nav-item" onClick={handleCloseModal}>
              <ClientConfigurationNavigation />
            </div>
          </Fragment>
        )}
        {(isSuperVisor || isAdmin) && (
          <div className="nav-item" onClick={handleCloseModal}>
            <TransactionAdjustment />
          </div>
        )}
        <div className="mt-28 d-flex align-items-center flex-wrap">
          <Button
            onClick={handleLogout}
            variant="outline-primary"
            className="ml-n8"
            size="sm"
          >
            {t('txt_log_out')}
          </Button>
          <div className="ml-auto">
            <LanguageDropdown onChange={changeLanguageCallback} />
          </div>
        </div>
        <div className="mt-28">
          <DialerAgentLogin></DialerAgentLogin>
        </div>
      </>
    );
  }, [
    changeLanguageCallback,
    dispatch,
    handleLogout,
    isAdmin,
    isSuperVisor,
    isToggle,
    t
  ]);

  useLayoutEffect(() => {
    const handleResize = () => {
      const widthHeader =
        navigationRef.current && navigationRef.current.scrollWidth + 212;
      setWidthWindow(window.innerWidth);

      if (widthHeader! > widthWindow) {
        setShowHamburger(true);
      } else {
        setShowHamburger(false);
      }
    };


    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [widthWindow, isShowHamburger]);

  useLayoutEffect(() => {
    const handleClickOutsideModal = (event: MouseEvent) => {
      if (
        isOpenModal &&
        modalRef.current &&
        !modalRef.current.contains(event.target as Node)
      ) {
        setOpenModal(false);
      }
    };

    window.addEventListener('mousedown', handleClickOutsideModal);

    return () => {
      window.removeEventListener('mousedown', handleClickOutsideModal);
    };
  }, [isOpenModal]);

  return (
    <>
      <header className="header" id="header-component">
        <a
          className="cursor-pointer"
          onClick={handleOnClickLogo}
          data-testid={genAmtId(`${dataTestId}-header`, 'logo', 'Header')}
        >
          <img src="images/remedy.svg" alt="Collections" />
        </a>
        <div>
          <div
            className={classNames('header-hamburger', {
              invisible: !isShowHamburger
            })}
          >
            <Icon
              onClick={handleOpenModal}
              name="drag"
              dataTestId={dataTestId}
            ></Icon>
          </div>
          <div
            ref={navigationRef}
            className={classNames('header-navigation', {
              invisible: isShowHamburger
            })}
          >
            {!!headerNavigation && headerNavigation}
          </div>
        </div>
      </header>
      <Modal
        dataTestId={genAmtId(`${dataTestId}-header`, 'modal', 'Modal')}
        ref={modalRef}
        rt
        show={isOpenModal}
        animationComponent={Slide}
        animationComponentProps={{ direction: 'left' }}
        className="modal-hamburger"
      >
        <ModalHeader
          dataTestId={genAmtId(`${dataTestId}-header`, 'modal-header', 'Modal')}
          closeButton
          onHide={handleCloseModal}
        >
          <ModalTitle>
            <Bubble small name={essUserName} />
            <span className="fs-14 fw-400 ml-8 mr-24">{essUserName}</span>
          </ModalTitle>
        </ModalHeader>

        <ModalBody
          dataTestId={genAmtId(`${dataTestId}-header`, 'modal-body', 'Modal')}
        >
          <div className="navigation-hamburger">
            {!!hamburgerNavigation && hamburgerNavigation}
          </div>
        </ModalBody>
      </Modal>
    </>
  );
};

export default Header;
