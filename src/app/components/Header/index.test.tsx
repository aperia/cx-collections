import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { mockInnerWidth } from 'app/test-utils/mocks/mockProperty';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import React from 'react';
import { act } from 'react-dom/test-utils';
import Header from './index';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});
jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);
jest.mock('pages/__commons/LanguageDropdown', () => ({
  __esModule: true,
  default: (props: { onChange: () => void }) => (
    <div data-testid="LanguageDropdown" onClick={() => props.onChange()}>
      LanguageDropdown
    </div>
  )
}));

const tabActionsMock = mockActionCreator(tabActions);

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {};

const logoDataTestId = 'undefined-header_logo';

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<Header />, {
    initialState
  });
};

const userClick = (element: Element) => {
  act(() => {
    userEvent.click(element);
  });
  jest.runAllTimers();
};

const adminConfig = () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      isAdminRole: true
    } as CommonConfig
  };
};

const collectorConfig = () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      isCollector: true
    } as CommonConfig
  };
};

describe('Test Header component', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('render default > not isAdmin', () => {
    renderWrapper(initialState);
    expect(screen.getByTestId(logoDataTestId)).toBeInTheDocument();
  });

  it('render default > isAdmin', () => {
    jest.useFakeTimers();
    adminConfig();
    const { wrapper } = renderWrapper({
      ...initialState
    });
    const element = wrapper.container.querySelector(
      'i[class="icon icon-drag"]'
    );
    userClick(element as HTMLElement);

    const toggleElement = screen.getAllByText('txt_entitlement');

    const userRoleBtn = screen.getByText(/txt_user_role_entitlement/);
    userClick(userRoleBtn as HTMLElement);
    userClick(toggleElement[0] as HTMLElement);
    expect(toggleElement[0]).toBeInTheDocument();
  });

  it('render default > not isAdmin & isShowHamburger & click icon hamburger to open modal', () => {
    collectorConfig();
    renderWrapper(initialState);
    expect(screen.getByTestId(logoDataTestId)).toBeInTheDocument();
  });

  it('should render with non navigationRef value', () => {
    renderWrapper(initialState);

    expect(screen.getByTestId(logoDataTestId)).toBeInTheDocument();
  });

  it('should render with non modalRef value', () => {
    renderWrapper(initialState);
    expect(screen.getByTestId(logoDataTestId)).toBeInTheDocument();
  });
});

describe('Test event', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render with handleClickOutsideModal ', () => {
    const { wrapper, baseElement } = renderWrapper(initialState);
    const element = wrapper.container.querySelector(
      'i[class="icon icon-drag"]'
    );
    userClick(element as HTMLElement);
    fireEvent.mouseDown(baseElement as HTMLElement);
    expect(element).toBeInTheDocument();
  });

  it('should render with handleOnClickLogo ', () => {
    renderWrapper(initialState);
    userClick(screen.getByTestId(logoDataTestId) as HTMLElement);
  });

  it('should be logout ', () => {
    const originalLocation = window.location;
    const mockLocationReplace = jest.fn();
    const mockLocation = new URL('https://google.com');
    (mockLocation as any).replace = mockLocationReplace;
    delete (window as any).location;
    (window as any).location = {
      replace: mockLocationReplace,
      assign: jest.fn()
    };
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    renderWrapper(initialState);
    const logoutBtn = screen.getByText('txt_log_out');
    logoutBtn.click();
    jest.runAllTimers();
    expect(mockLocationReplace).toBeCalled();
    window.location = originalLocation;
  });

  it('render default > isAdmin and handleContactAndAccountTab', () => {
    jest.useFakeTimers();
    adminConfig();

    const addTabAction = tabActionsMock('addTab');
    const { wrapper } = renderWrapper({
      ...initialState
    });
    const element = wrapper.container.querySelector(
      'i[class="icon icon-drag"]'
    );

    userClick(element as HTMLElement);
    const toggleElement = screen.getAllByText('txt_entitlement');
    userClick(toggleElement[1] as HTMLElement);
    const accountContactElement = screen.getByText(
      /txt_contact_account_entitlement/
    );
    userClick(accountContactElement as HTMLElement);
    expect(addTabAction).toBeCalledWith({
      id: 'Contact-And-Account-Entitlement',
      title: 'txt_contact_account_entitlement',
      storeId: 'Contact-And-Account-Entitlement',
      tabType: 'contactAndAccountEntitlement',
      iconName: 'file'
    });
  });

  it('should render with change LanguageDropdown', () => {
    renderWrapper(initialState);
    userClick(screen.getByTestId('LanguageDropdown') as HTMLElement);

    expect(screen.getByText('LanguageDropdown')).toBeInTheDocument();
  });

  it('should render with change LanguageDropdown condition widthHeader > widthWindow', () => {
    renderWrapper(initialState);

    act(() => {
      mockInnerWidth(720);
      window.dispatchEvent(new Event('resize'));
    });

    act(() => {
      mockInnerWidth(1920);
      window.dispatchEvent(new Event('resize'));
    });
    userClick(screen.getByTestId('LanguageDropdown') as HTMLElement);

    expect(screen.getByText('LanguageDropdown')).toBeInTheDocument();
  });
});
