import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';

export interface AddSectionProps {
  id: string;
  text: string;
  dataTestId?: string;
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

const AddSection: React.FC<AddSectionProps> = ({
  id,
  dataTestId,
  text,
  onClick
}) => {
  const { t } = useTranslation();
  const handleOnClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    if (onClick) onClick(event);
  };

  if (!text) return null;
  const testId = genAmtId(dataTestId!, 'add-section', 'AddSection');

  return (
    <div
      id={id}
      data-testid={testId}
      className="py-18 px-24 bg-light-l20 border-bottom cursor-pointer"
      onClick={handleOnClick}
    >
      <span className="color-grey-l32 fs-14">{t(text)}</span>
    </div>
  );
};

export default AddSection;
