import React from 'react';
import AddSection from './index';
import { fireEvent, render, screen } from '@testing-library/react';

describe('Test AddSection component', () => {
  it('not render component', () => {
    const wrapper = render(<AddSection id="id" text="" />);

    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('render component', () => {
    const wrapper = render(<AddSection id="id" text="Text Test" />);

    const divElement = wrapper.container.querySelector('div[id="id"]');
    fireEvent.click(divElement!);

    expect(screen.getByText('Text Test')).toBeInTheDocument();
  });

  it('on click having function', () => {
    const onClickSpy = jest.fn();

    const wrapper = render(
      <AddSection id="id" text="Text Test" onClick={onClickSpy} />
    );

    const divElement = wrapper.container.querySelector('div[id="id"]');
    fireEvent.click(divElement!);

    expect(onClickSpy).toBeCalled();
  });
});
