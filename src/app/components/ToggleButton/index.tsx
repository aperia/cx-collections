import React from 'react';

// types
import isFunction from 'lodash.isfunction';

// components
import { Icon, Tooltip } from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface ToggleButtonProps {
  expandMessage?: string;
  collapseMessage?: string;
  collapsed?: boolean;
  dataTestId?: string;
  onToggleCollapsed?: (collapsed: boolean) => void;
}

const ToggleButton: React.FC<ToggleButtonProps> = ({
  expandMessage,
  collapseMessage,
  collapsed,
  dataTestId,
  onToggleCollapsed
}) => {
  const handleClick = () => {
    isFunction(onToggleCollapsed) && onToggleCollapsed(!collapsed);
  };

  return (
    <span
      className="collapse-button"
      onClick={handleClick}
      data-testid={genAmtId(dataTestId!, 'push-right', '')}
    >
      <Tooltip
        triggerClassName="d-flex"
        variant="primary"
        placement="left"
        element={collapsed ? expandMessage : collapseMessage}
        dataTestId={genAmtId(dataTestId!, 'push-right-info', '')}
      >
        <Icon
          name={collapsed ? 'push-left' : 'push-right'}
          dataTestId={dataTestId}
        />
      </Tooltip>
    </span>
  );
};
export default ToggleButton;
