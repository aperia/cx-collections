import React from 'react';
import ToggleButton from './index';
import { screen } from '@testing-library/react';
import { queryByClass, renderMockStoreId } from 'app/test-utils';
import userEvent from '@testing-library/user-event';

describe('Test ToggleButton component', () => {
  it('Should have expandMessage prop', () => {
    const expandMessage = 'test-expand-message';
    const { wrapper } = renderMockStoreId(
      <ToggleButton expandMessage={expandMessage} collapsed />
    );
    const queryClassTrigger = queryByClass(
      wrapper.container,
      /dls-popper-trigger/
    );
    userEvent.hover(queryClassTrigger!);
    const queryTextExpand = screen.queryByText(expandMessage);
    const queryIcon = queryByClass(wrapper.container, /icon icon-push-left/);
    expect(queryTextExpand).toBeInTheDocument();
    expect(queryIcon).toBeInTheDocument();
    userEvent.unhover(queryClassTrigger!);
  });

  it('Should have collapseMessage prop', () => {
    const collapseMessage = 'test-collapse-message';
    const { wrapper } = renderMockStoreId(
      <ToggleButton collapseMessage={collapseMessage} />
    );
    const queryClassTrigger = queryByClass(
      wrapper.container,
      /dls-popper-trigger/
    );
    userEvent.hover(queryClassTrigger!);
    const queryTextExpand = screen.queryByText(collapseMessage);
    const queryIcon = queryByClass(wrapper.container, /icon icon-push-right/);
    expect(queryTextExpand).toBeInTheDocument();
    expect(queryIcon).toBeInTheDocument();
    userEvent.unhover(queryClassTrigger!);
  });

  it('Should have onToggleCollapsed', () => {
    const onToggleCollapsed = jest.fn();
    const { wrapper } = renderMockStoreId(
      <ToggleButton onToggleCollapsed={onToggleCollapsed} />
    );
    const queryIcon = queryByClass(wrapper.container, /icon icon-push-right/);
    queryIcon!.click();
    expect(onToggleCollapsed).toHaveBeenCalled();
  });
});
