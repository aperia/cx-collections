import React from 'react';
import MoreAction from './index';
import { act } from '@testing-library/react';
import {
  queryAllByClass,
  queryByClass,
  renderMockStoreId
} from 'app/test-utils';
import userEvent from '@testing-library/user-event';

const actions = [
  {
    label: 'Test More Action 1',
    uniqueId: 'ID Test More Action 1',
    disabled: false
  },
  {
    label: 'Test More Action 2',
    uniqueId: 'ID Test More Action 2',
    disabled: false
  }
];
describe('Test MoreAction', () => {
  it('should have onSelect', () => {
    const onSelect = jest.fn();
    const { wrapper } = renderMockStoreId(
      <MoreAction onSelect={onSelect} actions={actions} />
    );
    const queryClassDLSPopper = queryByClass(
      wrapper.container,
      /icon icon-more-vertical/
    );
    act(() => {
      userEvent.click(queryClassDLSPopper!);
    });
    const queryTextActions = queryAllByClass(
      wrapper.baseElement as HTMLElement,
      'item'
    );
    queryTextActions[0].click();
    expect(onSelect).toHaveBeenCalledWith(actions[0]);
  });

  it('render UI with empty action', () => {
    const { wrapper } = renderMockStoreId(<MoreAction actions={[]} />);
    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('render UI with one action', () => {
    const { wrapper } = renderMockStoreId(
      <MoreAction
        onSelect={jest.fn()}
        actions={[
          {
            label: 'button',
            uniqueId: 'ID Test More Action 1',
            disabled: false
          }
        ]}
      />
    );

    const queryIconClose = queryByClass(
      wrapper.baseElement as HTMLElement,
      /btn btn-icon-secondary/
    );
    queryIconClose!.click();

    expect(queryIconClose).toBeInTheDocument();
  });
});
