import React from 'react';
import Label from './index';
import { render } from '@testing-library/react';
import { queryByClass } from 'app/test-utils';

describe('Test Label component', () => {
  it('render component', () => {
    const wrapper = render(<Label id="id" label="Label" value="value" />);

    expect(
      wrapper.container.querySelector('span[class="form-group-static__label"]')
        ?.innerHTML
    ).toEqual('Label');
    expect(queryByClass(wrapper.container, /d-flex/)?.innerHTML).toEqual(
      '<span id="id" class="fs-14">value</span>'
    );
  });
});
