import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';

export interface LabelProps {
  label: string;
  value: string;
  id?: string;
  dataTestId?: string;
}

const Label: React.FC<LabelProps> = ({ label, value, id, dataTestId }) => {
  return (
    <div>
      <span
        className="form-group-static__label"
        data-testid={genAmtId(dataTestId!, 'label', 'Label')}
      >
        {label}
      </span>
      <div className="d-flex">
        <span
          id={id}
          className="fs-14"
          data-testid={genAmtId(dataTestId!, 'value', 'Label')}
        >
          {value}
        </span>
      </div>
    </div>
  );
};
export default Label;
