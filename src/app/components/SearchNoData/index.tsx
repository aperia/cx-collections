import { Button, Icon, IconProps } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';

export interface SearchNodataProps {
  iconName?: IconProps['name'];
  text?: React.ReactNode;
  children?: React.ReactNode;
  dataTestId?: string;
  onClearSearch: () => void;
}

export const SearchNodata: React.FC<SearchNodataProps> = ({
  iconName = 'file',
  text = 'txt_no_search_results_found',
  dataTestId,
  onClearSearch
}) => {
  const { t } = useTranslation();
  return (
    <div
      className="block-no-data"
      data-testid={genAmtId(dataTestId, 'search-no-data-grid', 'SearchNodata')}
    >
      <Icon
        name={iconName}
        className="fs-80 color-light-l12"
        dataTestId={dataTestId}
      />
      <p
        className="mt-20 color-grey"
        data-testid={genAmtId(
          dataTestId,
          'search-no-data-grid_text',
          'SearchNodata'
        )}
      >
        {t(text)}
      </p>
      <div className="mt-24">
        <Button
          size="sm"
          variant="outline-primary"
          onClick={onClearSearch}
          dataTestId={`${dataTestId}_addConfigBtn`}
        >
          {t('txt_clear_and_reset')}
        </Button>
      </div>
    </div>
  );
};
