import { screen } from '@testing-library/react';
import { renderMockStoreId } from 'app/test-utils';
import React from 'react';
import { SearchNodata } from './index';

describe('Test SearchNodata component', () => {
  const testId = 'searchNoData';
  it('Should have render UI default', () => {
    renderMockStoreId(
      <SearchNodata dataTestId={testId} onClearSearch={jest.fn()} />
    );
    expect(screen.getByText('txt_no_search_results_found')).toBeInTheDocument();
  });
});
