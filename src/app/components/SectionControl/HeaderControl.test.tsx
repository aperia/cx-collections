import React from 'react';
import HeaderControl from './HeaderControl';
import { screen } from '@testing-library/react';
import { cardholderMaintenanceActions } from 'pages/AccountDetails/CardHolderMaintenance/_redux';
import {
  mockActionCreator,
  queryAllByClass,
  queryByClass,
  renderMockStoreId,
  storeId
} from 'app/test-utils';
import userEvent from '@testing-library/user-event';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { act } from '@testing-library/react';

const mockActionCardholder = mockActionCreator(cardholderMaintenanceActions);

describe('HeaderControl component', () => {
  it('should have tooltips expand', () => {
    const dropdownButton = {
      tooltipText: 'text',
      tooltipTriggerClassName: '',
      items: [{ text: 'text', value: 'value' }],
      onSelect: () => {}
    };
    const { wrapper } = renderMockStoreId(
      <HeaderControl title="title" dropdownButton={dropdownButton} />
    );
    const queryIcon = queryByClass(wrapper.container, /icon icon-plus/);
    userEvent.hover(queryIcon!);
    const queryTextExpand = screen.queryByText(I18N_COMMON_TEXT.EXPAND);
    expect(queryTextExpand).toBeInTheDocument();
    userEvent.unhover(queryIcon!);
  });

  it('should have tooltips collapse', () => {
    const dropdownButton = {
      tooltipText: 'text',
      tooltipTriggerClassName: '',
      items: [{ text: 'text', value: 'value' }],
      onSelect: () => {}
    };

    const { wrapper } = renderMockStoreId(
      <HeaderControl title="title" isExpand dropdownButton={dropdownButton} />
    );
    const queryIcon = queryByClass(wrapper.container, /icon icon-minus/);
    userEvent.hover(queryIcon!);
    const queryTextExpand = screen.queryByText(I18N_COMMON_TEXT.COLLAPSE);
    expect(queryTextExpand).toBeInTheDocument();
    userEvent.unhover(queryIcon!);
  });

  it('should have data tooltip of dropdownButton', () => {
    const dropdownButton = {
      tooltipText: 'text',
      tooltipTriggerClassName: '',
      items: [{ text: 'text', value: 'value' }],
      onSelect: () => {}
    };

    const { wrapper } = renderMockStoreId(
      <HeaderControl title="title" isExpand dropdownButton={dropdownButton} />
    );
    const queryIcon = queryByClass(
      wrapper.container,
      /icon icon-more-vertical/
    );
    userEvent.hover(queryIcon!);
    const queryTextExpand = screen.queryByText(dropdownButton.tooltipText);
    expect(queryTextExpand).toBeInTheDocument();
    userEvent.unhover(queryIcon!);
  });

  it('should have with items dropdownButton', () => {
    const dropdownButton = {
      tooltipText: 'text',
      tooltipTriggerClassName: '',
      items: [{ text: 'text', value: 'value' }],
      onSelect: jest.fn()
    };

    const { wrapper } = renderMockStoreId(
      <HeaderControl title="title" isExpand dropdownButton={dropdownButton} />
    );
    const queryIcon = queryByClass(
      wrapper.container,
      /icon icon-more-vertical/
    );

    act(() => {
      userEvent.click(queryIcon!);
    });

    const queryDropdownBase = queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-dropdown-base-container/
    );
    const queryItem = queryAllByClass(queryDropdownBase!, /item/);
    queryItem[0].click();
    expect(queryItem.length).toEqual(dropdownButton.items.length);
    expect(dropdownButton.onSelect).toHaveBeenCalled();
  });

  it('should have without items dropdownButton', () => {
    const dropdownButton = {
      tooltipText: 'text',
      tooltipTriggerClassName: '',
      onSelect: () => {}
    };

    const { wrapper } = renderMockStoreId(
      <HeaderControl title="title" isExpand dropdownButton={dropdownButton} />
    );
    const queryIcon = queryByClass(
      wrapper.container,
      /icon icon-more-vertical/
    );

    act(() => {
      userEvent.click(queryIcon!);
    });

    const queryDropdownBase = queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-dropdown-base-container/
    );
    const queryItem = queryAllByClass(queryDropdownBase!, /item/);
    expect(queryItem).toEqual([]);
  });

  it('should have with isEdit', () => {
    const textTooltipEdit = 'text';
    const typeEdit = 'test';
    const spyUpdateModalEdit = mockActionCardholder('updateModalEdit');
    const { wrapper } = renderMockStoreId(
      <HeaderControl
        title="title"
        isEdit
        textTooltipEdit={textTooltipEdit}
        typeEdit={typeEdit}
      />
    );

    const queryIcon = queryByClass(wrapper.container, /icon icon-edit/);

    userEvent.hover(queryIcon!);
    const queryTextTooltip = screen.queryByText(textTooltipEdit);
    expect(queryTextTooltip).toBeInTheDocument();
    userEvent.unhover(queryIcon!);

    act(() => {
      userEvent.click(queryIcon!);
    });

    expect(spyUpdateModalEdit).toHaveBeenCalledWith({
      storeId,
      open: true,
      type: typeEdit
    });
  });

  it('should have with isAdd', () => {
    const isAdd = {
      handleAdd: jest.fn(),
      buttonLabel: 'test',
      enableToolTip: true
    };
    const title = 'title';
    const textTooltipAdd = 'textTooltipAdd';

    renderMockStoreId(
      <HeaderControl
        textTooltipAdd={textTooltipAdd}
        title={title}
        isAdd={isAdd}
      />
    );

    const queryTextTitle = screen.queryByText(title);
    const queryButton = screen.queryByText(isAdd.buttonLabel);
    queryButton!.click();
    expect(isAdd.handleAdd).toHaveBeenCalled();
    expect(queryTextTitle).toBeInTheDocument();

    userEvent.hover(queryButton!);
    const queryTextTooltip = screen.queryByText(textTooltipAdd);
    expect(queryTextTooltip).toBeInTheDocument();
    userEvent.unhover(queryButton!);
  });

  it('should have with isAdd', () => {
    const isAdd = {
      handleAdd: jest.fn(),
      buttonLabel: 'test',
      enableToolTip: false,
      enableSectionButton: true
    };
    const title = 'title';
    const textTooltipAdd = 'textTooltipAdd';

    const { wrapper } = renderMockStoreId(
      <HeaderControl
        textTooltipAdd={textTooltipAdd}
        title={title}
        isAdd={isAdd}
      />
    );

    const queryTextTitle = screen.queryByText(title);
    const queryClass = queryByClass(wrapper.container, 'fw-500');
    const queryButton = screen.queryByText(isAdd.buttonLabel);
    queryButton!.click();
    expect(isAdd.handleAdd).toHaveBeenCalled();
    expect(queryTextTitle).toBeInTheDocument();
    expect(queryClass).toBeInTheDocument();
  });
});
