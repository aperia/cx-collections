import React from 'react';
import SectionControl from './index';
import { screen } from '@testing-library/react';
import { queryByClass, renderMockStoreId } from 'app/test-utils';
import { act } from '@testing-library/react';

describe('Test SectionControl component', () => {
  it('Should have render UI with text', () => {
    const text = 'test-section-control';
    const { wrapper } = renderMockStoreId(
      <SectionControl title={text} sectionKey={text} />
    );
    const queryText = screen.queryByText(text);
    const queryIcon = queryByClass(wrapper.container, /icon icon-plus/);
    expect(queryIcon).toBeInTheDocument();
    expect(queryText).toBeInTheDocument();
  });

  it('Should have onToggle', () => {
    const onToggle = jest.fn();
    const text = 'test-section-control';
    const { wrapper } = renderMockStoreId(
      <SectionControl title={text} onToggle={onToggle} sectionKey={text} />
    );
    const queryIconPlus = queryByClass(wrapper.container, /icon icon-plus/);
    queryIconPlus!.click();
    expect(onToggle).toHaveBeenCalledWith(false, text);
  });

  it('Should have onChange state isExpand', () => {
    const text = 'test-section-control';
    const { wrapper } = renderMockStoreId(
      <SectionControl title={text} sectionKey={text} />
    );
    const queryIconPlus = queryByClass(wrapper.container, /icon icon-plus/);
    act(() => {
      queryIconPlus!.click();
    });
    const queryIconMinus = queryByClass(wrapper.container, /icon icon-minus/);
    expect(queryIconMinus).toBeInTheDocument();
  });
});
