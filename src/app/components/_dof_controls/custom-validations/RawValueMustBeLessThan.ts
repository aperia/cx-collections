import {
  Validator,
  ValidationContext,
  ValidationOptions
} from 'app/_libraries/_dof/core';

interface RawValueMustBeLessThanValidatorOptions extends ValidationOptions {
  otherField?: string;
}

interface ValueProps {
  enable?: boolean;
  rawValue?: string | number;
}

export class RawValueMustBeLessThanValidator implements Validator<ValueProps> {
  private readonly _otherField?: string;
  private readonly _errMsg?: string;

  constructor(options: RawValueMustBeLessThanValidatorOptions = {}) {
    this._otherField = options.otherField;
    this._errMsg = options.errorMsg;
  }

  validate(value: ValueProps, context: ValidationContext): string | undefined {
    if (
      this._otherField &&
      parseFloat(`${value?.rawValue ?? 0}`) >=
        parseFloat(`${context.valueOf(this._otherField)?.rawValue ?? 0}`)
    ) {
      return this._errMsg || `It must be less than ${this._otherField}`;
    }

    return undefined;
  }
}
