import { IsEmptyValidator } from './isEmpty';

describe('IsEmptyValidator', () => {
  describe('with empty value', () => {
    it('return undefined when has no error message', () => {
      const isEmptyValidator = new IsEmptyValidator();

      expect(isEmptyValidator.validate('')).toBeUndefined();
    });

    it('return error message when has error message', () => {
      const errorMsg = 'error message';
      const isEmptyValidator = new IsEmptyValidator({ errorMsg });

      expect(isEmptyValidator.validate('')).toEqual(errorMsg);
    });
  });

  it('return undefined when validate valid string', () => {
    const isEmptyValidator = new IsEmptyValidator();

    expect(isEmptyValidator.validate('text')).toBeUndefined();
  });
});
