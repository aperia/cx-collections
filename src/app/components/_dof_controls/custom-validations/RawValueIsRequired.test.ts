import { RawValueIsRequiredValidator } from './RawValueIsRequired';

describe('RawValueIsRequiredValidator', () => {
  describe('with empty value', () => {
    it('return undefined when has no error message', () => {
      const isRawValueRequired = new RawValueIsRequiredValidator();

      expect(
        isRawValueRequired.validate({ enable: false, rawValue: '' })
      ).toBeUndefined();
    });

    it('return error message when has error message', () => {
      const errorMsg = 'error message';
      const isRawValueRequired = new RawValueIsRequiredValidator({ errorMsg });

      expect(
        isRawValueRequired.validate({ enable: true, rawValue: '' })
      ).toEqual(errorMsg);
    });
  });

  it('return undefined when validate valid string', () => {
    const isRawValueRequired = new RawValueIsRequiredValidator();

    expect(
      isRawValueRequired.validate({ enable: true, rawValue: 'text' })
    ).toBeUndefined();
  });
});
