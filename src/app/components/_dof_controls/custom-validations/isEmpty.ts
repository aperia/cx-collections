import { Validator, ValidationOptions } from 'app/_libraries/_dof/core';
import isEmpty from 'lodash.isempty';
import isNull from 'lodash.isnull';
import isUndefined from 'lodash.isundefined';

export class IsEmptyValidator implements Validator<string> {
  private readonly _errMsg?: string;

  constructor(options: ValidationOptions = {}) {
    this._errMsg = options.errorMsg;
  }

  validate(value: string): string | undefined {
    if (isEmpty(value) && !isNull(value) && !isUndefined(value)) {
      return this._errMsg;
    }

    return undefined;
  }
}
