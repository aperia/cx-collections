import { LessThanOrEqualValidator } from './LessThanOrEqual';
const mockCustomErrorMsg = 'errorMsg';

const testCases = [
  {
    caseName: '0.00 <= 0.00 should return undefined',
    value: '0.00',
    otherValue: '0.00',
    expected: {
      errMsg: undefined
    }
  },
  {
    caseName: '0.00 <= 0.01 should return undefined',
    value: '0.00',
    otherValue: '0.01',
    expected: {
      errMsg: undefined
    }
  },
  {
    caseName: '1 <= 1 should return undefined',
    value: '1',
    otherValue: '1',
    expected: {
      errMsg: undefined
    }
  },
  {
    caseName: '0 <= 1 should return undefined',
    value: '0',
    otherValue: '1',
    expected: {
      errMsg: undefined
    }
  },
  {
    caseName: '1.1 <= 1.1 should return undefined',
    value: '1.1',
    otherValue: '1.1',
    expected: {
      errMsg: undefined
    }
  },
  {
    caseName: '1.0 <= 1.1 should return undefined',
    value: '1.0',
    otherValue: '1.1',
    expected: {
      errMsg: undefined
    }
  },
  {
    caseName: '2 <= 1 should return custom error message',
    value: '2',
    otherValue: '1',
    expected: {
      hasError: true,
      errMsg: mockCustomErrorMsg
    }
  },
  {
    caseName: '1.2 <= 1.1 should return custom error message',
    value: '1.2',
    otherValue: '1.1',
    expected: {
      hasError: true,
      errMsg: mockCustomErrorMsg
    }
  },
  {
    caseName: '1.2 <= 1.1 should return default error message',
    value: '1.2',
    otherValue: '1.1',
    expected: {
      hasError: true,
      errMsg: undefined
    }
  },
  {
    caseName: '0.01 <= 0.00 should return default error message',
    value: '0.01',
    otherValue: '0.00',
    expected: {
      hasError: true,
      errMsg: undefined
    }
  }
];

describe.each(testCases)(
  'LessThanOrEqualValidator',
  ({ caseName, value, otherValue, expected }) => {
    it(`value.compareTo(otherValue): ${caseName}${
      expected.hasError
        ? expected.errMsg
          ? ': ' + expected.errMsg
          : `: It must be less than or equal ${otherValue}`
        : '.'
    }`, () => {
      const { errMsg, hasError } = expected;
      const defaultErrorMsg = `It must be less than or equal ${otherValue}`;

      const validator = new LessThanOrEqualValidator({
        otherValue,
        errorMsg: errMsg
      });
      expect(validator.validate(value)).toEqual(
        hasError && !errMsg ? defaultErrorMsg : errMsg
      );
    });
  }
);

describe('LessThanOrEqualValidator empty constructor', () => {
  it('return undefined', () => {
    const validator = new LessThanOrEqualValidator();
    expect(validator.validate('1')).toEqual(undefined);
  });
});
