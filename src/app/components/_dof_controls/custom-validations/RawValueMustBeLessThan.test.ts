import { RawValueMustBeLessThanValidator } from './RawValueMustBeLessThan';

describe('RawValueMustBeLessThanValidator', () => {
  const mockOtherField = 'otherField';
  const mockErrorMsg = 'errorMsg';
  const mockMiddleValue = { enable: true, rawValue: '2' };
  const mockLessThanValue = { enable: true, rawValue: '1' };
  it('return undefined', () => {
    const newRawValueMustBeLessThanValidator =
      new RawValueMustBeLessThanValidator();
    expect(
      newRawValueMustBeLessThanValidator.validate(mockMiddleValue, {
        labelOf: jest.fn(),
        valueOf: jest.fn().mockReturnValue(mockLessThanValue)
      })
    ).toEqual(undefined);
  });

  it('has otherField and has not value', () => {
    const newRawValueMustBeLessThanValidator =
      new RawValueMustBeLessThanValidator({
        otherField: mockOtherField,
        errorMsg: mockErrorMsg
      });
    expect(
      newRawValueMustBeLessThanValidator.validate(
        {},
        {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(mockMiddleValue)
        }
      )
    ).toEqual(undefined);
  });

  it('has otherField and no value', () => {
    const newRawValueMustBeLessThanValidator =
      new RawValueMustBeLessThanValidator({
        otherField: mockOtherField
      });
    expect(
      newRawValueMustBeLessThanValidator.validate(mockMiddleValue, {
        labelOf: jest.fn(),
        valueOf: jest.fn().mockReturnValue({})
      })
    ).toEqual(`It must be less than ${mockOtherField}`);
  });

  it('has default error', () => {
    const newRawValueMustBeLessThanValidator =
      new RawValueMustBeLessThanValidator({
        otherField: mockOtherField
      });
    expect(
      newRawValueMustBeLessThanValidator.validate(mockMiddleValue, {
        labelOf: jest.fn(),
        valueOf: jest.fn().mockReturnValue(mockLessThanValue)
      })
    ).toEqual(`It must be less than ${mockOtherField}`);
  });
});
