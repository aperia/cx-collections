import {
  Validator,
  ValidationContext,
  ValidationOptions
} from 'app/_libraries/_dof/core';

interface RawValueMustBeGreaterThanValidatorOptions extends ValidationOptions {
  otherField?: string;
  otherValue?: string;
}

interface ValueProps {
  enable?: boolean;
  rawValue?: string | number;
}

export class RawValueMustBeGreaterThanValidator
  implements Validator<ValueProps>
{
  private readonly _otherField?: string;
  private readonly _otherValue?: string;
  private readonly _errMsg?: string;

  constructor(options: RawValueMustBeGreaterThanValidatorOptions = {}) {
    this._otherField = options.otherField;
    this._otherValue = options.otherValue;
    this._errMsg = options.errorMsg;
  }

  validate(value: ValueProps, context: ValidationContext): string | undefined {
    let otherValue = undefined;
    if (this._otherField) {
      otherValue = parseFloat(
        `${context.valueOf(this._otherField)?.rawValue ?? 0}`
      );
    } else if (this._otherValue !== undefined && this._otherValue !== '') {
      otherValue = parseFloat(this._otherValue);
    }

    if (otherValue && parseFloat(`${value?.rawValue ?? 0}`) <= otherValue) {
      return this._errMsg || `It must be greater than ${this._otherField}`;
    }

    return undefined;
  }
}
