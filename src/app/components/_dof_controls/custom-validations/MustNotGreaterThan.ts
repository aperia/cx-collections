import {
  Validator,
  ValidationContext,
  ValidationOptions
} from 'app/_libraries/_dof/core';

interface MustNotGreaterThanValidatorOptions extends ValidationOptions {
  otherField?: string;
}

export class MustNotGreaterThanValidator implements Validator<string> {
  private readonly _otherField?: string;
  private readonly _errMsg?: string;

  constructor(options: MustNotGreaterThanValidatorOptions = {}) {
    this._otherField = options.otherField;
    this._errMsg = options.errorMsg;
  }

  validate(value: string, context: ValidationContext): string | undefined {
    if (this._otherField && +value > +context.valueOf(this._otherField)) {
      return this._errMsg || `It must be not greater than ${this._otherField}`;
    }

    return undefined;
  }
}
