import { validatorRegistry } from 'app/_libraries/_dof/core';
import { GreaterThanOrEqualValidator } from './GreaterThanOrEqual';
import { IsEmptyValidator } from './isEmpty';
import { LessThanOrEqualValidator } from './LessThanOrEqual';
import { MustNotGreaterThanValidator } from './MustNotGreaterThan';
import { MustNotLessThanValidator } from './MustNotLessThan';
import { RawValueIsRequiredValidator } from './RawValueIsRequired';
import { RawValueMustBeGreaterThanValidator } from './RawValueMustBeGreaterThan';
import { RawValueMustBeLessThanValidator } from './RawValueMustBeLessThan';

validatorRegistry.registerValidator(
  'MustNotLessThan',
  MustNotLessThanValidator
);
validatorRegistry.registerValidator(
  'MustNotGreaterThan',
  MustNotGreaterThanValidator
);

validatorRegistry.registerValidator('IsEmptyValidator', IsEmptyValidator);

validatorRegistry.registerValidator(
  'RawValueIsRequired',
  RawValueIsRequiredValidator
);

validatorRegistry.registerValidator(
  'RawValueMustBeLessThan',
  RawValueMustBeLessThanValidator
);

validatorRegistry.registerValidator(
  'RawValueMustBeGreaterThan',
  RawValueMustBeGreaterThanValidator
);

validatorRegistry.registerValidator(
  'GreaterThanOrEqual',
  GreaterThanOrEqualValidator
);

validatorRegistry.registerValidator(
  'LessThanOrEqual',
  LessThanOrEqualValidator
);
