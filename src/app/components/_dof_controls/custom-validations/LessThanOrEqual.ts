import { ValidationOptions, Validator } from 'app/_libraries/_dof/core';

export interface LessThanOrEqualOptions extends ValidationOptions {
  otherValue?: string;
}

export class LessThanOrEqualValidator implements Validator<string> {
  private readonly _otherValue?: string;
  private readonly _errMsg?: string;

  constructor(options: LessThanOrEqualOptions = {}) {
    this._otherValue = options.otherValue;
    this._errMsg = options.errorMsg;
  }

  validate(value: string): string | undefined {
    if (
      typeof this._otherValue === 'undefined' ||
      (typeof this._otherValue !== 'undefined' &&
        parseFloat(value) <= parseFloat(this._otherValue))
    ) {
      return;
    }

    return this._errMsg || `It must be less than or equal ${this._otherValue}`;
  }
}
