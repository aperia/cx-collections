import { Validator, ValidationOptions } from 'app/_libraries/_dof/core';
import isEmpty from 'lodash.isempty';

interface ValueProps {
  enable?: boolean;
  rawValue?: string | number;
}

export class RawValueIsRequiredValidator
  implements Validator<ValueProps, string>
{
  private readonly _errMsg?: string;

  constructor(options: ValidationOptions = {}) {
    this._errMsg = options.errorMsg;
  }

  validate(value: ValueProps): string | undefined {
    if (value?.enable && isEmpty(value?.rawValue)) {
      return this._errMsg;
    }

    return undefined;
  }
}
