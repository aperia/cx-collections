import { ValidationOptions, Validator } from 'app/_libraries/_dof/core';

export interface GreaterThanOrEqualOptions extends ValidationOptions {
  otherValue?: string;
}

export class GreaterThanOrEqualValidator implements Validator<string> {
  private readonly _otherValue?: string;
  private readonly _errMsg?: string;

  constructor(options: GreaterThanOrEqualOptions = {}) {
    this._otherValue = options.otherValue;
    this._errMsg = options.errorMsg;
  }

  validate(value: string): string | undefined {
    if (
      typeof this._otherValue === 'undefined' ||
      (typeof this._otherValue !== 'undefined' &&
        parseFloat(value) >= parseFloat(this._otherValue))
    ) {
      return;
    }

    return (
      this._errMsg || `It must be greater than or equal ${this._otherValue}`
    );
  }
}
