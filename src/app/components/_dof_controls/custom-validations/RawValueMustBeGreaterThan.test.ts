import { RawValueMustBeGreaterThanValidator } from './RawValueMustBeGreaterThan';

describe('RawValueMustBeGreaterThanValidator', () => {
  const mockOtherField = 'otherField';
  const mockErrorMsg = 'errorMsg';
  const mockMiddleValue = { enable: true, rawValue: '2' };
  const mockGreaterThanValue = { enable: true, rawValue: '3' };
  it('return undefined', () => {
    const newRawValueMustBeGreaterThanValidator =
      new RawValueMustBeGreaterThanValidator();
    expect(
      newRawValueMustBeGreaterThanValidator.validate(mockMiddleValue, {
        labelOf: jest.fn(),
        valueOf: jest.fn().mockReturnValue(mockGreaterThanValue)
      })
    ).toEqual(undefined);
  });

  it('has otherField and has not value', () => {
    const newRawValueMustBeGreaterThanValidator =
      new RawValueMustBeGreaterThanValidator({
        otherField: mockOtherField,
        errorMsg: mockErrorMsg
      });
    expect(
      newRawValueMustBeGreaterThanValidator.validate(
        {},
        {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(mockMiddleValue)
        }
      )
    ).toEqual(mockErrorMsg);
  });

  it('has otherField and no value', () => {
    const newRawValueMustBeGreaterThanValidator =
      new RawValueMustBeGreaterThanValidator({
        otherField: mockOtherField
      });
    expect(
      newRawValueMustBeGreaterThanValidator.validate(mockMiddleValue, {
        labelOf: jest.fn(),
        valueOf: jest.fn().mockReturnValue({})
      })
    ).toEqual(undefined);
  });

  it('has default error', () => {
    const newRawValueMustBeGreaterThanValidator =
      new RawValueMustBeGreaterThanValidator({
        otherField: mockOtherField
      });
    expect(
      newRawValueMustBeGreaterThanValidator.validate(mockMiddleValue, {
        labelOf: jest.fn(),
        valueOf: jest.fn().mockReturnValue(mockGreaterThanValue)
      })
    ).toEqual(`It must be greater than ${mockOtherField}`);
  });

  it('has no otherField but has otherValue', () => {
    const newRawValueMustBeGreaterThanValidator =
      new RawValueMustBeGreaterThanValidator({
        otherValue: mockMiddleValue.rawValue,
        errorMsg: mockErrorMsg
      });
    expect(
      newRawValueMustBeGreaterThanValidator.validate(mockMiddleValue, {
        labelOf: jest.fn(),
        valueOf: jest.fn().mockReturnValue(mockGreaterThanValue)
      })
    ).toEqual(mockErrorMsg);
  });
});
