import { Validator, ValidationOptions } from 'app/_libraries/_dof/core';

interface MustNotLessThanValidatorOptions extends ValidationOptions {
  otherField?: string;
  otherValue?: string;
}

export class MustNotLessThanValidator implements Validator<string> {
  private readonly _otherValue?: string;
  private readonly _errMsg?: string;

  constructor(options: MustNotLessThanValidatorOptions = {}) {
    this._otherValue = options.otherValue;
    this._errMsg = options.errorMsg;
  }

  validate(value: string): string | undefined {
    if (
      this._otherValue !== undefined &&
      +value <= parseFloat(this._otherValue)
    ) {
      return this._errMsg;
    }

    return undefined;
  }
}
