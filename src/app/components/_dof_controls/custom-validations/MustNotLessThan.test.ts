import { MustNotLessThanValidator } from './MustNotLessThan';

describe('MustNotLessThanValidator', () => {
  const mockLessThanValue = '1';
  const mockGreaterThanValue = '2';
  const mockErrorMsg = 'errorMsg';
  it('return undefined', () => {
    const newMustNotLessThanValidator = new MustNotLessThanValidator();
    expect(newMustNotLessThanValidator.validate(mockLessThanValue)).toEqual(
      undefined
    );
  });

  it('return error message', () => {
    const newMustNotLessThanValidator = new MustNotLessThanValidator({
      otherValue: mockGreaterThanValue,
      errorMsg: mockErrorMsg
    });
    expect(newMustNotLessThanValidator.validate(mockLessThanValue)).toEqual(
      mockErrorMsg
    );
  });
});
