import { MustNotGreaterThanValidator } from './MustNotGreaterThan';

describe('MustNotGreaterThanValidator', () => {
  const mockOtherField = 'otherField';
  const mockErrorMsg = 'errorMsg';
  it('return undefined', () => {
    const newMustNotGreaterThanValidator = new MustNotGreaterThanValidator();
    expect(
      newMustNotGreaterThanValidator.validate(mockOtherField, {
        labelOf: jest.fn(),
        valueOf: jest.fn()
      })
    ).toEqual(undefined);
  });

  it('has otherField and value greater than other field', () => {
    const newMustNotGreaterThanValidator = new MustNotGreaterThanValidator({
      otherField: mockOtherField
    });
    expect(
      newMustNotGreaterThanValidator.validate('1', {
        labelOf: jest.fn(),
        valueOf: jest.fn().mockReturnValue('2')
      })
    ).toEqual(undefined);
  });

  it('has otherField and value not greater than other field', () => {
    const newMustNotGreaterThanValidator = new MustNotGreaterThanValidator({
      otherField: mockOtherField,
      errorMsg: mockErrorMsg
    });
    expect(
      newMustNotGreaterThanValidator.validate('1', {
        labelOf: jest.fn(),
        valueOf: jest.fn().mockReturnValue('0')
      })
    ).toEqual(mockErrorMsg);
  });

  it('has otherField and value not greater than other field and not config error msg', () => {
    const newMustNotGreaterThanValidator = new MustNotGreaterThanValidator({
      otherField: mockOtherField
    });
    expect(
      newMustNotGreaterThanValidator.validate('1', {
        labelOf: jest.fn(),
        valueOf: jest.fn().mockReturnValue('0')
      })
    ).toEqual('It must be not greater than otherField');
  });
});
