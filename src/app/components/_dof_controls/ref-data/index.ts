import { DataApiRequest } from 'app/_libraries/_dof/core';

const requestBuilder = (request: Partial<DataApiRequest>): DataApiRequest => {
  const DEFAULT_HEADERS = [
    {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  ];
  const { method, headers, requestData } = request;
  return {
    method: method || 'POST',
    headers: headers || DEFAULT_HEADERS,
    requestData: requestData
  };
};

function getIncomeFrequency(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'incomeFrequency'
    },
    method: 'GET'
  });
}

function getMonthlyOutgoInformation(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'getMonthlyOutgoInformation'
    },
    method: 'GET'
  });
}

function getCountryOfDeployment(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'getCountryOfDeployment'
    },
    method: 'GET'
  });
}
function getPaymentMethod(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'getPaymentMethod'
    },
    method: 'GET'
  });
}

function getLongTermReageMonths(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'getLongTermReageMonths'
    },
    method: 'GET'
  });
}

function bankruptcyChapter(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'bankruptcyChapter'
    },
    method: 'GET'
  });
}

function fillingInformationType(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'fillingInformationType'
    },
    method: 'GET'
  });
}

function fillingInformationAssets(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'fillingInformationAssets'
    },
    method: 'GET'
  });
}

function fillingInformationProSe(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'fillingInformationProSe'
    },
    method: 'GET'
  });
}

function fillingInformationSurrenderIndicator(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'fillingInformationSurrenderIndicator'
    },
    method: 'GET'
  });
}

function fillingInformationNonfilerIndicator(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'fillingInformationNonfilerIndicator'
    },
    method: 'GET'
  });
}

function fillingInformationNonfilerCollectibleIndicator(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'fillingInformationNonfilerCollectibleIndicator'
    },
    method: 'GET'
  });
}

function fillingInformationPrimaryCustomerInformationIndicatorCode(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'fillingInformationPrimaryCustomerInformationIndicatorCode'
    },
    method: 'GET'
  });
}

function fillingInformationPrimaryCreditBureauReportCode(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'fillingInformationPrimaryCreditBureauReportCode'
    },
    method: 'GET'
  });
}

function fillingInformationSecondaryCustomerInformationIndicatorCode(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'fillingInformationSecondaryCustomerInformationIndicatorCode'
    },
    method: 'GET'
  });
}

function fillingInformationSecondaryCreditBureauReportCode(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'fillingInformationSecondaryCreditBureauReportCode'
    },
    method: 'GET'
  });
}

function additionalAttorneyInformationRetained(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'additionalAttorneyInformationRetained'
    },
    method: 'GET'
  });
}

function getStateRefData(): DataApiRequest {
  return requestBuilder({
    requestData: { componentID: 'state' },
    method: 'GET'
  });
}

function settlementMethod(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function deceasedCardholderContact(): DataApiRequest {
  return requestBuilder({
    requestData: { componentID: 'deceasedCardholderContact' }
  });
}

function getDeceasedContact(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getUSAState(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getPromiseToPayMethod(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getPromiseToPayInterval(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getPromiseToPayCount(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getFullMonth(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getMethodToSend(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function editPhoneFlag(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function editPhoneDeviceType(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getAddressCategory(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getAddressType(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getAddressRelationShip(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getCountryIso(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function adjustmentType(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function reasonForAdjustment(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function promoId(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getMoveToQueue(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getDelinquencyReason(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function reagePaymentPeriod(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getExternalStatus(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

function getExternalStatusReason(): DataApiRequest {
  return requestBuilder({
    method: 'GET',
    requestData: {}
  });
}

export const refData = {
  getIncomeFrequency,
  getCountryOfDeployment,
  getPaymentMethod,
  getLongTermReageMonths,
  getMonthlyOutgoInformation,
  bankruptcyChapter,
  getMethodToSend,
  getFullMonth,
  fillingInformationType,
  fillingInformationAssets,
  fillingInformationProSe,
  fillingInformationSurrenderIndicator,
  fillingInformationNonfilerIndicator,
  fillingInformationNonfilerCollectibleIndicator,
  fillingInformationPrimaryCustomerInformationIndicatorCode,
  fillingInformationPrimaryCreditBureauReportCode,
  fillingInformationSecondaryCustomerInformationIndicatorCode,
  fillingInformationSecondaryCreditBureauReportCode,
  additionalAttorneyInformationRetained,
  getStateRefData,
  settlementMethod,
  deceasedCardholderContact,
  getUSAState,
  getDeceasedContact,
  getPromiseToPayMethod,
  getPromiseToPayInterval,
  getPromiseToPayCount,
  editPhoneDeviceType,
  editPhoneFlag,
  getAddressCategory,
  getAddressType,
  getAddressRelationShip,
  getCountryIso,
  adjustmentType,
  reasonForAdjustment,
  promoId,
  getMoveToQueue,
  getDelinquencyReason,
  reagePaymentPeriod,
  getExternalStatus,
  getExternalStatusReason
};
