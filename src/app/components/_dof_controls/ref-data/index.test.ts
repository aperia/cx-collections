import { refData } from './index';

const headers = [
  {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
];

describe('dof_controls > ref-data', () => {
  it('getIncomeFrequency', () => {
    const result = refData.getIncomeFrequency();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {
        componentID: 'incomeFrequency'
      }
    });
  });

  it('getMonthlyOutgoInformation', () => {
    const result = refData.getMonthlyOutgoInformation();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {
        componentID: 'getMonthlyOutgoInformation'
      }
    });
  });

  it('getCountryOfDeployment', () => {
    const result = refData.getCountryOfDeployment();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {
        componentID: 'getCountryOfDeployment'
      }
    });
  });

  it('getPaymentMethod', () => {
    const result = refData.getPaymentMethod();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {
        componentID: 'getPaymentMethod'
      }
    });
  });

  it('getLongTermReageMonths', () => {
    const result = refData.getLongTermReageMonths();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {
        componentID: 'getLongTermReageMonths'
      }
    });
  });

  it('bankruptcyChapter', () => {
    const result = refData.bankruptcyChapter();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: { componentID: 'bankruptcyChapter' }
    });
  });

  it('fillingInformationType', () => {
    const result = refData.fillingInformationType();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: { componentID: 'fillingInformationType' }
    });
  });

  it('fillingInformationAssets', () => {
    const result = refData.fillingInformationAssets();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: { componentID: 'fillingInformationAssets' }
    });
  });

  it('fillingInformationProSe', () => {
    const result = refData.fillingInformationProSe();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: { componentID: 'fillingInformationProSe' }
    });
  });

  it('fillingInformationSurrenderIndicator', () => {
    const result = refData.fillingInformationSurrenderIndicator();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: { componentID: 'fillingInformationSurrenderIndicator' }
    });
  });

  it('fillingInformationNonfilerIndicator', () => {
    const result = refData.fillingInformationNonfilerIndicator();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: { componentID: 'fillingInformationNonfilerIndicator' }
    });
  });

  it('fillingInformationNonfilerCollectibleIndicator', () => {
    const result = refData.fillingInformationNonfilerCollectibleIndicator();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {
        componentID: 'fillingInformationNonfilerCollectibleIndicator'
      }
    });
  });

  it('fillingInformationPrimaryCustomerInformationIndicatorCode', () => {
    const result =
      refData.fillingInformationPrimaryCustomerInformationIndicatorCode();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {
        componentID: 'fillingInformationPrimaryCustomerInformationIndicatorCode'
      }
    });
  });

  it('fillingInformationPrimaryCreditBureauReportCode', () => {
    const result = refData.fillingInformationPrimaryCreditBureauReportCode();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {
        componentID: 'fillingInformationPrimaryCreditBureauReportCode'
      }
    });
  });

  it('fillingInformationSecondaryCustomerInformationIndicatorCode', () => {
    const result =
      refData.fillingInformationSecondaryCustomerInformationIndicatorCode();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {
        componentID:
          'fillingInformationSecondaryCustomerInformationIndicatorCode'
      }
    });
  });

  it('fillingInformationSecondaryCreditBureauReportCode', () => {
    const result = refData.fillingInformationSecondaryCreditBureauReportCode();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {
        componentID: 'fillingInformationSecondaryCreditBureauReportCode'
      }
    });
  });

  it('additionalAttorneyInformationRetained', () => {
    const result = refData.additionalAttorneyInformationRetained();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: { componentID: 'additionalAttorneyInformationRetained' }
    });
  });

  it('getStateRefData', () => {
    const result = refData.getStateRefData();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: { componentID: 'state' }
    });
  });

  it('settlementMethod', () => {
    const result = refData.settlementMethod();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('deceasedCardholderContact', () => {
    const result = refData.deceasedCardholderContact();

    expect(result).toEqual({
      method: 'POST',
      headers,
      requestData: { componentID: 'deceasedCardholderContact' }
    });
  });

  it('getDeceasedContact', () => {
    const result = refData.getDeceasedContact();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getUSAState', () => {
    const result = refData.getUSAState();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getPromiseToPayMethod', () => {
    const result = refData.getPromiseToPayMethod();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getPromiseToPayInterval', () => {
    const result = refData.getPromiseToPayInterval();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getPromiseToPayCount', () => {
    const result = refData.getPromiseToPayCount();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getFullMonth', () => {
    const result = refData.getFullMonth();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getMethodToSend', () => {
    const result = refData.getMethodToSend();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('editPhoneFlag', () => {
    const result = refData.editPhoneFlag();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('editPhoneDeviceType', () => {
    const result = refData.editPhoneDeviceType();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getAddressCategory', () => {
    const result = refData.getAddressCategory();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getAddressType', () => {
    const result = refData.getAddressType();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getAddressRelationShip', () => {
    const result = refData.getAddressRelationShip();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getCountryIso', () => {
    const result = refData.getCountryIso();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('adjustmentType', () => {
    const result = refData.adjustmentType();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('reasonForAdjustment', () => {
    const result = refData.reasonForAdjustment();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('promoId', () => {
    const result = refData.promoId();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getMoveToQueue', () => {
    const result = refData.getMoveToQueue();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getDelinquencyReason', () => {
    const result = refData.getDelinquencyReason();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('reagePaymentPeriod', () => {
    const result = refData.reagePaymentPeriod();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getExternalStatus', () => {
    const result = refData.getExternalStatus();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });

  it('getExternalStatusReason', () => {
    const result = refData.getExternalStatusReason();

    expect(result).toEqual({
      method: 'GET',
      headers,
      requestData: {}
    });
  });
});
