import { IValueChangeFunc } from 'app/_libraries/_dof/core';
import isEqual from 'lodash.isequal';

const fieldMap: Record<string, string> = {
  hardshipProgramPeriod: 'hardshipProgramPeriod',
  ragePaymentPeriod: 'ragePaymentPeriod'
};

const nameToField: Record<string, string> = {
  hardship_hardshipProgramPeriod: 'hardshipProgramPeriod',
  hardship_ragePaymentPeriod: 'ragePaymentPeriod'
};

export const createDataSource = (
  dataSources: MagicKeyValue[],
  currentValue: MagicKeyValue
) => {
  const indexCurrentFieldValue = dataSources.findIndex(item =>
    isEqual(currentValue, item)
  );

  return dataSources.slice(0, indexCurrentFieldValue + 1);
};

/**
 * Enable/disable dateNoticeReceived if dateOfDeath change,
 * clear value of dateNoticeReceived if new value of dateOfDeath > current dateNoticeReceived
 * set min value for dateNoticeReceived with the new value of dateOfDeath
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const periodChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  event,
  formValues
) => {
  const nameField = nameToField[name];
  const isHardshipProgramPeriod = nameField === fieldMap.hardshipProgramPeriod;
  const fieldUpdate = isHardshipProgramPeriod
    ? fieldMap.ragePaymentPeriod
    : fieldMap.hardshipProgramPeriod;

  const currentField = onFind(fieldMap[nameField]);
  const anotherField = onFind(fieldMap[fieldUpdate]);

  const dataSources: MagicKeyValue[] =
    (currentField?.props?.props as MagicKeyValue)?.data || [];

  const anotherValue = anotherField.value;

  const indexCurrentFieldValue = dataSources.findIndex(item =>
    isEqual(newValue, item)
  );
  const indexOtherFieldValue = dataSources.findIndex(item =>
    isEqual(anotherValue, item)
  );

  if (!isHardshipProgramPeriod) return;

  const newDataSource = createDataSource(dataSources, newValue);
  anotherField?.props?.props?.setData(newDataSource);
  if (indexOtherFieldValue > indexCurrentFieldValue) {
    anotherField?.props?.props?.setValue(newValue);
  }
};
