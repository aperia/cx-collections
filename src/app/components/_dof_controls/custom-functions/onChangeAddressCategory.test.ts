import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { ID_ADDRESS_TYPE } from 'pages/AccountDetails/CardHolderMaintenance/ExpandedAddress/constants';
import { onChangeAddressCategory } from './onChangeAddressCategory';

const mockSetValue = jest.fn();
const mockSetReadOnly = jest.fn();
let mockOnFind: jest.SpyInstance;

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setValue: mockSetValue,
    dependentField: ['dependentField 1', 'dependentField 2'],
    setReadOnly: mockSetReadOnly
  });
});

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockSetReadOnly.mockReset();
  mockSetReadOnly.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('onChangeAddressCategory custom function', () => {
  it('field ID equal PERMANENT', () => {
    customFunctionCommon(onChangeAddressCategory, {
      newValue: { fieldID: ID_ADDRESS_TYPE.PERMANENT },
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith('12/31/9999');
    expect(mockSetReadOnly).toBeCalledWith(true);
  });

  it('field ID not equal PERMANENT and has effective to year 9999', () => {
    customFunctionCommon(onChangeAddressCategory, {
      newValue: { fieldID: ID_ADDRESS_TYPE.REPEATING },
      onFind: mockOnFind,
      formValues: { effectiveTo: '12/30/9999' }
    });
    expect(mockSetValue).toBeCalledWith(undefined);
    expect(mockSetReadOnly).toBeCalledWith(false);
  });

  it('field ID equal PERMANENT and has effective to normal day', () => {
    customFunctionCommon(onChangeAddressCategory, {
      newValue: { fieldID: ID_ADDRESS_TYPE.PERMANENT },
      onFind: mockOnFind,
      formValues: { effectiveTo: '12/30/2099', effectiveFrom: '12/31/2088' }
    });
    expect(mockSetValue.mock.calls).toEqual([['12/31/9999'], [undefined]]);
    expect(mockSetReadOnly).toBeCalledWith(true);
  });

  it('field ID not equal PERMANENT and has effective to normal day', () => {
    customFunctionCommon(onChangeAddressCategory, {
      newValue: { fieldID: ID_ADDRESS_TYPE.REPEATING },
      onFind: mockOnFind,
      formValues: { effectiveTo: '12/30/2099', effectiveFrom: '12/31/2088' }
    });
    expect(mockSetValue.mock.calls).toEqual([[undefined], [undefined]]);
    expect(mockSetReadOnly).toBeCalledWith(false);
  });

  it('previousValue === newValue', () => {
    customFunctionCommon(onChangeAddressCategory, {
      newValue: {
        description: 'P - Permanent',
        fieldID: 'P',
        fieldValue: 'Permanent'
      },
      previousValue: {
        description: 'P - Permanent',
        fieldID: 'P',
        fieldValue: 'Permanent'
      },
      onFind: mockOnFind,
      formValues: { effectiveTo: '12/30/2099', effectiveFrom: '12/31/2088' }
    });
    expect(mockSetValue.mock.calls).not.toEqual([[undefined], [undefined]]);
  });
});
