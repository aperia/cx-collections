import { IValueChangeFunc } from 'app/_libraries/_dof/core';
import isEqual from 'lodash.isequal';
import { HARDSHIP_AMOUNT } from 'pages/HardShip/constants';
import { getErrorMsgHardshipAmount } from './CompareNumber';

const fieldMap: Record<string, string> = {
  cycleToDatePaymentsAmount: HARDSHIP_AMOUNT.TOTAL_AMOUNT,
  nonDelinquentMpdAmount: HARDSHIP_AMOUNT.NON_DELINQUENT_AMOUNT,
  newFixedPaymentAmount: HARDSHIP_AMOUNT.NEW_FIXED_PAYMENT_AMOUNT
};

const nameField: Record<string, string> = {
  hardship_newFixedPaymentAmount: HARDSHIP_AMOUNT.NEW_FIXED_PAYMENT_AMOUNT,
  hardship_nonDelinquentMpdAmount: HARDSHIP_AMOUNT.NON_DELINQUENT_AMOUNT
};

const otherNameField: Record<string, string> = {
  hardship_nonDelinquentMpdAmount: HARDSHIP_AMOUNT.NEW_FIXED_PAYMENT_AMOUNT,
  hardship_newFixedPaymentAmount: HARDSHIP_AMOUNT.NON_DELINQUENT_AMOUNT
};

const MsgGreaterError =
  'New Fixed Payment Amount must be equal to or greater than minimum payment amount';
/**
 * Enable/disable dateNoticeReceived if dateOfDeath change,
 * clear value of dateNoticeReceived if new value of dateOfDeath > current dateNoticeReceived
 * set min value for dateNoticeReceived with the new value of dateOfDeath
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const amountChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  event,
  formValues
) => {
  const fieldUpdate = otherNameField[name];

  const notChange = isEqual(newValue, previousValue);
  if (notChange) return;

  const isChangeMPD = nameField[name] === HARDSHIP_AMOUNT.NON_DELINQUENT_AMOUNT;

  const currentField = onFind(nameField[name]);
  const anotherField = onFind(fieldMap[fieldUpdate]);
  const { value: total } = onFind(fieldMap[HARDSHIP_AMOUNT.TOTAL_AMOUNT]) || {};
  const { value: fixedMinValue } =
    onFind(HARDSHIP_AMOUNT.NEW_FIXED_PAYMENT_MIN) || {};
  const nextValue = (total - newValue).toFixed(2);
  const newAnotherValue = Number(nextValue) < 0 ? '' : nextValue;

  anotherField?.props?.props?.setValue(newAnotherValue);

  const { setOthers: mpdSetOthers } = isChangeMPD
    ? currentField?.props?.props
    : anotherField?.props?.props;

  const { setOthers: fixedSetOthers } = !isChangeMPD
    ? currentField?.props?.props
    : anotherField?.props?.props;

  mpdSetOthers((prevState: MagicKeyValue) => {
    const mpdValue = isChangeMPD ? newValue : newAnotherValue;
    let newOthers: MagicKeyValue = { ...prevState, forceInvalid: false };

    // message required
    if (mpdValue === '') {
      newOthers = {
        ...prevState,
        forceInvalid: true,
        meta: { ...prevState.meta, invalid: true, error: 'txt_error_mpd' }
      };
    }
    // message over total
    if (Number(mpdValue) > Number(total)) {
      newOthers = {
        ...prevState,
        forceInvalid: true,
        meta: {
          ...prevState.meta,
          invalid: true,
          error: 'txt_error_mpd_payment'
        }
      };
    }
    return newOthers;
  });
  fixedSetOthers((prevState: MagicKeyValue) => {
    const fixedValue = !isChangeMPD ? newValue : newAnotherValue;
    let newOthers: MagicKeyValue = { ...prevState, forceInvalid: false };
    // message over total
    if (Number(fixedValue) > Number(total)) {
      newOthers = {
        ...prevState,
        forceInvalid: true,
        meta: {
          ...prevState.meta,
          invalid: true,
          error: 'txt_error_new_fixed_payment'
        }
      };
    }
    // message under first Fixed Amount
    if (Number(fixedValue) < Number(fixedMinValue)) {
      newOthers = {
        ...prevState,
        forceInvalid: true,
        meta: {
          ...prevState.meta,
          invalid: true,
          error: getErrorMsgHardshipAmount(MsgGreaterError, fixedMinValue)
        }
      };
    }
    return newOthers;
  });
};
