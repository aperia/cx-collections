import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { textAreaMemoChange } from './textAreaMemoChange';

const mockSetValue = jest.fn();
const mockSetOthers = jest.fn().mockImplementation((fn: Function) => fn());
let mockOnFind: jest.SpyInstance;

const value = 'value';

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    value,
    setValue: mockSetValue,
    setOthers: (fn: Function) => {
      fn();
      mockSetOthers();
    }
  });
});

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('textAreaMemoChange custom function', () => {
  it('should return undefined when alertMsgMemo Field is null', () => {
    const result = customFunctionCommon(textAreaMemoChange);
    expect(result).toEqual(undefined);
  });

  it('should enter a valid value', () => {
    customFunctionCommon(textAreaMemoChange, {
      newValue: 'to be accept',
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith('');
  });

  it('should enter invalid value', () => {
    customFunctionCommon(textAreaMemoChange, {
      newValue: 'invalid %<>',
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith(
      'Remove invalid character. These special characters are not allowed “ ) ( + ; \\ %'
    );
  });
});
