import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { onChangeAddressCountry } from './onChangeAddressCountry';
import * as getState from 'pages/AccountDetails/CardHolderMaintenance/ExpandedAddress/getStateSubdivision';

const mockSetData = jest.fn();
const mockSetReadOnly = jest.fn();
const mockSetValue = jest.fn();
const mockSetOthers = jest.fn();
let mockOnFind: jest.SpyInstance;
const mockPrevState = { validationRules: [] };

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setData: mockSetData,
    setReadOnly: mockSetReadOnly,
    setValue: mockSetValue,
    setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState)),
    value: 'value'
  });
});

afterEach(() => {
  mockSetData.mockReset();
  mockSetData.mockRestore();
  mockSetReadOnly.mockReset();
  mockSetReadOnly.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
});

describe('onChangeAddressCountry custom function', () => {
  it('should return undefined when newValue empty', async () => {
    const result = await customFunctionCommon(onChangeAddressCountry, {
      newValue: undefined
    });
    expect(mockSetData).not.toBeCalled();
    expect(result).toEqual(undefined);
  });

  it('should return undefined when previous value equal new value', async () => {
    const mockDataReturn = 'data';
    const spyGetStateSubdivision = jest
      .spyOn(getState, 'getStateSubdivision')
      .mockResolvedValue(mockDataReturn);
    await customFunctionCommon(onChangeAddressCountry, {
      newValue: { fieldID: 'value' },
      previousValue: { fieldID: 'value' },
      onFind: mockOnFind,
      formValues: {
        stateSubdivision: {
          description: 'BC - British Columbia',
          fieldId: 'BC',
          fieldValue: 'British Columbia'
        }
      }
    });
    expect(mockSetValue).not.toHaveBeenCalled();
    spyGetStateSubdivision.mockRestore();
    spyGetStateSubdivision.mockReset();
  });

  it('has value', async () => {
    const mockDataReturn = 'data';
    const spyGetStateSubdivision = jest
      .spyOn(getState, 'getStateSubdivision')
      .mockResolvedValue(mockDataReturn);
    await customFunctionCommon(onChangeAddressCountry, {
      newValue: { fieldID: 'value' },
      previousValue: { fieldID: 'other value' },
      onFind: mockOnFind,
      formValues: {
        stateSubdivision: {
          description: 'BC - British Columbia',
          fieldId: 'BC',
          fieldValue: 'British Columbia'
        }
      }
    });
    expect(mockSetValue).toHaveBeenCalled();
    expect(mockSetData).toBeCalledWith(mockDataReturn);
    expect(mockSetReadOnly).toBeCalledWith(false);
    spyGetStateSubdivision.mockReset();
    spyGetStateSubdivision.mockRestore();
  });

  it('value equal CAN', async () => {
    const mockDataReturn = 'data';
    const spyGetStateSubdivision = jest
      .spyOn(getState, 'getStateSubdivision')
      .mockResolvedValue(mockDataReturn);
    await customFunctionCommon(onChangeAddressCountry, {
      newValue: { fieldID: 'CAN' },
      previousValue: { fieldID: 'prev Value' },
      onFind: mockOnFind,
      formValues: {
        stateSubdivision: {
          description: 'BC - British Columbia',
          fieldId: 'BC',
          fieldValue: 'British Columbia'
        },
        postalCode: 'value'
      }
    });
    expect(mockSetValue).toHaveBeenCalled();
    expect(mockSetData).toBeCalledWith(mockDataReturn);
    expect(mockSetReadOnly).toBeCalledWith(false);
    spyGetStateSubdivision.mockReset();
    spyGetStateSubdivision.mockRestore();
  });
});
