import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { onChangeIncarceration } from './onChangeIncarceration';

const mockSetValue = jest.fn();
let mockOnFind: jest.SpyInstance;

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('onChangeIncarceration', () => {
  it('newValueDate >= valueEndDayDate', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      value: '12/31/1999'
    });
    customFunctionCommon(onChangeIncarceration, {
      newValue: '12/31/2000',
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith({});
  });

  it('newValueDate < valueEndDayDate', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      value: '12/31/2000'
    });
    customFunctionCommon(onChangeIncarceration, {
      newValue: '12/31/1999',
      onFind: mockOnFind
    });
    expect(mockSetValue).not.toBeCalled();
  });

  it('value end date is undefined', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      value: undefined
    });
    customFunctionCommon(onChangeIncarceration, {
      newValue: '12/31/1999',
      onFind: mockOnFind
    });
    expect(mockSetValue).not.toBeCalled();
  });

  it('new value is null', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      value: '12/31/1999'
    });
    customFunctionCommon(onChangeIncarceration, {
      newValue: null,
      onFind: mockOnFind
    });
    expect(mockSetValue).not.toBeCalled();
  });
});
