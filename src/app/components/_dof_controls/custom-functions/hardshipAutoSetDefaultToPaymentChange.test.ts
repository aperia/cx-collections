import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { hardshipAutoSetDefaultToPaymentChange } from './hardshipAutoSetDefaultToPaymentChange';
import * as commonHelpers from 'app/helpers/commons';

const mockSetValue = jest.fn();
const mockSetRequired = jest.fn();
let mockOnFind: jest.SpyInstance;

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('hardshipAutoSetDefaultToPaymentChange custom function', () => {
  const mockData = [{ value: 1 }, { value: 2 }];
  it('empty period', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      setRequired: mockSetRequired,
      value: new Date()
    });
    customFunctionCommon(hardshipAutoSetDefaultToPaymentChange, {
      newValue: new Date(),
      onFind: mockOnFind
    });
    expect(mockSetRequired).toBeCalledWith(true);
  });
  it('lastItemData.value < valuePeriod.value', () => {
    const mockData = [{ value: 1 }, { value: 2 }];
    mockOnFind = jest
      .fn()
      .mockImplementationOnce(
        mockOnFindGenerator({
          setValue: mockSetValue,
          setRequired: mockSetRequired,
          value: new Date()
        })
      )
      .mockImplementationOnce(
        mockOnFindGenerator({
          setValue: mockSetValue,
          setRequired: mockSetRequired,
          value: new Date()
        })
      )
      .mockImplementationOnce(
        mockOnFindGenerator({
          setValue: mockSetValue,
          setRequired: mockSetRequired,
          data: mockData,
          value: { value: 3 }
        })
      );
    customFunctionCommon(hardshipAutoSetDefaultToPaymentChange, {
      newValue: new Date(),
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith(mockData[0]);
  });

  it('isInvalidDateValue(valueFromPayment)', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      setRequired: mockSetRequired,
      value: undefined
    });
    customFunctionCommon(hardshipAutoSetDefaultToPaymentChange, {
      newValue: new Date(),
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith({});
  });

  it('lastItemData.value < valuePeriod.value', () => {
    const mockSetError = jest.fn();
    const spy = jest
      .spyOn(commonHelpers, 'setErrorField')
      .mockImplementation(mockSetError);
    mockOnFind = jest
      .fn()
      .mockImplementationOnce(
        mockOnFindGenerator({
          setValue: mockSetValue,
          setRequired: mockSetRequired,
          value: new Date()
        })
      )
      .mockImplementationOnce(
        mockOnFindGenerator({
          setValue: mockSetValue,
          setRequired: mockSetRequired,
          value: new Date()
        })
      )
      .mockImplementationOnce(
        mockOnFindGenerator({
          setValue: mockSetValue,
          setRequired: mockSetRequired,
          data: mockData,
          value: { value: 1 }
        })
      );
    customFunctionCommon(hardshipAutoSetDefaultToPaymentChange, {
      newValue: new Date(),
      onFind: mockOnFind
    });
    expect(mockSetError).toBeCalled();
    spy.mockRestore();
    spy.mockReset();
  });
  it('invalid newValue', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      setRequired: mockSetRequired,
      value: new Date()
    });
    customFunctionCommon(hardshipAutoSetDefaultToPaymentChange, {
      newValue: undefined,
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith({});
  });
});
