import { setErrorField } from 'app/helpers';
import { ExtraFieldProps, IValueChangeFunc } from 'app/_libraries/_dof/core';
import isNull from 'lodash.isnull';
import isUndefined from 'lodash.isundefined';
import { Field } from 'redux-form';

/**
 * Function to check if text area field has non-allowed character,
 * then set message for alert field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const hardshipDeploymentStartDateChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const deploymentEndRef = onFind('deploymentEndDate') as MagicKeyValue;
  deploymentEndRef?.props?.props?.setValue(null);
  const valueEnd = deploymentEndRef?.ref?.current?.props?.value;
  if (!isNull(valueEnd) && !isUndefined(valueEnd)) {
    setErrorField(deploymentEndRef as Field<ExtraFieldProps>, true);
  }
};
