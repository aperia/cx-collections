import { DECEASED_FIELDS } from 'pages/Deceased/constants';
import { IValueChangeFunc } from 'app/_libraries/_dof/core';
import { generateDateObject } from 'app/helpers';

const fieldMap: Record<string, string> = {
  primaryDateOfDeath: DECEASED_FIELDS.PRIMARY_DATE_NOTICE_RECEIVED,
  secondaryDateOfDeath: DECEASED_FIELDS.SECOND_DATE_NOTICE_RECEIVED
};

const anotherFieldMap: Record<string, string> = {
  primaryDateOfDeath: DECEASED_FIELDS.SECONDARY_DATE_OF_DEATH,
  secondaryDateOfDeath: DECEASED_FIELDS.PRIMARY_DATE_OF_DEATH
};

/**
 * Enable/disable dateNoticeReceived if dateOfDeath change,
 * clear value of dateNoticeReceived if new value of dateOfDeath > current dateNoticeReceived
 * set min value for dateNoticeReceived with the new value of dateOfDeath
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const dateOfDeathChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  event,
  formValues
) => {
  const dateNoticeReceived = onFind(fieldMap[name]);
  const anotherDateOfDeathField = onFind(anotherFieldMap[name]);
  const settlementDate = onFind(DECEASED_FIELDS.SETTLEMENT_DATE);

  const dateNoticeReceivedValue = generateDateObject(dateNoticeReceived?.value);
  const anotherDateOfDeathValue = anotherDateOfDeathField?.value;
  const settlementDateValue = generateDateObject(settlementDate?.value);
  const newDateOfDeathValue = generateDateObject(newValue);

  // clear value of dateNoticeReceived
  if (!newValue || newDateOfDeathValue > dateNoticeReceivedValue)
    dateNoticeReceived?.props?.props?.setValue(null);

  // clear value of settlementDate
  if (!newValue || newDateOfDeathValue > settlementDateValue) {
    settlementDate?.props?.props?.setValue(null);
  }

  // Set enabled/disabled for dateNoticeReceived and settlementDate
  dateNoticeReceived?.props?.props?.setEnable(!!newValue);
  settlementDate?.props?.props?.setEnable(
    !!newValue && (!anotherDateOfDeathField || anotherDateOfDeathValue)
  );

  // set min value for dateNoticeReceived and settlementDate
  dateNoticeReceived?.props?.props?.setOthers((prevState: MagicKeyValue) => ({
    ...prevState,
    options: {
      ...prevState.options,
      min: newDateOfDeathValue
    }
  }));
};
