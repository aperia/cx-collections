import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { dateTypeMemoChange } from './dateTypeChange';

const mockSetVisible = jest.fn();
const mockSetValue = jest.fn();
let mockOnFind: jest.SpyInstance;

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setVisible: mockSetVisible,
    setValue: mockSetValue
  });
});

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockSetVisible.mockReset();
  mockSetVisible.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('dataTypeChange', () => {
  it('searchByRadios equal null', () => {
    jest.useFakeTimers();
    const result = customFunctionCommon(dateTypeMemoChange);
    jest.runAllTimers();
    expect(result).toEqual(undefined);
  });

  it('is date range', () => {
    jest.useFakeTimers();
    customFunctionCommon(dateTypeMemoChange, {
      newValue: { dateRange: true },
      onFind: mockOnFind
    });
    jest.runAllTimers();
    expect(mockSetVisible.mock.calls).toEqual([[true], [false]]);
    expect(mockSetValue.mock.calls).toEqual([[null]]);
  });

  it('is not date range', () => {
    jest.useFakeTimers();
    customFunctionCommon(dateTypeMemoChange, {
      newValue: { dateRange: false },
      onFind: mockOnFind
    });
    jest.runAllTimers();
    expect(mockSetVisible.mock.calls).toEqual([[false], [true]]);
    expect(mockSetValue.mock.calls).toEqual([[null]]);
  });
});
