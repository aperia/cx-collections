import { IValueChangeFunc } from 'app/_libraries/_dof/core';
import isNull from 'lodash.isnull';
import isUndefined from 'lodash.isundefined';

/**
 * Function to check if text area field has non-allowed character,
 * then set message for alert field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const onChangeIncarceration: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  if (!isUndefined(newValue) && !isNull(newValue)) {
    const endDayRef = onFind('endDay') as MagicKeyValue;
    const valueEndDay = endDayRef?.ref?.current?.props?.value;
    if (!isNull(valueEndDay) && !isUndefined(valueEndDay)) {
      const newValueDate = new Date(newValue);
      const valueEndDayDate = new Date(valueEndDay);
      if (newValueDate >= valueEndDayDate)
        endDayRef?.props?.props?.setValue({});
    }
  }
};
