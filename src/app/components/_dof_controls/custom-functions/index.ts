import { dateOfDeathChange } from './dateOfDeath';
import { amountChange } from './currencyAmount';
import { hardshipGroupCountryOfDeploymentChange } from './hardshipGroupCountryOfDeploymentChange';
import { hardshipDeploymentStartDateChange } from './hardshipDeploymentStartDateChange';
import { hardshipDeploymentEndDateChange } from './hardshipDeploymentEndDateChange';
import { periodChange } from './hardshipPeriod';
import { valueChangeRegistry } from 'app/_libraries/_dof/core';
import { textAreaMemoChange } from './textAreaMemoChange';
import { dateTypeMemoChange } from './dateTypeChange';
import { deceasedDateOfDeath } from './deceasedDateOfDeath';
import { chronicleSearchByChange } from './chronicleSearchByChange';
import { onChangeAmountCCCS } from './onChangeAmountCCCS';
import { onChangeDelinquentCCCS } from './onChangeDelinquentCCCS';
import { editTelephoneByChange } from './editTelephoneByChange';
import { onChangeAddressCategory } from './onChangeAddressCategory';
import { onChangeAddressType } from './onChangeAddressType';
import { onChangeAddressCountry } from './onChangeAddressCountry';
import { onChangeDateExpandedAddress } from './onChangeDateExpandedAddress';
import { hardshipFromPaymentChange } from './hardshipFromPaymentChange';
import { hardshipToPaymentChange } from './hardshipToPaymentChange';
import { hardshipAutoSetDefaultToPaymentChange } from './hardshipAutoSetDefaultToPaymentChange';
import { onChangeIncarceration } from './onChangeIncarceration';
import { hardshipReageTypeChange } from './hardshipReageTypeChange';
import { onChangeReagePaymentPeriod } from './onChangeReagePaymentPeriod';
export * from './CompareNumber';

valueChangeRegistry.register('deceasedDateOfDeath', deceasedDateOfDeath);
valueChangeRegistry.register('textAreaMemoChange', textAreaMemoChange);
valueChangeRegistry.register('dateTypeMemoChange', dateTypeMemoChange);
valueChangeRegistry.register('dateOfDeathChange', dateOfDeathChange);
valueChangeRegistry.register(
  'hardshipGroupCountryOfDeploymentChange',
  hardshipGroupCountryOfDeploymentChange
);
valueChangeRegistry.register(
  'hardshipDeploymentStartDateChange',
  hardshipDeploymentStartDateChange
);
valueChangeRegistry.register(
  'hardshipDeploymentEndDateChange',
  hardshipDeploymentEndDateChange
);
valueChangeRegistry.register('amountChange', amountChange);
valueChangeRegistry.register('periodChange', periodChange);
valueChangeRegistry.register(
  'chronicleSearchByChange',
  chronicleSearchByChange
);
valueChangeRegistry.register('onChangeAmountCCCS', onChangeAmountCCCS);
valueChangeRegistry.register('onChangeDelinquentCCCS', onChangeDelinquentCCCS);
valueChangeRegistry.register('editTelephoneByChange', editTelephoneByChange);
valueChangeRegistry.register(
  'onChangeAddressCategory',
  onChangeAddressCategory
);
valueChangeRegistry.register('onChangeAddressCountry', onChangeAddressCountry);
valueChangeRegistry.register(
  'onChangeDateExpandedAddress',
  onChangeDateExpandedAddress
);
valueChangeRegistry.register('onChangeAddressType', onChangeAddressType);
valueChangeRegistry.register(
  'hardshipFromPaymentChange',
  hardshipFromPaymentChange
);
valueChangeRegistry.register(
  'hardshipToPaymentChange',
  hardshipToPaymentChange
);
valueChangeRegistry.register(
  'hardshipAutoSetDefaultToPaymentChange',
  hardshipAutoSetDefaultToPaymentChange
);
valueChangeRegistry.register('onChangeIncarceration', onChangeIncarceration);
valueChangeRegistry.register(
  'hardshipReageTypeChange',
  hardshipReageTypeChange
);

valueChangeRegistry.register(
  'onChangeReagePaymentPeriod',
  onChangeReagePaymentPeriod
);
