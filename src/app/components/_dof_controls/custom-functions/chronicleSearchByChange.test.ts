import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { chronicleSearchByChange } from './chronicleSearchByChange';

const mockSetVisible = jest.fn();
const mockSetValue = jest.fn();
let mockOnFind: jest.SpyInstance;

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setVisible: mockSetVisible,
    setValue: mockSetValue
  });
});

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockSetVisible.mockReset();
  mockSetVisible.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('chronicleSearchByChange', () => {
  it('searchByRadios equal null', () => {
    jest.useFakeTimers();
    const result = customFunctionCommon(chronicleSearchByChange);
    jest.runAllTimers();
    expect(result).toEqual(undefined);
  });

  it('is first new value', () => {
    jest.useFakeTimers();
    customFunctionCommon(chronicleSearchByChange, {
      newValue: { first: true },
      onFind: mockOnFind
    });
    jest.runAllTimers();
    expect(mockSetVisible.mock.calls).toEqual([[true], [false]]);
    expect(mockSetValue.mock.calls).toEqual([[null]]);
  });

  it('is second new Value', () => {
    jest.useFakeTimers();
    customFunctionCommon(chronicleSearchByChange, {
      newValue: { first: false, second: true },
      onFind: mockOnFind
    });
    jest.runAllTimers();
    expect(mockSetVisible.mock.calls).toEqual([[false], [true]]);
    expect(mockSetValue.mock.calls).toEqual([[null]]);
  });
});
