import { IValueChangeFunc } from 'app/_libraries/_dof/core';

/**
 * Function to check if text area field has non-allowed character,
 * then set message for alert field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const dateTypeMemoChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const dateTypeMemo = onFind('dateType');
  const specificDateMemo = onFind('memoDate');
  const dateRangeMemo = onFind('memoDateRange');
  // Handle error if close popup date immediately
  setTimeout(() => {
    if (dateTypeMemo === null) return;

    if (newValue?.dateRange) {
      dateRangeMemo?.props?.props?.setVisible(true);
      specificDateMemo?.props?.props?.setVisible(false);
      specificDateMemo?.props?.props?.setValue(null);
    } else {
      dateRangeMemo?.props?.props?.setVisible(false);
      specificDateMemo?.props?.props?.setVisible(true);
      dateRangeMemo?.props?.props?.setValue(null);
    }
  }, 300);
};
