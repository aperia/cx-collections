import { setErrorField } from 'app/helpers';
import { ExtraFieldProps, IValueChangeFunc } from 'app/_libraries/_dof/core';
import isEmpty from 'lodash.isempty';
import isNull from 'lodash.isnull';
import isUndefined from 'lodash.isundefined';
import { isLastDayMonth } from 'pages/HardShip/helpers';
import { Field } from 'redux-form';

/**
 * Function to check if text area field has non-allowed character,
 * then set message for alert field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */

export const lengthMonth = (from: string, to: string) => {
  const fromDate = new Date(`${from}`).getDate();
  const fromMonth = new Date(`${from}`).getMonth();
  const fromYear = new Date(`${from}`).getFullYear();
  const toDate = new Date(`${to}`).getDate();
  const toMonth = new Date(`${to}`).getMonth();
  const toYear = new Date(`${to}`).getFullYear();
  if (toMonth === fromMonth && toDate === fromDate) return 12;

  const lastDayFrom = isLastDayMonth(fromDate, fromMonth + 1, fromYear);
  const lastDayTo = isLastDayMonth(toDate, toMonth + 1, toYear);

  // fromMonth > toMonth =>  toYear > fromYear
  if (fromMonth > toMonth) {
    if (lastDayFrom && lastDayTo) {
      return toMonth + 12 - fromMonth - 3;
    }
    return fromDate > toDate
      ? toMonth + 12 - fromMonth - 1 - 3
      : toMonth + 12 - fromMonth - 3;
  }

  // fromMonth < toMonth
  // toYear === fromYear
  if (lastDayFrom && lastDayTo) {
    return toMonth - fromMonth - 3;
  }
  return fromDate > toDate
    ? toMonth - fromMonth - 1 - 3
    : toMonth - fromMonth - 3;
};

export const hardshipToPaymentChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const toPaymentRef = onFind('toPaymentDate') as MagicKeyValue;
  const manualType = onFind('hardshipTypeOfReage') as MagicKeyValue;

  const valueManualType = manualType?.ref?.current?.props?.value;

  if (!isUndefined(newValue) && !isNull(newValue)) {
    setErrorField(toPaymentRef as Field<ExtraFieldProps>, false);

    if (!isEmpty(manualType)) {
      const manualRef = onFind('manualReage') as MagicKeyValue;
      const valueManual = manualRef?.ref?.current?.props?.value;
      const periodRef = onFind('reagePaymentPeriod') as MagicKeyValue;
      const valuePeriod = periodRef?.ref?.current?.props?.value;
      if (valueManualType === 'Manual Reage') {
        periodRef?.props?.props?.setRequired(false);
        if (!isNull(valueManual) && !isUndefined(valueManual)) {
          manualRef?.props?.props?.setValue(null);
        }
      } else {
        manualRef?.props?.props?.setRequired(false);
        if (!isNull(valuePeriod) && !isUndefined(valuePeriod)) {
          const fromPaymentRef = onFind('fromPaymentDate') as MagicKeyValue;
          const valueFromPayment = fromPaymentRef?.ref?.current?.props?.value;
          const newTime = new Date(`${newValue}`).getTime();
          const periodTime = new Date(`${previousValue}`).getTime();
          // newValue > previousValue
          if (newTime > periodTime) return;
          // check length month
          const length = lengthMonth(`${valueFromPayment}`, `${newValue}`);
          if (length >= parseInt(valuePeriod?.value) - 3) return;
          periodRef?.props?.props?.setValue({});
        }
      }
    } else {
      const fromPaymentRef = onFind('fromPaymentDate') as MagicKeyValue;
      const valueFromPayment = fromPaymentRef?.ref?.current?.props?.value;

      if (!isNull(valueFromPayment) && !isUndefined(valueFromPayment)) {
        const periodRef = onFind('reagePaymentPeriod') as MagicKeyValue;
        const valuePeriod = periodRef?.ref?.current?.props?.value;
        const newTime = new Date(`${newValue}`).getTime();
        const periodTime = new Date(`${previousValue}`).getTime();
        if (!isNull(valuePeriod) && !isUndefined(valuePeriod)) {
          // newValue > previousValue
          if (newTime > periodTime) return;
          // check length month
          const length = lengthMonth(`${valueFromPayment}`, `${newValue}`);
          if (length >= parseInt(valuePeriod?.value) - 3) return;
          periodRef?.props?.props?.setValue({});
          periodRef?.props?.props?.setRequired(true);
        }
      }
    }
  }
};
