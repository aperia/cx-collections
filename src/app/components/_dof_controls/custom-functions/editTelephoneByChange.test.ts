import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { editTelephoneByChange } from './editTelephoneByChange';

const mockSetValue = jest.fn();
let mockOnFind: jest.SpyInstance;

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setValue: mockSetValue,
    dependentField: ['dependentField 1', 'dependentField 2']
  });
});

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('editTelephoneByChange custom function', () => {
  it('should return undefined when dependentField empty', () => {
    const result = customFunctionCommon(editTelephoneByChange);
    expect(mockSetValue).not.toBeCalled();
    expect(result).toEqual(undefined);
  });

  it('empty new value', () => {
    customFunctionCommon(editTelephoneByChange, { onFind: mockOnFind });
    expect(mockSetValue.mock.calls).toEqual([[null], [null]]);
  });

  it('new value length less than 10', () => {
    customFunctionCommon(editTelephoneByChange, {
      newValue: '01234',
      onFind: mockOnFind
    });
    expect(mockSetValue.mock.calls).toEqual([[null], [null]]);
  });

  it('new value length greater than 10', () => {
    customFunctionCommon(editTelephoneByChange, {
      newValue: '01234567890',
      onFind: mockOnFind
    });
    expect(mockSetValue).not.toBeCalled();
  });
});
