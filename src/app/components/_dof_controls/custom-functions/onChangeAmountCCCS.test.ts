import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { onChangeAmountCCCS } from './onChangeAmountCCCS';

const mockSetValue = jest.fn();
let mockOnFind: jest.SpyInstance;

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setValue: mockSetValue,
    dependentField: ['dependentField 1', 'dependentField 2']
  });
});

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('onChangeAmountCCCS custom function', () => {
  it('should return undefined when newValue empty', () => {
    const result = customFunctionCommon(onChangeAmountCCCS, {
      newValue: undefined,
      onFind: mockOnFind
    });
    expect(mockSetValue).not.toBeCalled();
    expect(result).toEqual(undefined);
  });

  it('payment amount < 0', () => {
    customFunctionCommon(onChangeAmountCCCS, {
      newValue: 7,
      formValues: { cycleToDatePaymentAmount: 5 },
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith(null);
  });

  it('payment amount > 0', () => {
    customFunctionCommon(onChangeAmountCCCS, {
      newValue: 5,
      formValues: { cycleToDatePaymentAmount: 7 },
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith(2);
  });

  it('payment amount = 0', () => {
    const result = customFunctionCommon(onChangeAmountCCCS, {
      newValue: -1,
      formValues: { cycleToDatePaymentAmount: 0 },
      onFind: mockOnFind
    });
    expect(result).toEqual(undefined);
  });
});
