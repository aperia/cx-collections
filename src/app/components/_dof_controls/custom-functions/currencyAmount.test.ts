import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { amountChange } from './currencyAmount';

const mockSetValue = jest.fn();
const mockSetOthers = jest.fn();
let mockOnFind: jest.SpyInstance;
const mockPrevState = { options: {} };

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setValue: mockSetValue,
    setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState))
  });
});

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockSetOthers.mockReset();
  mockSetOthers.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('amountChange custom function', () => {
  const mockOnFindDefault = {
    setValue: mockSetValue,
    setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState))
  };
  it('should set value null when newValue empty', () => {
    customFunctionCommon(amountChange, {
      newValue: undefined,
      onFind: mockOnFind
    });
    expect(mockSetValue).not.toBeCalled();
    expect(mockSetOthers).not.toBeCalled();
  });

  it('isChangeMPD equal false', () => {
    customFunctionCommon(amountChange, {
      name: 'nonDelinquentMpdAmount',
      newValue: '',
      previousValue: '2',
      onFind: jest
        .fn()
        .mockImplementationOnce(mockOnFindGenerator(mockOnFindDefault))
        .mockImplementationOnce(mockOnFindGenerator(mockOnFindDefault))
        .mockImplementationOnce(jest.fn())
        .mockImplementationOnce(jest.fn())
    });
    expect(mockSetValue).toBeCalledWith('NaN');
    expect(mockSetOthers.mock.calls).toEqual([
      [{ forceInvalid: false, options: {} }],
      [{ forceInvalid: false, options: {} }]
    ]);
  });

  it('change value', () => {
    customFunctionCommon(amountChange, {
      name: 'hardship_nonDelinquentMpdAmount',
      newValue: '3',
      previousValue: '2',
      onFind: jest
        .fn()
        .mockImplementationOnce(mockOnFindGenerator(mockOnFindDefault))
        .mockImplementationOnce(mockOnFindGenerator(mockOnFindDefault))
        .mockImplementationOnce(
          mockOnFindGenerator({
            value: '5',
            setValue: mockSetValue,
            setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState))
          })
        )
        .mockImplementationOnce(
          mockOnFindGenerator({
            value: '2',
            setValue: mockSetValue,
            setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState))
          })
        )
    });
    expect(mockSetValue).toBeCalledWith('2.00');
    expect(mockSetOthers.mock.calls).toEqual([
      [{ forceInvalid: false, options: {} }],
      [{ forceInvalid: false, options: {} }]
    ]);
  });

  it('next value less than 0', () => {
    customFunctionCommon(amountChange, {
      name: 'hardship_nonDelinquentMpdAmount',
      newValue: '3',
      previousValue: '2',
      onFind: jest
        .fn()
        .mockImplementationOnce(mockOnFindGenerator(mockOnFindDefault))
        .mockImplementationOnce(mockOnFindGenerator(mockOnFindDefault))
        .mockImplementationOnce(
          mockOnFindGenerator({
            value: '2',
            setValue: mockSetValue,
            setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState))
          })
        )
        .mockImplementationOnce(
          mockOnFindGenerator({
            value: '4',
            setValue: mockSetValue,
            setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState))
          })
        )
    });
    expect(mockSetValue).toBeCalledWith('');
    expect(mockSetOthers.mock.calls).toEqual([
      [
        {
          forceInvalid: true,
          meta: { invalid: true, error: 'txt_error_mpd_payment' },
          options: {}
        }
      ],
      [
        {
          forceInvalid: true,
          meta: {
            invalid: true,
            error:
              'New Fixed Payment Amount must be equal to or greater than minimum payment amount $4.00.'
          },
          options: {}
        }
      ]
    ]);
  });

  it('mdpValue is empty', () => {
    customFunctionCommon(amountChange, {
      name: 'nonDelinquentMpdAmount',
      newValue: '3',
      previousValue: '2',
      onFind: jest
        .fn()
        .mockImplementationOnce(mockOnFindGenerator(mockOnFindDefault))
        .mockImplementationOnce(mockOnFindGenerator(mockOnFindDefault))
        .mockImplementationOnce(
          mockOnFindGenerator({
            value: '1',
            setValue: mockSetValue,
            setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState))
          })
        )
        .mockImplementationOnce(jest.fn())
    });
    expect(mockSetValue).toBeCalledWith('');
    expect(mockSetOthers.mock.calls).toEqual([
      [
        {
          forceInvalid: true,
          options: {},
          meta: { error: 'txt_error_mpd', invalid: true }
        }
      ],
      [
        {
          forceInvalid: true,
          options: {},
          meta: { error: 'txt_error_new_fixed_payment', invalid: true }
        }
      ]
    ]);
  });
});
