import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { hardshipDeploymentStartDateChange } from './hardshipDeploymentStartDateChange';

const mockSetOthers = jest.fn();
let mockOnFind: jest.SpyInstance;

afterEach(() => {
  mockSetOthers.mockReset();
  mockSetOthers.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('hardshipDeploymentEndDateChange', () => {
  it('has valueEnd', () => {
    mockOnFind = mockOnFindGenerator({
      setOthers: mockSetOthers,
      value: 'has Value'
    });
    customFunctionCommon(hardshipDeploymentStartDateChange, {
      newValue: new Date(),
      onFind: mockOnFind
    });
    expect(mockSetOthers).toBeCalledTimes(1);
  });

  it('has no valueEnd', () => {
    mockOnFind = mockOnFindGenerator({
      setOthers: mockSetOthers
    });
    customFunctionCommon(hardshipDeploymentStartDateChange, {
      newValue: undefined,
      onFind: mockOnFind
    });
    expect(mockSetOthers).toBeCalledTimes(0);
  });
});
