import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault, handleOnFind } from 'app/helpers';
import { IValueChangeFunc } from 'app/_libraries/_dof/core';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import { ID_ADDRESS_TYPE } from 'pages/AccountDetails/CardHolderMaintenance/ExpandedAddress/constants';

export const onChangeAddressType: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  if (isEqual(newValue, previousValue) || isEmpty(newValue)) return;
  const addressCategory = formValues?.addressCategory;
  if (!addressCategory?.fieldID) return;
  let readOnly = false;
  const addressEffectiveTo = handleOnFind('effectiveTo', onFind);
  const addressEffectiveFrom = handleOnFind('effectiveFrom', onFind);
  const effectiveToValue = formValues?.effectiveTo;
  const effectiveFromValue = formValues?.effectiveFrom;
  const effectToFormat = effectiveToValue
    ? formatTimeDefault(effectiveToValue, FormatTime.YearMonthDay)
    : null;
  const effectFromFormat = effectiveFromValue
    ? formatTimeDefault(effectiveFromValue, FormatTime.YearMonthDay)
    : null;
  const { PERMANENT } = ID_ADDRESS_TYPE;
  if (addressCategory?.fieldID === PERMANENT) {
    readOnly = true;
    addressEffectiveTo.props.setValue('12/31/9999');
    effectFromFormat && addressEffectiveFrom.props.setValue(undefined);
  } else {
    effectToFormat && addressEffectiveTo.props.setValue(undefined);
    effectFromFormat && addressEffectiveFrom.props.setValue(undefined);
  }
  addressEffectiveTo.props.setReadOnly(readOnly);
};
