import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { hardshipDeploymentEndDateChange } from './hardshipDeploymentEndDateChange';

const mockSetOthers = jest.fn();
let mockOnFind: jest.SpyInstance;

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setOthers: mockSetOthers
  });
});

afterEach(() => {
  mockSetOthers.mockReset();
  mockSetOthers.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('hardshipDeploymentEndDateChange', () => {
  it('newValue is valid', () => {
    customFunctionCommon(hardshipDeploymentEndDateChange, {
      newValue: new Date(),
      onFind: mockOnFind
    });
    expect(mockSetOthers).toBeCalledTimes(1);
  });

  it('newValue is invalid', () => {
    customFunctionCommon(hardshipDeploymentEndDateChange, {
      newValue: undefined,
      onFind: mockOnFind
    });
    expect(mockSetOthers).toBeCalledTimes(0);
  });
});
