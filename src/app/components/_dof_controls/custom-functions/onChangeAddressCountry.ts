import { handleOnFind } from 'app/helpers';
import { IValueChangeFunc } from 'app/_libraries/_dof/core';
import isEmpty from 'lodash.isempty';
import { getStateSubdivision } from 'pages/AccountDetails/CardHolderMaintenance/ExpandedAddress/getStateSubdivision';

export const onChangeAddressCountry: IValueChangeFunc = async (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  if (isEmpty(newValue)) return;
  if (newValue?.fieldID === previousValue?.fieldID) return;
  const postalCodeValue = formValues.postalCode;
  let maxLength = 5;
  let validation = "^[^&',-/.]+$";
  let errorMsg = 'txt_postal_code_not_contain';
  const addressStateSubdivision = handleOnFind('stateSubdivision', onFind);
  const postalCode = handleOnFind('postalCode', onFind);
  if (newValue?.fieldID === 'CAN') {
    maxLength = 10;
    validation = "^[^&',/.]+$";
    errorMsg = 'txt_postal_code_contain_hyphen';
  }

  postalCodeValue && postalCode?.props.setValue(null);

  postalCode?.props.setOthers((pre: MagicKeyValue) => ({
    ...pre,
    options: {
      ...pre.options,
      maxLength
    },
    validationRules: [
      { ...pre?.validationRules[0] },
      {
        ruleName: 'RegExp',
        errorMsg,
        ruleOptions: {
          regExp: validation
        }
      }
    ]
  }));

  addressStateSubdivision.props.setValue(null);
  const data = await getStateSubdivision(newValue?.fieldID);
  addressStateSubdivision.props.setData(data);
  addressStateSubdivision.props.setReadOnly(false);
};
