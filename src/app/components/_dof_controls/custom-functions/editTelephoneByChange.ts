import { IValueChangeFunc } from 'app/_libraries/_dof/core';
import forEach from 'lodash.foreach';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import { dataField } from 'pages/AccountDetails/CardHolderMaintenance/TelePhone/helpers';

/**
 * Function to check if text area field has non-allowed character,
 * then set message for alert field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const editTelephoneByChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const nameField = dataField(name);

  const phoneRef = onFind(nameField);

  const dependentField = get(
    phoneRef,
    ['props', 'props', 'dependentField'],
    []
  );

  if (isEmpty(dependentField)) return;

  if (isEmpty(newValue) || newValue.length < 10) {
    forEach(dependentField, text => {
      const fieldRef = onFind(text);
      fieldRef?.props?.props?.setValue(null);
    });
  }
};
