import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault, handleOnFind } from 'app/helpers';
import { IValueChangeFunc } from 'app/_libraries/_dof/core';

export const isReAgePaymentPeriodNone = (reAgePaymentPeriod: any): boolean => {
  return (
    reAgePaymentPeriod === null ||
    reAgePaymentPeriod == '0' ||
    reAgePaymentPeriod?.value == '0'
  );
};

export const handleEndDateUI = (
  endDate: any,
  endDateValue: any,
  reAgePaymentPeriod: any
) => {
  if (isReAgePaymentPeriodNone(reAgePaymentPeriod)) {
    endDate?.props?.setReadOnly(false);
    endDate?.props.setOthers((pre: MagicKeyValue) => ({
      ...pre,
      options: {
        ...pre?.options,
        min: formatTimeDefault(endDateValue, FormatTime.Date),
        minDateDisabledText: 'txt_min_end_date_if_reAgePaymentPeriod_none_cccs'
      }
    }));
  } else {
    endDate?.props?.setReadOnly(true);
  }
};

export const updateEndDateByReAgePaymentPeriod = (
  endDate: any,
  reAgePaymentPeriod: any | RefDataValue,
  updateFromOnChange = false as boolean,
  endDateValue: Date
) => {
  const { value } = reAgePaymentPeriod;
  if (!endDate) return;

  if (updateFromOnChange) {
    endDateValue.setMonth(
      isReAgePaymentPeriodNone(reAgePaymentPeriod)
        ? endDateValue.getMonth() + 1
        : endDateValue.getMonth() + value * 1
    );

    endDate?.props?.setValue(new Date(endDateValue));
  }

  handleEndDateUI(endDate, endDateValue, reAgePaymentPeriod);
};

export const onChangeReagePaymentPeriod: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  if (!newValue) return;
  const endDate = handleOnFind('endDate', onFind);

  const startDate = formValues['startDate'] || new Date();
  const copiedStartDate = new Date(startDate.getTime());

  updateEndDateByReAgePaymentPeriod(endDate, newValue, true, copiedStartDate);
};
