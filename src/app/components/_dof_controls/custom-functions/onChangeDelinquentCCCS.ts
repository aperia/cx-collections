import { IValueChangeFunc, ExtraFieldProps } from 'app/_libraries/_dof/core';

export const onChangeDelinquentCCCS: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  if (!newValue) return;
  const cycleToDatePaymentAmount = Number(formValues?.cycleToDatePaymentAmount);
  let calculationAmount = 0;
  const paymentAmountProps: ExtraFieldProps = onFind('paymentAmount').props;
  calculationAmount = cycleToDatePaymentAmount - Number(newValue);
  if (calculationAmount < 0) {
    return paymentAmountProps.props.setValue(null);
  }
  paymentAmountProps.props.setValue(calculationAmount);
  if (calculationAmount < formValues?.minimumAmount) {
    return paymentAmountProps.props.setOthers((prevState: MagicKeyValue) => ({
      ...prevState,
      forceInvalid: true
    }));
  }

  paymentAmountProps.props.setValue(calculationAmount);
};
