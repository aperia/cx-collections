import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { hardshipGroupCountryOfDeploymentChange } from './hardshipGroupCountryOfDeploymentChange';

const mockSetEnable = jest.fn();
let mockOnFind: jest.SpyInstance;

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setEnable: mockSetEnable
  });
});

afterEach(() => {
  mockSetEnable.mockReset();
  mockSetEnable.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('hardshipGroupCountryOfDeploymentChange', () => {
  it('set Enable true', () => {
    customFunctionCommon(hardshipGroupCountryOfDeploymentChange, {
      newValue: undefined,
      onFind: mockOnFind
    });
    expect(mockSetEnable).toBeCalledWith(true);
  });

  it('set Enable false', () => {
    customFunctionCommon(hardshipGroupCountryOfDeploymentChange, {
      newValue: 'confidential',
      onFind: mockOnFind
    });
    expect(mockSetEnable).toBeCalledWith(false);
  });
});
