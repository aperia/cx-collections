import { generateDateObject } from 'app/helpers';
import { IValueChangeFunc, ExtraFieldProps } from 'app/_libraries/_dof/core';

export const handleDateNoticeReceivedField = (
  dateOfDeath: string,
  dateNoticeReceived: string,
  deceasedDateNoticeReceivedProps: ExtraFieldProps,
  isEnable: boolean
) => {
  if (deceasedDateNoticeReceivedProps?.props) {
    const isClearDateNoticeReceived =
      generateDateObject(dateOfDeath) > generateDateObject(dateNoticeReceived);
    // If date of death is empty or date of death > date notice received
    // Clear date notice received value
    if (!isEnable || isClearDateNoticeReceived) {
      deceasedDateNoticeReceivedProps?.props.setValue(null);
    }
    // Set min date for date notice received
    // min date of date notice received = date of death
    deceasedDateNoticeReceivedProps.props.setOthers(
      (previousValue: MagicKeyValue) => ({
        ...previousValue,
        options: {
          ...previousValue?.options,
          min: dateOfDeath
        }
      })
    );
    // Set date notice received enable or disable
    deceasedDateNoticeReceivedProps.props.setEnable(isEnable);
  }
};

/**
 * Function to check if recurring field was changed,
 * then set enabled/disabled, empty for interval and count field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const deceasedDateOfDeath: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const deceasedDateNoticeReceived = onFind('deceasedDateNoticeReceived');

  const deceasedDateNoticeReceivedProps: ExtraFieldProps =
    deceasedDateNoticeReceived.props;

  const isEnable = !!newValue;

  handleDateNoticeReceivedField(
    newValue,
    formValues?.deceasedDateNoticeReceived,
    deceasedDateNoticeReceivedProps,
    isEnable
  );
};
