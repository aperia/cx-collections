import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { hardshipReageTypeChange } from './hardshipReageTypeChange';

const mockSetValue = jest.fn();
let mockOnFind: jest.SpyInstance;

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('hardshipReageTypeChange', () => {
  describe('newValue !== Manual Reage', () => {
    it('should set value object', () => {
      mockOnFind = mockOnFindGenerator({
        setValue: mockSetValue,
        value: 'Manual Reage'
      });
      customFunctionCommon(hardshipReageTypeChange, {
        newValue: undefined,
        onFind: mockOnFind
      });
      expect(mockSetValue).toBeCalledWith({});
    });

    it('should not be call set value', () => {
      mockOnFind = mockOnFindGenerator({
        setValue: mockSetValue
      });
      customFunctionCommon(hardshipReageTypeChange, {
        newValue: undefined,
        onFind: mockOnFind
      });
      expect(mockSetValue).not.toBeCalled();
    });
  });

  describe('newValue === Manual Reage', () => {
    it('should be set null', () => {
      mockOnFind = mockOnFindGenerator({
        setValue: mockSetValue,
        value: 'Manual Reage'
      });
      customFunctionCommon(hardshipReageTypeChange, {
        newValue: 'Manual Reage',
        onFind: mockOnFind
      });
      expect(mockSetValue).toBeCalledWith(null);
    });

    it('should not be call set value', () => {
      mockOnFind = mockOnFindGenerator({
        setValue: mockSetValue
      });
      customFunctionCommon(hardshipReageTypeChange, {
        newValue: 'Manual Reage',
        onFind: mockOnFind
      });
      expect(mockSetValue).not.toBeCalled();
    });
  });
});
