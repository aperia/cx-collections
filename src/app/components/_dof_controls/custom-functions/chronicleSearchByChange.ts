import { IValueChangeFunc } from 'app/_libraries/_dof/core';
import { SearchMemoChronicleDataField } from 'pages/Memos/Chronicle/constants';

/**
 * Function to check if text area field has non-allowed character,
 * then set message for alert field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const chronicleSearchByChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const searchByRadios = onFind(SearchMemoChronicleDataField.SEARCH_BY_RADIO);
  const memoGroup = onFind(SearchMemoChronicleDataField.MEMO_SEARCH_GROUP);
  const memoType = onFind(SearchMemoChronicleDataField.MEMO_SEARCH_TYPE);
  setTimeout(() => {
    if (searchByRadios === null) return;

    if (newValue?.first) {
      memoGroup?.props?.props?.setVisible(true);
      memoType?.props?.props?.setVisible(false);
      memoType?.props?.props?.setValue(null);
    } else {
      memoGroup?.props?.props?.setVisible(false);
      memoGroup?.props?.props?.setValue(null);
      memoType?.props?.props?.setVisible(true);
    }
  }, 300);
};
