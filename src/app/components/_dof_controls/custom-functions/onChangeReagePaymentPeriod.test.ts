import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import {
  onChangeReagePaymentPeriod,
  updateEndDateByReAgePaymentPeriod
} from './onChangeReagePaymentPeriod';

const mockSetValue = jest.fn();
let mockOnFind: jest.SpyInstance;

afterEach(() => {
  mockSetValue?.mockReset();
  mockSetValue?.mockRestore();
  mockOnFind?.mockReset();
  mockOnFind?.mockRestore();
});

describe('updateEndDateByReAgePaymentPeriod', () => {
  const setReadOnly = jest.fn();
  const setOthers = jest.fn();
  const setValue = jest.fn();

  const endDate = {
    props: {
      setReadOnly,
      setValue,
      setOthers: (cb: Function) => {
        cb && cb();
        setOthers();
      }
    }
  };

  beforeEach(() => {
    setReadOnly?.mockClear();
    setOthers?.mockClear();
    setValue?.mockClear();
  });

  it('when endDate is undefined', () => {
    updateEndDateByReAgePaymentPeriod(undefined, '0', true, new Date());

    expect(setReadOnly).not.toBeCalled();
    expect(setOthers).not.toBeCalled();
    expect(setValue).not.toBeCalled();
  });

  it('when reAgePaymentPeriod is undefined', () => {
    updateEndDateByReAgePaymentPeriod(undefined, {}, true, new Date());

    expect(setReadOnly).not.toBeCalled();
    expect(setOthers).not.toBeCalled();
    expect(setValue).not.toBeCalled();
  });

  it('when updateFromOnChange is undefined', () => {
    updateEndDateByReAgePaymentPeriod(
      endDate,
      '0',
      undefined as unknown as boolean,
      new Date()
    );

    expect(setReadOnly).toBeCalled();
    expect(setOthers).toBeCalled();
    expect(setValue).not.toBeCalled();
  });

  it('when reAgePaymentPeriod is 0', () => {
    updateEndDateByReAgePaymentPeriod(endDate, '0', true, new Date());

    expect(setReadOnly).toBeCalled();
    expect(setOthers).toBeCalled();
    expect(setValue).toBeCalled();
  });

  it('when reAgePaymentPeriod is 10', () => {
    updateEndDateByReAgePaymentPeriod(endDate, '10', true, new Date());

    expect(setReadOnly).toBeCalled();
    expect(setOthers).not.toBeCalled();
    expect(setValue).toBeCalled();
  });
});

describe('onChangeReagePaymentPeriod', () => {
  it('should return undefined when newValue is empty string', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      value: '12/31/2000'
    });
    const result = customFunctionCommon(onChangeReagePaymentPeriod, {
      newValue: '',
      onFind: mockOnFind
    });
    expect(mockSetValue).not.toBeCalled();
    expect(result).toEqual(undefined);
  });

  it('should return undefined when newValue equal 0', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      value: '12/31/2000'
    });
    customFunctionCommon(onChangeReagePaymentPeriod, {
      newValue: {
        value: 0
      },
      onFind: mockOnFind,
      formValues: {
        startDate: new Date()
      }
    });

    expect(mockSetValue).toBeCalled();
  });

  it('should return data when newValue equal 10', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      value: '12/31/2000'
    });
    customFunctionCommon(onChangeReagePaymentPeriod, {
      newValue: {
        value: 10
      },
      formValues: {
        startDate: new Date()
      },
      onFind: mockOnFind
    });

    expect(mockSetValue).toBeCalled();
  });

  it('should return data when newValue equal 10 & formValues.startDate is undefined', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      value: '12/31/2000'
    });
    customFunctionCommon(onChangeReagePaymentPeriod, {
      newValue: {
        value: 10
      },
      formValues: {
        startDate: undefined
      },
      onFind: mockOnFind
    });

    expect(mockSetValue).toBeCalled();
  });
});
