import { IValueChangeFunc } from 'app/_libraries/_dof/core';
import isNull from 'lodash.isnull';
import isUndefined from 'lodash.isundefined';

/**
 * Function to check if text area field has non-allowed character,
 * then set message for alert field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const hardshipReageTypeChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const periodRef = onFind('reagePaymentPeriod') as MagicKeyValue;
  const valuePeriod = periodRef?.ref?.current?.props?.value;
  const manualRef = onFind('manualReage') as MagicKeyValue;
  const valueManual = manualRef?.ref?.current?.props?.value;
  if (newValue === 'Manual Reage') {
    periodRef?.props?.props?.setRequired(false);
    manualRef?.props?.props?.setRequired(true);
    if (!isNull(valueManual) && !isUndefined(valueManual)) {
      manualRef?.props?.props?.setValue(null);
    }
  } else {
    manualRef?.props?.props?.setRequired(false);
    periodRef?.props?.props?.setRequired(true);
    if (!isNull(valuePeriod) && !isUndefined(valuePeriod)) {
      periodRef?.props?.props?.setValue({});
    }
  }
};