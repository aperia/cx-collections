import { setErrorField } from 'app/helpers';
import { ExtraFieldProps, IValueChangeFunc } from 'app/_libraries/_dof/core';
import isEmpty from 'lodash.isempty';
import isNull from 'lodash.isnull';
import isUndefined from 'lodash.isundefined';
import { Field } from 'redux-form';

/**
 * Function to check if text area field has non-allowed character,
 * then set message for alert field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const hardshipFromPaymentChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const toPaymentRef = onFind('toPaymentDate') as MagicKeyValue;
  const valuePayment = toPaymentRef?.ref?.current?.props?.value;
  const manualType = onFind('hardshipTypeOfReage') as MagicKeyValue;
  const valueManualType = manualType?.ref?.current?.props?.value;

  if (!isNull(valuePayment) && !isUndefined(valuePayment)) {
    toPaymentRef?.props?.props?.setValue(null);
    setErrorField(toPaymentRef as Field<ExtraFieldProps>, true);
  }

  if (!isEmpty(manualType)) {
    const manualRef = onFind('manualReage') as MagicKeyValue;
    const valueManual = manualRef?.ref?.current?.props?.value;
    const periodRef = onFind('reagePaymentPeriod') as MagicKeyValue;
    const valuePeriod = periodRef?.ref?.current?.props?.value;
    if (valueManualType === 'Manual Reage') {
      periodRef?.props?.props?.setRequired(false);
      if (!isNull(valueManual) && !isUndefined(valueManual)) {
        manualRef?.props?.props?.setValue(null);
      }
    } else {
      manualRef?.props?.props?.setRequired(false);
      if (!isNull(valuePeriod) && !isUndefined(valuePeriod)) {
        periodRef?.props?.props?.setValue({});
      }
    }
  } else {
    if (!isNull(valuePayment) && !isUndefined(valuePayment)) {
      const periodRef = onFind('reagePaymentPeriod') as MagicKeyValue;
      const valuePeriod = periodRef?.ref?.current?.props?.value;
  
      if (!isNull(valuePeriod) && !isUndefined(valuePeriod)) {
        periodRef?.props?.props?.setValue({});
        periodRef?.props?.props?.setRequired(false);
      }
    }
  }
};
