import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { hardshipFromPaymentChange } from './hardshipFromPaymentChange';

const mockSetValue = jest.fn();
const mockSetRequired = jest.fn();
let mockOnFind: jest.SpyInstance;

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('hardshipFromPaymentChange', () => {
  describe('has manual type', () => {
    it('should set value null', () => {
      mockOnFind = mockOnFindGenerator({
        setValue: mockSetValue,
        setRequired: mockSetRequired,
        value: 'Manual Reage'
      });
      customFunctionCommon(hardshipFromPaymentChange, {
        newValue: undefined,
        onFind: mockOnFind
      });
      expect(mockSetRequired).toBeCalledWith(false);
      expect(mockSetValue).toBeCalledWith(null);
    });

    it('should not be call set value', () => {
      const _mockOnFindGenerator = (value?: any) =>
        mockOnFindGenerator({
          setRequired: mockSetRequired,
          setValue: mockSetValue,
          value
        });
      customFunctionCommon(hardshipFromPaymentChange, {
        newValue: undefined,
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator()) //toPaymentDate
          .mockImplementationOnce(_mockOnFindGenerator('Manual Reage')) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator()) //manualReage
          .mockImplementationOnce(_mockOnFindGenerator()) //reagePaymentPeriod
      });
      expect(mockSetValue).not.toBeCalled();
    });

    it('valueManualType not equal Manual Reage', () => {
      const _mockOnFindGenerator = (value?: any) =>
        mockOnFindGenerator({
          setRequired: mockSetRequired,
          setValue: mockSetValue,
          value
        });
      customFunctionCommon(hardshipFromPaymentChange, {
        newValue: undefined,
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator()) //toPaymentDate
          .mockImplementationOnce(
            _mockOnFindGenerator(' not equal Manual Reage')
          ) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator()) //manualReage
          .mockImplementationOnce(_mockOnFindGenerator()) //reagePaymentPeriod
      });
      expect(mockSetValue).not.toBeCalled();
    });

    it('should be setValue empty object', () => {
      const _mockOnFindGenerator = (value?: any) =>
        mockOnFindGenerator({
          setRequired: mockSetRequired,
          setValue: mockSetValue,
          value
        });
      customFunctionCommon(hardshipFromPaymentChange, {
        newValue: undefined,
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator()) //toPaymentDate
          .mockImplementationOnce(
            _mockOnFindGenerator(' not equal Manual Reage')
          ) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator()) //manualReage
          .mockImplementationOnce(_mockOnFindGenerator('has value')) //reagePaymentPeriod
      });
      expect(mockSetValue).toBeCalledWith({});
    });

    it('empty manual type', () => {
      const _mockOnFindGenerator = (value?: any) =>
        mockOnFindGenerator({
          setRequired: mockSetRequired,
          setValue: mockSetValue,
          value
        });
      customFunctionCommon(hardshipFromPaymentChange, {
        newValue: undefined,
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator()) //toPaymentDate
          .mockImplementationOnce(() => ({})) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator()) //manualReage
          .mockImplementationOnce(_mockOnFindGenerator()) //reagePaymentPeriod
      });
      expect(mockSetValue).not.toBeCalled();
    });
  });

  describe('empty manualType', () => {
    it('has value payment', () => {
      const _mockOnFindGenerator = (value?: any) =>
        mockOnFindGenerator({
          setRequired: mockSetRequired,
          setValue: mockSetValue,
          value
        });
      customFunctionCommon(hardshipFromPaymentChange, {
        newValue: undefined,
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator('value payment')) //toPaymentDate
          .mockImplementationOnce(() => ({})) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator()) //manualReage
          .mockImplementationOnce(_mockOnFindGenerator()) //reagePaymentPeriod
      });
      expect(mockSetValue).toBeCalledWith(null);
    });

    it('has value period', () => {
      const _mockOnFindGenerator = (value?: any) =>
        mockOnFindGenerator({
          setRequired: mockSetRequired,
          setValue: mockSetValue,
          value
        });
      customFunctionCommon(hardshipFromPaymentChange, {
        newValue: undefined,
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator('value payment')) //toPaymentDate
          .mockImplementationOnce(() => ({})) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator('value')) //reagePaymentPeriod
      });
      expect(mockSetValue.mock.calls).toEqual([[null], [{}]]);
      expect(mockSetRequired).toBeCalledWith(false);
    });
  });
});
