import { CompareNumber } from './CompareNumber';

describe('MustNotGreaterThanValidator', () => {
  const mockOtherField = '3';
  const lessThanOtherField = '2';
  const greaterThanOtherField = '4';
  const mockErrorMsg = 'errorMessage';
  describe('validator', () => {
    it('empty constructor', () => {
      const compareNumberValidator = new CompareNumber();
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn()
        })
      ).toEqual(undefined);
    });

    it('return undefined', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'Equal',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn()
        })
      ).toEqual(undefined);
    });

    it('operator = Equal', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'Equal',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(greaterThanOtherField)
        })
      ).toEqual(`It must be greater or equal  to ${mockOtherField}`);
    });

    it('operator = Equal not error', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'Equal',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(mockOtherField)
        })
      ).toEqual(undefined);
    });

    it('operator = Less', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'Less',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(lessThanOtherField)
        })
      ).toEqual(`It must be less to ${mockOtherField}`);
    });

    it('operator = Less not error', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'Less',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(greaterThanOtherField)
        })
      ).toEqual(undefined);
    });

    it('operator = Greater', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'Greater',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(greaterThanOtherField)
        })
      ).toEqual(`It must be greater ${mockOtherField}`);
    });

    it('operator = Greater not error', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'Greater',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(lessThanOtherField)
        })
      ).toEqual(undefined);
    });

    it('operator = GreaterEqual', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'GreaterEqual',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(greaterThanOtherField)
        })
      ).toEqual(`It must be greater or equal ${mockOtherField}`);
    });

    it('operator = GreaterEqual not error', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'GreaterEqual',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(+mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(lessThanOtherField)
        })
      ).toEqual(undefined);
    });

    it('operator = LessEqual', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'LessEqual',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(lessThanOtherField)
        })
      ).toEqual(`It must be equal or less ${mockOtherField}`);
    });

    it('operator = LessEqual not error', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'LessEqual',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(greaterThanOtherField)
        })
      ).toEqual(undefined);
    });

    it('operator = undefined', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'undefined' as never,
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.validate(mockOtherField, {
          labelOf: jest.fn(),
          valueOf: jest.fn().mockReturnValue(greaterThanOtherField)
        })
      ).toEqual(undefined);
    });
  });

  describe('getFormatCurrency', () => {
    it('c2', () => {
      const compareNumberValidator = new CompareNumber();
      expect(
        compareNumberValidator.getFormatCurrency(mockOtherField, 'c2')
      ).toEqual(`$${mockOtherField}.00`);
    });

    it('c3', () => {
      const compareNumberValidator = new CompareNumber();
      expect(
        compareNumberValidator.getFormatCurrency(mockOtherField, 'c3')
      ).toEqual(`$${mockOtherField}.000`);
    });

    it('default', () => {
      const compareNumberValidator = new CompareNumber();
      expect(
        compareNumberValidator.getFormatCurrency(mockOtherField, undefined)
      ).toEqual(`$${mockOtherField}`);
    });
  });

  describe('getErrorMsgWithType', () => {
    it('has not operator and fieldToCompare', () => {
      const compareNumberValidator = new CompareNumber();
      expect(
        compareNumberValidator.getErrorMsgWithType(mockOtherField)
      ).toEqual(undefined);
    });

    it('field type = currency', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'Equal',
        fieldType: 'currency',
        fieldToCompare: mockOtherField
      });
      expect(
        compareNumberValidator.getErrorMsgWithType(mockOtherField)
      ).toEqual(`It must be greater or equal  to ${mockOtherField}`);
    });

    it('has return field compare', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'Equal',
        fieldType: 'currency',
        fieldToCompare: mockOtherField,
        isReturnFieldCompare: true
      });
      expect(
        compareNumberValidator.getErrorMsgWithType(mockOtherField)
      ).toEqual({
        count: `$${mockOtherField}`,
        messageCustom: `It must be greater or equal  to ${mockOtherField}`
      });
    });

    it('has type', () => {
      const compareNumberValidator = new CompareNumber({
        operator: 'Equal',
        fieldType: 'currency',
        fieldToCompare: mockOtherField,
        type: 'c2',
        errorMsg: mockErrorMsg
      });
      expect(
        compareNumberValidator.getErrorMsgWithType(mockOtherField)
      ).toEqual(`${mockErrorMsg} $${mockOtherField}.00.`);
    });
  });
});
