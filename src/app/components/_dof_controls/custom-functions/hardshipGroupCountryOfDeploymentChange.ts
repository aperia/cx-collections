import { IValueChangeFunc } from 'app/_libraries/_dof/core';

export const DISABLED_VALUE = 'confidential';

/**
 * Enable/disable countryOfDeployment if group checkbox check 'confidential',
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const hardshipGroupCountryOfDeploymentChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  event,
  formValues
) => {
  const hardshipCountryOfDeployment = onFind('countryOfDeployment');
  hardshipCountryOfDeployment?.props?.props?.setEnable(
    newValue !== DISABLED_VALUE
  );
};
