import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import {
  hardshipToPaymentChange,
  lengthMonth
} from './hardshipToPaymentChange';

const mockSetValue = jest.fn();
const mockSetOthers = jest.fn();
const mockSetRequired = jest.fn();

let mockOnFind: jest.SpyInstance;

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockSetOthers.mockReset();
  mockSetOthers.mockRestore();
  mockSetRequired.mockReset();
  mockSetRequired.mockRestore();
  mockOnFind?.mockReset();
  mockOnFind?.mockRestore();
});
describe('hardshipToPaymentChange custom function', () => {
  it('when toPayment Field is null', () => {
    const result = customFunctionCommon(hardshipToPaymentChange);

    expect(result).toBeUndefined();
  });

  describe('has manual type value', () => {
    it('value manual type equal manual reage and has new value', () => {
      mockOnFind = mockOnFindGenerator({
        setValue: mockSetValue,
        setOthers: mockSetOthers,
        setRequired: mockSetRequired,
        value: 'Manual Reage'
      });

      customFunctionCommon(hardshipToPaymentChange, {
        newValue: '12/31/2999',
        onFind: mockOnFind
      });

      expect(mockSetValue).toBeCalledWith(null);
      expect(mockSetRequired).toBeCalledWith(false);
    });

    it('value manual type is not equal manual reage ', () => {
      mockOnFind = mockOnFindGenerator({
        setValue: mockSetValue,
        setOthers: mockSetOthers,
        setRequired: mockSetRequired,
        value: 'any value'
      });

      customFunctionCommon(hardshipToPaymentChange, {
        newValue: '12/31/2999',
        onFind: mockOnFind
      });

      expect(mockSetValue).toBeCalledWith({});
      expect(mockSetRequired).toBeCalledWith(false);
    });

    it('newValue > previousValue', () => {
      mockOnFind = mockOnFindGenerator({
        setValue: mockSetValue,
        setOthers: mockSetOthers,
        setRequired: mockSetRequired,
        value: 'any'
      });

      customFunctionCommon(hardshipToPaymentChange, {
        newValue: '12/31/2999',
        previousValue: '12/30/2999',
        onFind: mockOnFind
      });

      expect(mockSetRequired).toBeCalledWith(false);
    });

    it('newValue < previousValue', () => {
      const _mockOnFindGenerator = (value?: string | Record<string, string>) =>
        mockOnFindGenerator({
          setValue: mockSetValue,
          setOthers: mockSetOthers,
          setRequired: mockSetRequired,
          value
        });

      customFunctionCommon(hardshipToPaymentChange, {
        newValue: '12/30/2999',
        previousValue: '12/31/2999',
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator('any value')) //toPaymentDate
          .mockImplementationOnce(_mockOnFindGenerator('any value')) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator('any value')) //manualReage
          .mockImplementationOnce(_mockOnFindGenerator({ value: '4' })) // reagePaymentPeriod
          .mockImplementationOnce(_mockOnFindGenerator('05/31/2999')) // fromPaymentDate
      });

      expect(mockSetRequired).toBeCalledWith(false);
    });

    it('value period is undefined', () => {
      const _mockOnFindGenerator = (value?: string | Record<string, string>) =>
        mockOnFindGenerator({
          setValue: mockSetValue,
          setOthers: mockSetOthers,
          setRequired: mockSetRequired,
          value
        });

      customFunctionCommon(hardshipToPaymentChange, {
        newValue: '12/30/2999',
        previousValue: '12/31/2999',
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator('any value')) //toPaymentDate
          .mockImplementationOnce(_mockOnFindGenerator('any value')) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator('any value')) //manualReage
          .mockImplementationOnce(_mockOnFindGenerator()) // reagePaymentPeriod
          .mockImplementationOnce(_mockOnFindGenerator('05/31/2999')) // fromPaymentDate
      });

      expect(mockSetRequired).toBeCalledWith(false);
    });

    it('valueManual is undefined', () => {
      const _mockOnFindGenerator = (value?: string | Record<string, string>) =>
        mockOnFindGenerator({
          setValue: mockSetValue,
          setOthers: mockSetOthers,
          setRequired: mockSetRequired,
          value
        });

      customFunctionCommon(hardshipToPaymentChange, {
        newValue: '12/30/2999',
        previousValue: '12/31/2999',
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator('any value')) //toPaymentDate
          .mockImplementationOnce(_mockOnFindGenerator('Manual Reage')) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator()) //manualReage
          .mockImplementationOnce(_mockOnFindGenerator()) // reagePaymentPeriod
          .mockImplementationOnce(_mockOnFindGenerator('05/31/2999')) // fromPaymentDate
      });

      expect(mockSetRequired).toBeCalledWith(false);
    });
  });

  describe('empty manual type', () => {
    it('should set required', () => {
      const mOnFind = mockOnFindGenerator({
        setValue: mockSetValue,
        setOthers: mockSetOthers,
        setRequired: mockSetRequired,
        value: 'has value'
      });

      customFunctionCommon(hardshipToPaymentChange, {
        onFind: jest
          .fn()
          .mockImplementationOnce(mOnFind)
          .mockImplementationOnce(() => ({}))
          .mockImplementationOnce(mOnFind)
          .mockImplementationOnce(mOnFind),
        newValue: '12/31/2999'
      });

      expect(mockSetRequired).toBeCalledWith(true);
    });

    it('length >= parseInt(valuePeriod?.value) - 3', () => {
      const _mockOnFindGenerator = (value?: string | Record<string, string>) =>
        mockOnFindGenerator({
          setValue: mockSetValue,
          setOthers: mockSetOthers,
          setRequired: mockSetRequired,
          value
        });

      customFunctionCommon(hardshipToPaymentChange, {
        newValue: '12/30/2999',
        previousValue: '12/31/2999',
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator('any value')) //toPaymentDate
          .mockImplementationOnce(() => ({})) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator('05/31/2999')) //fromPaymentDate
          .mockImplementationOnce(_mockOnFindGenerator({ value: '4' })) // reagePaymentPeriod
      });

      expect(mockSetRequired).not.toBeCalled();
      expect(mockSetValue).not.toBeCalled();
    });

    it('new Time > previous time', () => {
      const _mockOnFindGenerator = (value?: string | Record<string, string>) =>
        mockOnFindGenerator({
          setValue: mockSetValue,
          setOthers: mockSetOthers,
          setRequired: mockSetRequired,
          value
        });

      customFunctionCommon(hardshipToPaymentChange, {
        newValue: '12/31/2999',
        previousValue: '12/30/2999',
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator('any value')) //toPaymentDate
          .mockImplementationOnce(() => ({})) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator('05/31/2999')) //fromPaymentDate
          .mockImplementationOnce(_mockOnFindGenerator({ value: '4' })) // reagePaymentPeriod
      });

      expect(mockSetRequired).not.toBeCalled();
      expect(mockSetValue).not.toBeCalled();
    });

    it('value period is undefined', () => {
      const _mockOnFindGenerator = (value?: string | Record<string, string>) =>
        mockOnFindGenerator({
          setValue: mockSetValue,
          setOthers: mockSetOthers,
          setRequired: mockSetRequired,
          value
        });

      customFunctionCommon(hardshipToPaymentChange, {
        newValue: '12/31/2999',
        previousValue: '12/30/2999',
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator('any value')) //toPaymentDate
          .mockImplementationOnce(() => ({})) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator('05/31/2999')) //fromPaymentDate
          .mockImplementationOnce(_mockOnFindGenerator()) // reagePaymentPeriod
      });

      expect(mockSetRequired).not.toBeCalled();
      expect(mockSetValue).not.toBeCalled();
    });

    it('valueFromPayment is undefined', () => {
      const _mockOnFindGenerator = (value?: string | Record<string, string>) =>
        mockOnFindGenerator({
          setValue: mockSetValue,
          setOthers: mockSetOthers,
          setRequired: mockSetRequired,
          value
        });

      customFunctionCommon(hardshipToPaymentChange, {
        newValue: '12/31/2999',
        previousValue: '12/30/2999',
        onFind: jest
          .fn()
          .mockImplementationOnce(_mockOnFindGenerator('any value')) //toPaymentDate
          .mockImplementationOnce(() => ({})) // hardshipTypeOfReage
          .mockImplementationOnce(_mockOnFindGenerator()) //fromPaymentDate
          .mockImplementationOnce(_mockOnFindGenerator()) // reagePaymentPeriod
      });

      expect(mockSetRequired).not.toBeCalled();
      expect(mockSetValue).not.toBeCalled();
    });
  });
});

describe('lengthMonth', () => {
  it('should be return 12', () => {
    const result = lengthMonth('12/31/2999', '12/31/2999');
    expect(result).toEqual(12);
  });

  it('should be return 4 and lastDateFrom && lastDateTo', () => {
    const result = lengthMonth('6/30/2998', '1/31/2997');
    expect(result).toEqual(4);
  });

  it('should be return 4', () => {
    const result = lengthMonth('6/29/2998', '1/29/2997');
    expect(result).toEqual(4);
  });

  it('should be return 3', () => {
    const result = lengthMonth('6/29/2998', '1/28/2997');
    expect(result).toEqual(3);
  });

  it('should be return 2', () => {
    const result = lengthMonth('1/31/2998', '6/30/2997');
    expect(result).toEqual(2);
  });

  it('should be return 1', () => {
    const result = lengthMonth('1/30/2998', '6/28/2997');
    expect(result).toEqual(1);
  });

  it('should be return 2', () => {
    const result = lengthMonth('1/28/2998', '6/29/2997');
    expect(result).toEqual(2);
  });
});
