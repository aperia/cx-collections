import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { periodChange } from './hardshipPeriod';

const mockSetValue = jest.fn();
const mockSetData = jest.fn();
const mockData = ['2', '3'];
let mockOnFind: jest.SpyInstance;

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockSetData.mockReset();
  mockSetData.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('periodChange custom function', () => {
  it('name is not hardshipProgramPeriod', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      setData: mockSetData
    });
    customFunctionCommon(periodChange, {
      newValue: undefined,
      onFind: mockOnFind
    });
    expect(mockSetData).not.toBeCalled();
    expect(mockSetValue).not.toBeCalled();
  });

  it('name is hardshipProgramPeriod', () => {
    const mockNewValue = '2';
    mockOnFind = jest
      .fn()
      .mockImplementationOnce(
        mockOnFindGenerator({
          setValue: mockSetValue,
          setData: mockSetData,
          data: mockData
        })
      )
      .mockImplementationOnce(
        mockOnFindGenerator({
          setValue: mockSetValue,
          setData: mockSetData,
          data: mockData,
          value: '3'
        })
      );
    customFunctionCommon(periodChange, {
      name: 'hardship_hardshipProgramPeriod',
      newValue: mockNewValue,
      onFind: mockOnFind
    });
    expect(mockSetData).toBeCalledWith(['2']);
    expect(mockSetValue).toBeCalledWith(mockNewValue);
  });

  it('indexOtherFieldValue not greater than indexCurrentFieldValue', () => {
    mockOnFind = jest
      .fn()
      .mockImplementationOnce(
        mockOnFindGenerator({
          setValue: mockSetValue,
          setData: mockSetData,
          data: mockData
        })
      )
      .mockImplementationOnce(
        mockOnFindGenerator({
          setValue: mockSetValue,
          setData: mockSetData,
          data: mockData,
          value: '2'
        })
      );
    customFunctionCommon(periodChange, {
      name: 'hardship_hardshipProgramPeriod',
      newValue: '3',
      onFind: mockOnFind
    });
    expect(mockSetData).toBeCalledWith(mockData);
    expect(mockSetValue).not.toBeCalled();
  });
});
