import { IValueChangeFunc, ExtraFieldProps } from 'app/_libraries/_dof/core';

export const onChangeAmountCCCS: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  if (!newValue) return;
  const cycleToDatePaymentAmount = formValues?.cycleToDatePaymentAmount;
  let paymentAmount = 0;
  const nonDelinquentPmts = onFind('nonDelinquentMpdAmount');
  const nonDelinquentPmtsProps: ExtraFieldProps = nonDelinquentPmts.props;
  paymentAmount = cycleToDatePaymentAmount - newValue;
  if (paymentAmount < 0) {
    return nonDelinquentPmtsProps.props.setValue(null);
  }
  if (cycleToDatePaymentAmount) {
    return nonDelinquentPmtsProps.props.setValue(paymentAmount);
  }
};
