import {
  Validator,
  ValidationContext,
  validatorRegistry,
  ValidationOptions,
  compareTo
} from 'app/_libraries/_dof/core';
import isUndefined from 'lodash.isundefined';
import { formatCommon } from 'app/helpers';
import { ReactText } from 'react';

interface CompareDateOptions extends ValidationOptions {
  fieldToCompare?: string;
  fieldType?: string;
  operator?: 'Equal' | 'Less' | 'Greater' | 'GreaterEqual' | 'LessEqual';
  format?: string;
  type?: string;
  isReturnFieldCompare?: boolean;
}

export class CompareNumber
  implements
    Validator<
      string | number,
      string | undefined | { messageCustom: string; count: string | number }
    > {
  private readonly _fieldToCompare?: string;
  private readonly _fieldType?: string;
  private readonly _format?: string;
  private readonly _isReturnFieldCompare?: boolean;
  private readonly _errMsg?: string;
  private _type: string | undefined;
  private readonly _operator?:
    | 'Equal'
    | 'Less'
    | 'Greater'
    | 'GreaterEqual'
    | 'LessEqual';

  constructor(options: CompareDateOptions = {}) {
    this._fieldToCompare = options.fieldToCompare;
    this._errMsg = options.errorMsg;
    this._operator = options.operator;
    this._fieldType = options.fieldType;
    this._format = options.format;
    this._isReturnFieldCompare = options.isReturnFieldCompare;
    this._type = options.type;
  }

  validate(value: string | number, context: ValidationContext) {
    if (
      this._operator &&
      this._fieldToCompare &&
      !isUndefined(context.valueOf(this._fieldToCompare))
    ) {
      const valueToCompare = context.valueOf(this._fieldToCompare).toString();

      const errMessage = this.getErrorMsgWithType(valueToCompare);

      const toStringValue =
        typeof value === 'number' ? value.toString() : value;

      switch (this._operator) {
        case 'Equal': {
          if (compareTo(toStringValue, valueToCompare) !== 0) return errMessage;
          break;
        }
        case 'Less': {
          if (compareTo(toStringValue, valueToCompare) !== -1)
            return errMessage;
          break;
        }
        case 'Greater': {
          if (compareTo(toStringValue, valueToCompare) !== 1) return errMessage;
          break;
        }
        case 'GreaterEqual': {
          if (compareTo(toStringValue, valueToCompare) === -1)
            return errMessage;
          break;
        }
        case 'LessEqual': {
          if (compareTo(toStringValue, valueToCompare) === 1) return errMessage;
          break;
        }
        default: {
          return undefined;
        }
      }
    }
    return undefined;
  }

  getFormatCurrency = (
    valueToCompare: string | number,
    type: string | undefined
  ) => {
    let typeFormat: number;
    switch (type) {
      case 'c2':
        typeFormat = 2;
        break;
      case 'c3':
        typeFormat = 3;
        break;
      default:
        typeFormat = 0;
    }
    return formatCommon(valueToCompare).currency(typeFormat);
  };

  getErrorMsgWithType(valueToCompare: string | number) {
    if (!this._operator || !this._fieldToCompare) {
      return;
    }

    const DEFAULT_MESSAGES = {
      Equal: `It must be greater or equal  to ${this._fieldToCompare}`,
      Less: `It must be less to ${this._fieldToCompare}`,
      Greater: `It must be greater ${this._fieldToCompare}`,
      GreaterEqual: `It must be greater or equal ${this._fieldToCompare}`,
      LessEqual: `It must be equal or less ${this._fieldToCompare}`
    };

    const messageCustom = this._errMsg || DEFAULT_MESSAGES[this._operator];

    const count =
      this._fieldType === 'currency'
        ? this.getFormatCurrency(valueToCompare, this._format)
        : valueToCompare;

    const errMessage = !this._isReturnFieldCompare
      ? messageCustom
      : { messageCustom, count };

    if (this._type) {
      return getErrorMsgHardshipAmount(this._errMsg, valueToCompare);
    }

    return errMessage;
  }
}

export const getErrorMsgHardshipAmount = (
  errMsg: string | undefined,
  value: ReactText
) => {
  return `${errMsg} ${formatCommon(value).currency(2)}.`;
};

validatorRegistry.registerValidator('CompareNumber', CompareNumber);
