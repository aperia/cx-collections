import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { onChangeDelinquentCCCS } from './onChangeDelinquentCCCS';

const mockSetValue = jest.fn();
const mockSetOthers = jest.fn();
let mockOnFind: jest.SpyInstance;
const mockPrevState = {};

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setValue: mockSetValue,
    dependentField: ['dependentField 1', 'dependentField 2'],
    setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState))
  });
});

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockSetOthers.mockReset();
  mockSetOthers.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('onChangeDelinquentCCCS custom function', () => {
  it('should return undefined when newValue empty', () => {
    const result = customFunctionCommon(onChangeDelinquentCCCS, {
      newValue: undefined
    });
    expect(mockSetValue).not.toBeCalled();
    expect(result).toEqual(undefined);
  });

  it('should call SetValue null when cycleToDatePayment Amount < input Value', () => {
    customFunctionCommon(onChangeDelinquentCCCS, {
      newValue: 100,
      formValues: { cycleToDatePaymentAmount: 99 },
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith(null);
  });

  it('calculationAmount > 0 & calculationAmount < minimumAmount', () => {
    customFunctionCommon(onChangeDelinquentCCCS, {
      newValue: 99,
      formValues: { cycleToDatePaymentAmount: 100, minimumAmount: 5 },
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith(1);
    expect(mockSetOthers).toBeCalledWith({
      ...mockPrevState,
      forceInvalid: true
    });
  });

  it('calculationAmount > 0 & calculationAmount > minimumAmount', () => {
    customFunctionCommon(onChangeDelinquentCCCS, {
      newValue: 95,
      formValues: { cycleToDatePaymentAmount: 100, minimumAmount: 3 },
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith(5);
  });
});
