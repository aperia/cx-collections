import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { dateOfDeathChange } from './dateOfDeath';

const mockSetValue = jest.fn();
const mockSetOthers = jest.fn();
const mockSetEnable = jest.fn();
let mockOnFind: jest.SpyInstance;
const mockPrevState = {};

const mockValue = 32;

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setValue: mockSetValue,
    setEnable: mockSetEnable,
    value: mockValue,
    dependentField: ['dependentField 1', 'dependentField 2'],
    setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState))
  });
});

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockSetOthers.mockReset();
  mockSetOthers.mockRestore();
  mockSetEnable.mockReset();
  mockSetEnable.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('dateOfDeathChange custom function', () => {
  it('should return undefined when newValue empty', () => {
    customFunctionCommon(dateOfDeathChange, {
      newValue: undefined,
      onFind: mockOnFind
    });
    expect(mockSetValue.mock.calls).toEqual([[null], [null]]);
    expect(mockSetEnable.mock.calls).toEqual([[false], [false]]);
  });

  it('has value', () => {
    customFunctionCommon(dateOfDeathChange, {
      newValue: mockValue,
      onFind: mockOnFind
    });
    expect(mockSetValue).not.toBeCalled();
    expect(mockSetEnable.mock.calls).toEqual([[true], [mockValue]]);
  });
});
