import { IValueChangeFunc, ExtraFieldProps } from 'app/_libraries/_dof/core';
import { MEMO_TEXT_REGEX } from 'app/constants';
/**
 * Function to check if text area field has non-allowed character,
 * then set message for alert field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const textAreaMemoChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const alertMsgMemo = onFind('alertMsgMemo');
  if (alertMsgMemo === null) return;

  const isTextValid = new RegExp(MEMO_TEXT_REGEX).test(newValue);
  const alertMsgMemoField: ExtraFieldProps = alertMsgMemo?.props;

  if (isTextValid) {
    alertMsgMemoField?.props.setValue('');
  } else {
    alertMsgMemoField?.props.setValue(
      'Remove invalid character. These special characters are not allowed “ ) ( + ; \\ %'
    );
  }
};
