import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { deceasedDateOfDeath } from './deceasedDateOfDeath';

const mockSetValue = jest.fn();
const mockSetOthers = jest.fn();
const mockSetEnable = jest.fn();
let mockOnFind: jest.SpyInstance;
const mockPrevState = { options: {} };

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setValue: mockSetValue,
    setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState)),
    setEnable: mockSetEnable
  });
});

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockSetOthers.mockReset();
  mockSetOthers.mockRestore();
  mockSetEnable.mockReset();
  mockSetEnable.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('deceasedDateOfDeath custom function', () => {
  it('should set value null when newValue empty', () => {
    customFunctionCommon(deceasedDateOfDeath, {
      newValue: undefined,
      onFind: mockOnFind
    });
    expect(mockSetValue).toBeCalledWith(null);
    expect(mockSetOthers).toBeCalledWith({ options: { min: undefined } });
  });

  it('has newValue', () => {
    const dateOfDeath = '2021-04-12';
    customFunctionCommon(deceasedDateOfDeath, {
      newValue: dateOfDeath,
      onFind: mockOnFind
    });
    expect(mockSetValue).not.toBeCalled();
    expect(mockSetOthers).toBeCalledWith({ options: { min: dateOfDeath } });
  });

  it('has newValue and date of death > date notice', () => {
    const dateOfDeath = '2021-04-12';
    customFunctionCommon(deceasedDateOfDeath, {
      newValue: dateOfDeath,
      onFind: mockOnFind,
      formValues: { deceasedDateNoticeReceived: '2021-04-10' }
    });
    expect(mockSetValue).toBeCalledWith(null);
    expect(mockSetOthers).toBeCalledWith({ options: { min: dateOfDeath } });
  });

  it('has find field not found', () => {
    const dateOfDeath = '2021-04-12';
    customFunctionCommon(deceasedDateOfDeath, {
      newValue: dateOfDeath,
      onFind: jest.fn().mockReturnValue({ props: {} }),
      formValues: { deceasedDateNoticeReceived: '2021-04-10' }
    });
    expect(mockSetValue).not.toBeCalled();
    expect(mockSetOthers).not.toBeCalled();
  });
});
