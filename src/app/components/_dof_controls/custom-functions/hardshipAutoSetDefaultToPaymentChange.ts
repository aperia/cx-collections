import { isInvalidDateValue, setErrorField } from 'app/helpers';
import { ExtraFieldProps, IValueChangeFunc } from 'app/_libraries/_dof/core';
import isEmpty from 'lodash.isempty';
import { lengthMonth } from 'pages/HardShip/helpers';
import { Field } from 'redux-form';

/**
 * Function to check if text area field has non-allowed character,
 * then set message for alert field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const hardshipAutoSetDefaultToPaymentChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const toPaymentRef = onFind('toPaymentDate') as MagicKeyValue;
  const fromPaymentRef = onFind('fromPaymentDate') as MagicKeyValue;
  const valueFromPayment = fromPaymentRef?.ref?.current?.props?.value;

  const periodRef = onFind('reagePaymentPeriod') as MagicKeyValue;
  const valuePeriod = periodRef?.ref?.current?.props?.value;
  const {
    dependentType,
    data = [],
    setValue: setPeriodValue,
    setRequired: setPeriodRequired
  } = periodRef?.props?.props;

  if (!isInvalidDateValue(newValue)) {
    setErrorField(toPaymentRef as Field<ExtraFieldProps>, false);

    if (!isInvalidDateValue(valueFromPayment)) {
      const nextFormValues = { ...formValues, toPaymentDate: newValue };
      const dependentValues = ['fromPaymentDate', 'toPaymentDate'];
      const lengthData = lengthMonth(
        nextFormValues,
        dependentValues,
        dependentType
      );

      const nextData = data.slice(0, lengthData + 1);
      const lastItemData = nextData?.slice(-1).pop();

      if (isEmpty(valuePeriod)) {
        // init value
        setPeriodValue(data[0]);
        setPeriodRequired(true);
      } else if (lastItemData.value < valuePeriod.value) {
        // auto change value is over range period data
        setPeriodValue(data[nextData.length - 1]);
      }
    }
  }
  if (isInvalidDateValue(newValue) || isInvalidDateValue(valueFromPayment)) {
    setPeriodValue({});
  }
};
