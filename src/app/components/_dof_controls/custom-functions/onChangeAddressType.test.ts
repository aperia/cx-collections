import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { ID_ADDRESS_TYPE } from 'pages/AccountDetails/CardHolderMaintenance/ExpandedAddress/constants';
import { onChangeAddressType } from './onChangeAddressType';

const mockSetValue = jest.fn();
const mockSetReadOnly = jest.fn();
let mockOnFind: jest.SpyInstance;

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockSetReadOnly.mockReset();
  mockSetReadOnly.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('onChangeAddressType', () => {
  it('formValue has not fieldId address', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue
    });
    customFunctionCommon(onChangeAddressType, {
      previousValue: {
        description: 'P - Permanent',
        fieldID: 'P',
        fieldValue: 'Permanent'
      },
      newValue: {
        description: 'BLL1 - Billing',
        fieldID: 'BLL1',
        fieldValue: 'Billing'
      },
      onFind: mockOnFind,
      formValues: { addressCategory: {} }
    });
    expect(mockSetValue).not.toBeCalled();
  });

  it('fieldID equal PERMANENT', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      setReadOnly: mockSetReadOnly
    });
    customFunctionCommon(onChangeAddressType, {
      previousValue: {
        description: 'P - Permanent',
        fieldID: 'P',
        fieldValue: 'Permanent'
      },
      newValue: {
        description: 'BLL1 - Billing',
        fieldID: 'BLL1',
        fieldValue: 'Billing'
      },
      onFind: mockOnFind,
      formValues: {
        addressCategory: { fieldID: ID_ADDRESS_TYPE.PERMANENT },
        effectiveTo: '12/31/2999',
        effectiveFrom: '12/31/1999'
      }
    });
    expect(mockSetValue.mock.calls).toEqual([['12/31/9999'], [undefined]]);
    expect(mockSetReadOnly).toBeCalledWith(true);
  });

  it('fieldID not equal PERMANENT', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      setReadOnly: mockSetReadOnly
    });
    customFunctionCommon(onChangeAddressType, {
      previousValue: {
        description: 'T - Temporary',
        fieldID: 'T',
        fieldValue: 'Temporary'
      },
      newValue: {
        description: 'BLL1 - Billing',
        fieldID: 'BLL1',
        fieldValue: 'Billing'
      },
      onFind: mockOnFind,
      formValues: {
        addressCategory: { fieldID: ID_ADDRESS_TYPE.TEMPORARY },
        effectiveTo: '12/31/2999',
        effectiveFrom: '12/31/1999'
      }
    });
    expect(mockSetValue.mock.calls).toEqual([[undefined], [undefined]]);
    expect(mockSetReadOnly).toBeCalledWith(false);
  });

  it('has not effectiveTo, effectiveFrom', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      setReadOnly: mockSetReadOnly
    });
    customFunctionCommon(onChangeAddressType, {
      newValue: {
        description: 'BLL1 - Billing',
        fieldID: 'BLL1',
        fieldValue: 'Billing'
      },
      previousValue: {
        description: 'P - Permanent',
        fieldID: 'P',
        fieldValue: 'Permanent'
      },
      onFind: mockOnFind,
      formValues: {
        addressCategory: { fieldID: ID_ADDRESS_TYPE.TEMPORARY }
      }
    });
    expect(mockSetValue).not.toBeCalled();
    expect(mockSetReadOnly).toBeCalledWith(false);
  });

  it('newValue = previousValue', () => {
    mockOnFind = mockOnFindGenerator({
      setValue: mockSetValue,
      setReadOnly: mockSetReadOnly
    });
    customFunctionCommon(onChangeAddressType, {
      newValue: {
        description: 'BLL1 - Billing',
        fieldID: 'BLL1',
        fieldValue: 'Billing'
      },
      previousValue: {
        description: 'BLL1 - Billing',
        fieldID: 'BLL1',
        fieldValue: 'Billing'
      },
      onFind: mockOnFind,
      formValues: {
        addressCategory: { fieldID: ID_ADDRESS_TYPE.TEMPORARY }
      }
    });
    expect(mockSetValue).not.toBeCalled();
  });
});
