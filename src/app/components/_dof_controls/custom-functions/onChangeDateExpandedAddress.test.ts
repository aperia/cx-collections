import { customFunctionCommon, mockOnFindGenerator } from 'app/test-utils';
import { ID_ADDRESS_TYPE } from 'pages/AccountDetails/CardHolderMaintenance/ExpandedAddress/constants';
import { onChangeDateExpandedAddress } from './onChangeDateExpandedAddress';

const mockSetValue = jest.fn();
const mockSetOthers = jest.fn();
let mockOnFind: jest.SpyInstance;
const mockPrevState = { options: {} };

beforeEach(() => {
  mockOnFind = mockOnFindGenerator({
    setValue: mockSetValue,
    dependentField: ['dependentField 1', 'dependentField 2'],
    setOthers: (fn: Function) => mockSetOthers(fn(mockPrevState))
  });
});

afterEach(() => {
  mockSetValue.mockReset();
  mockSetValue.mockRestore();
  mockSetOthers.mockReset();
  mockSetOthers.mockRestore();
  mockOnFind.mockReset();
  mockOnFind.mockRestore();
});

describe('onChangeDateExpandedAddress custom function', () => {
  const mockNewValue = '2021-04-12';
  const date = new Date(mockNewValue);
  const minDate = new Date(date.setDate(date.getDate() + 1));
  it('newValue is empty', () => {
    const result = customFunctionCommon(onChangeDateExpandedAddress, {
      newValue: undefined,
      onFind: mockOnFind
    });
    expect(mockSetOthers).toBeCalledWith({
      ...mockPrevState,
      options: {
        ...mockPrevState.options,
        min: null,
        minDateDisabledText: null
      }
    });
    expect(result).toEqual(undefined);
  });

  it('has newValue and category is equal to ID_ADDRESS_TYPE.PERMANENT', () => {
    customFunctionCommon(onChangeDateExpandedAddress, {
      newValue: mockNewValue,
      onFind: mockOnFind,
      formValues: {
        addressCategory: {
          fieldID: ID_ADDRESS_TYPE.PERMANENT
        }
      }
    });
    expect(mockSetValue).not.toBeCalled();
    expect(mockSetOthers).toBeCalledWith({
      ...mockPrevState,
      options: {
        ...mockPrevState.options,
        min: minDate,
        minDateDisabledText: 'txt_effective_to_cannot_before_effective_from'
      }
    });
  });

  it('has newValue and category is equal to ID_ADDRESS_TYPE.PERMANENT', () => {
    customFunctionCommon(onChangeDateExpandedAddress, {
      newValue: mockNewValue,
      onFind: mockOnFind,
      formValues: {
        effectiveTo: '2021-04-11',
        addressCategory: {
          fieldID: ID_ADDRESS_TYPE.PERMANENT
        }
      }
    });
    expect(mockSetValue).toBeCalledWith(undefined);
    expect(mockSetOthers).toBeCalledWith({
      ...mockPrevState,
      options: {
        ...mockPrevState.options,
        min: minDate,
        minDateDisabledText: 'txt_effective_to_cannot_before_effective_from'
      }
    });
  });

  it('has newValue and category is equal to ID_ADDRESS_TYPE.REPEATING', () => {
    customFunctionCommon(onChangeDateExpandedAddress, {
      newValue: mockNewValue,
      onFind: mockOnFind,
      formValues: {
        effectiveTo: '2021-04-11',
        addressCategory: {
          fieldID: ID_ADDRESS_TYPE.REPEATING
        }
      }
    });
    expect(mockSetValue).toBeCalledWith(undefined);
    expect(mockSetOthers).toBeCalledWith({
      ...mockPrevState,
      options: {
        ...mockPrevState.options,
        min: minDate,
        minDateDisabledText: 'txt_effective_to_cannot_before_effective_from'
      }
    });
  });

  it('has no newValue and category is equal to ID_ADDRESS_TYPE.TEMPORARY', () => {
    customFunctionCommon(onChangeDateExpandedAddress, {
      newValue: undefined,
      onFind: mockOnFind,
      formValues: {
        effectiveTo: '2021-04-11',
        addressCategory: {
          fieldID: ID_ADDRESS_TYPE.TEMPORARY
        }
      }
    });
    expect(mockSetValue).not.toBeCalled();
    expect(mockSetOthers).toBeCalledWith({
      ...mockPrevState,
      options: {
        ...mockPrevState.options,
        min: null,
        minDateDisabledText: null
      }
    });
  });
});
