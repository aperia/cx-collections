import { isInvalidDateValue, setErrorField } from 'app/helpers';
import { IValueChangeFunc } from 'app/_libraries/_dof/core';

/**
 * Function to check if text area field has non-allowed character,
 * then set message for alert field
 * @param name
 * @param newValue
 * @param previousValue
 * @param onFind
 * @param formKey
 * @param setFormValues
 */
export const hardshipDeploymentEndDateChange: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const deploymentEndRef = onFind('deploymentEndDate');
  if (!isInvalidDateValue(newValue)) {
    setErrorField(deploymentEndRef, false);
  }
};
