import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault, handleOnFind } from 'app/helpers';
import { ExtraFieldProps, IValueChangeFunc } from 'app/_libraries/_dof/core';
import { ID_ADDRESS_TYPE } from 'pages/AccountDetails/CardHolderMaintenance/ExpandedAddress/constants';

export const handleMaxDayAddAddress = (
  idCategory: string,
  addressEffectiveTo: ExtraFieldProps,
  effectiveFrom: Date,
  effectiveTo: Date
) => {
  if (!idCategory) return;
  const { REPEATING, TEMPORARY } = ID_ADDRESS_TYPE;
  let maxDateDisabledText = '';
  let numberOfMonth = 0;

  if (idCategory === REPEATING) {
    maxDateDisabledText = 'txt_limited_11_month';
    numberOfMonth = 11;
  }

  if (idCategory === TEMPORARY) {
    maxDateDisabledText = 'txt_limited_12_month';
    numberOfMonth = 12;
  }

  if (effectiveFrom > effectiveTo) {
    addressEffectiveTo?.props?.setValue(null);
  }

  addressEffectiveTo?.props?.setOthers((pre: MagicKeyValue) => ({
    ...pre,
    options: {
      ...pre?.options,
      maxDateWithDays: {
        numberOfMonth,
        date: effectiveFrom || new Date()
      },
      maxDateDisabledText
    }
  }));
};

export const onChangeDateExpandedAddress: IValueChangeFunc = (
  name,
  newValue,
  previousValue,
  onFind,
  formKey,
  setFormValues,
  _,
  formValues
) => {
  const effectiveToValue = formValues?.effectiveTo;
  const idCategory = formValues?.addressCategory?.fieldID;
  const effectiveTo = handleOnFind('effectiveTo', onFind);
  if (!newValue) {
    effectiveTo.props.setOthers((pre: MagicKeyValue) => ({
      ...pre,
      options: {
        ...pre?.options,
        min: null,
        minDateDisabledText: null
      }
    }));
  } else {
    const date = new Date(newValue);
    const calculateMinDate = new Date(date.setDate(date.getDate() + 1));
    const effectToFormat = formatTimeDefault(
      effectiveToValue,
      FormatTime.YearMonthDay
    )!;

    const effectFromFormat = formatTimeDefault(
      newValue,
      FormatTime.YearMonthDay
    )!;

    const isSetEffectiveToUndefined =
      idCategory !== ID_ADDRESS_TYPE.PERMANENT ||
      effectToFormat < effectFromFormat;

    if (isSetEffectiveToUndefined && effectToFormat) {
      effectiveToValue && effectiveTo.props.setValue(undefined);
    }

    effectiveTo.props.setOthers((pre: MagicKeyValue) => ({
      ...pre,
      options: {
        ...pre?.options,
        min: calculateMinDate,
        minDateDisabledText: 'txt_effective_to_cannot_before_effective_from'
      }
    }));
  }
  const addressEffectiveTo = handleOnFind('effectiveTo', onFind);
  handleMaxDayAddAddress(
    idCategory,
    addressEffectiveTo,
    newValue,
    effectiveToValue
  );
};
