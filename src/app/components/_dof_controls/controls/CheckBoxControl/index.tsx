import React, { useState } from 'react';

// components
import { CommonControlProps } from 'app/_libraries/_dof/core';
import {
  CheckBox,
  Tooltip,
  TooltipProps
} from 'app/_libraries/_dls/components';
import { InputProps } from 'app/_libraries/_dls/components/CheckBox/Input';

// utils
import isUndefined from 'lodash.isundefined';
import isFunction from 'lodash.isfunction';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface CheckBoxControlProps
  extends CommonControlProps,
    Omit<InputProps, 'id' | 'label'> {
  tooltipProps?: TooltipProps;
  message?: string;
  dataTestId?: string;
}

export const CheckBoxControl: React.FC<CheckBoxControlProps> = ({
  input: { name, value, onChange },
  meta: { touched, error, valid, invalid },
  id,
  label,
  enable,
  readOnly,
  tooltipProps,
  message: elementProps,

  dataTestId,

  ...props
}) => {
  const [opened, setOpened] = useState<boolean>(false);
  const element = (
    <p dangerouslySetInnerHTML={{ __html: elementProps || '' }}></p>
  );
  const { t } = useTranslation();

  const handleVisibilityChange = () => {
    if (isUndefined(elementProps)) return setOpened(false);
    setOpened(!opened);
  };

  return (
    <Tooltip
      dataTestId={`${dataTestId}-tooltip`}
      opened={opened}
      element={element}
      variant="default"
      placement="top"
      triggerClassName="d-inline-block"
      onVisibilityChange={handleVisibilityChange}
      {...tooltipProps}
    >
      <CheckBox dataTestId={`${dataTestId}-checkbox-control`}>
        <CheckBox.Input
          id={id}
          name={name}
          readOnly={readOnly}
          disabled={!enable}
          checked={value}
          onChange={event => {
            isFunction(onChange) && onChange(event.target.checked);
          }}
        />
        <CheckBox.Label>{t(label)}</CheckBox.Label>
      </CheckBox>
    </Tooltip>
  );
};

export default CheckBoxControl;
