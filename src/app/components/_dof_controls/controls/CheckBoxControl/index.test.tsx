import React from 'react';

// RTL
import { fireEvent, render, screen } from '@testing-library/react';

// mocks
import 'app/test-utils/mocks/MockToolTip';
import 'app/test-utils/mocks/MockCheckboxInput';

// components
import CheckboxControl from './index';

// helpers
import { getDefaultDOFMockProps } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const mockLabel = 'checkbox-test-label';

describe('CheckboxControl', () => {
  it('render tooltip message', () => {
    const mockToolTipMsg = 'message';
    const mockProps = getDefaultDOFMockProps();

    render(
      <CheckboxControl
        {...mockProps}
        label={mockLabel}
        message={mockToolTipMsg}
      />
    );
    fireEvent.change(screen.getByTestId('Tooltip_onVisibilityChange'), {
      target: { value: 'undefined', isOpened: true }
    });
    expect(screen.getByText(mockLabel)).toBeInTheDocument();
    expect(screen.getByTestId('CheckBox.Input_onChange')).toBeInTheDocument();
    expect(screen.getByText(mockToolTipMsg)).toBeInTheDocument();
  });

  it('render without tooltip message', () => {
    const mockProps = getDefaultDOFMockProps();

    render(<CheckboxControl {...mockProps} label={mockLabel} />);
    fireEvent.change(screen.getByTestId('Tooltip_onVisibilityChange'), {
      target: { value: 'undefined', isOpened: true }
    });
    fireEvent.change(screen.getByTestId('CheckBox.Input_onChange'), {
      target: { value: 'undefined', isOpened: true }
    });
    expect(screen.getByText(mockLabel)).toBeInTheDocument();
    expect(screen.getByTestId('CheckBox.Input_onChange')).toBeInTheDocument();
  });
});
