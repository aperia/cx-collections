import React from 'react';

// RTL
import { render } from '@testing-library/react';

// components
import DividerControl from './index';

// helpers
import { getDefaultDOFMockProps } from 'app/test-utils';

describe('Test DividerControl component', () => {
  const props = getDefaultDOFMockProps();

  it('render default component', () => {
    const wrapper = render(<DividerControl {...props} />);

    expect(wrapper.container.innerHTML).toEqual('<hr>');
  });

  it('render component > dashed = true', () => {
    const wrapper = render(<DividerControl {...props} dashed={true} />);

    expect(wrapper.container.innerHTML).toEqual(
      '<div class="divider-dashed"></div>'
    );
  });
});
