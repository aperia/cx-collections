import React from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';

export interface DividerControlProps extends CommonControlProps {
  dashed?: boolean;
}

const DividerControl: React.FC<DividerControlProps> = ({ ...props }) => {
  if (props.dashed) return <div className="divider-dashed" />;
  return <hr />;
};

export default DividerControl;
