import React, { useEffect, useMemo, useState } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';

// components
import {
  NumericProps,
  NumericTextBox,
  OnChangeNumericEvent
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import isNumber from 'lodash.isnumber';
import isString from 'lodash.isstring';
import classnames from 'classnames';

export interface NumericControlProps
  extends CommonControlProps,
    Omit<NumericProps, 'id' | 'label'> {
  dataTestId?: string;
  forceInvalid?: boolean;
  autoResetForceInvalid?: boolean;
  options?: {
    disabled?: boolean;
    min?: number;
    max?: number;
  };
}

const NumericControl: React.FC<NumericControlProps> = ({
  input: { value: valueProp, onChange, onBlur, name },
  meta: { error, valid, invalid, touched },
  id,
  dataTestId,
  label,
  tooltip,
  enable,
  required,
  format,
  maxLength,
  readOnly,
  autoFill,
  options,
  forceInvalid,
  autoResetForceInvalid: autoResetForceInvalidProps,
  className,
  ...props
}) => {
  const { t } = useTranslation();
  const [autoResetForceInvalid, setAutoResetForceInvalid] = useState(
    autoResetForceInvalidProps
  );
  const [value, setValue] = useState<React.ReactText>(valueProp);

  const handleOnChange = (event: OnChangeNumericEvent) => {
    const { value } = event.target;
    setValue(value);
  };

  const handleOnBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    if (!enable || readOnly) return;
    const { value } = event.target;
    onChange(value);
    onBlur(value);
    setAutoResetForceInvalid(false);
  };

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  // effect in init value
  useEffect(() => {
    setAutoResetForceInvalid(autoResetForceInvalidProps);
  }, [autoResetForceInvalidProps]);

  const renderMessageError = useMemo(() => {
    if (isString(error) || isNumber(error)) {
      return t(error);
    }
    return t(error?.messageCustom, { count: error?.count });
  }, [error, t]);

  return (
    <div className={classnames(className)}>
      <NumericTextBox
        id={id}
        dataTestId={dataTestId}
        label={t(label)}
        value={value}
        format={format}
        maxLength={maxLength}
        required={required}
        readOnly={readOnly}
        autoFill={autoFill}
        error={
          (enable && required && touched && invalid) ||
          forceInvalid ||
          autoResetForceInvalid
            ? {
                message: t(renderMessageError),
                status: invalid
              }
            : undefined
        }
        onChange={handleOnChange}
        onBlur={handleOnBlur}
        {...options}
      />
    </div>
  );
};

export default NumericControl;
