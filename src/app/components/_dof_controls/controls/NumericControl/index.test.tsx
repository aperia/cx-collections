import React from 'react';

// components
import NumericControl from './index';

// RTL
import { act, fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

// helpers
import { queryByClass } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test NumericControl control', () => {
  it('render component', () => {
    const { container } = render(
      <NumericControl
        id="id"
        options={{}}
        input={{} as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );

    expect(
      queryByClass(container, 'dls-numreric-container')
    ).toBeInTheDocument();
    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });

  it('render component > error', () => {
    render(
      <NumericControl
        id="id"
        options={{}}
        input={{} as WrappedFieldInputProps}
        meta={
          {
            error: 'Error Message',
            valid: true,
            invalid: true,
            touched: true
          } as WrappedFieldMetaProps
        }
        enable
        required
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      userEvent.click(inputElement);
    });

    expect(screen.getByText('Error Message')).toBeInTheDocument();
  });

  it('onBlur > readOnly', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    render(
      <NumericControl
        id="id"
        options={{}}
        input={
          {
            onChange: onChangeSpy,
            onBlur: onBlurSpy
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        enable
        readOnly
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      fireEvent.blur(inputElement);
    });

    expect(onBlurSpy).not.toBeCalled();
  });

  it('onBlur', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    render(
      <NumericControl
        id="id"
        options={{}}
        input={
          {
            onChange: onChangeSpy,
            onBlur: onBlurSpy
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        enable
        readOnly={false}
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      fireEvent.blur(inputElement);
    });

    expect(onChangeSpy).toBeCalled();
    expect(onBlurSpy).toBeCalled();
  });

  it('trigger onChange and onBlur', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    const { container } = render(
      <NumericControl
        id="id"
        options={{}}
        input={
          {
            onChange: onChangeSpy,
            onBlur: onBlurSpy
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      fireEvent.change(inputElement, {
        target: { value: '1' }
      });
      fireEvent.blur(inputElement);
    });

    expect(container.querySelector('input[value="1.00"')).toBeInTheDocument();
  });
});
