import React from 'react';
import TextAreaControl from './index';
import { fireEvent, render, screen } from '@testing-library/react';
import { getDefaultDOFMockProps } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks', () => {
  return {
    ...jest.requireActual('app/_libraries/_dls/hooks'),
    useTranslation: () => ({ t: (text: string) => text })
  };
});

describe('Test TextAreaControl component', () => {
  describe('render component', () => {
    it('default', () => {
      const props = getDefaultDOFMockProps({
        options: {
          allowOverMaxLength: true
        }
      });

      const wrapper = render(<TextAreaControl {...props} />);

      expect(wrapper.container.querySelector('textarea')).toBeInTheDocument();
    });

    it('with label', () => {
      const props = getDefaultDOFMockProps({
        label: 'Test Label',
        options: {
          maxLength: 20,
          countDown: 20
        }
      });

      const wrapper = render(<TextAreaControl {...props} />);

      expect(screen.getByText('Test Label')).toBeInTheDocument();
      expect(wrapper.container.querySelector('textarea')).toBeInTheDocument();
      expect(screen.getByText('txt_character_left')).toBeInTheDocument();
    });
  });

  describe('on change', () => {
    it('on change', () => {
      const props = getDefaultDOFMockProps({
        label: 'Test Label',
        options: {
          maxLength: 20,
          countDown: 10
        }
      });

      const wrapper = render(<TextAreaControl {...props} />);
      // input text
      const textElement = wrapper.container.querySelector('textarea');
      fireEvent.change(textElement!, { target: { value: 'test' } });

      wrapper.rerender(<TextAreaControl {...props} />);

      expect(textElement?.innerHTML).toEqual('test');
    });

    it('on change over max length', () => {
      const props = getDefaultDOFMockProps({
        label: 'Test Label',
        options: {
          maxLength: 5,
          countDown: 10
        }
      });

      const wrapper = render(<TextAreaControl {...props} />);
      // input text
      const textElement = wrapper.container.querySelector('textarea');
      fireEvent.change(textElement!, { target: { value: 'test test' } });

      wrapper.rerender(<TextAreaControl {...props} />);

      expect(textElement?.innerHTML).toEqual('test test');
    });
  });

  it('on blur', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    const props = getDefaultDOFMockProps({
      label: 'Test Label',
      onChange: onChangeSpy,
      onBlur: onBlurSpy,
      options: {
        maxLength: 20,
        countDown: 10
      }
    });

    const wrapper = render(<TextAreaControl {...props} />);

    // simulate blur
    const textElement = wrapper.container.querySelector('textarea');
    fireEvent.blur(textElement!, { target: { value: 'test' } });

    expect(onChangeSpy).toBeCalled();
    expect(onBlurSpy).toBeCalled();
  });
});
