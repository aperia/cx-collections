import React, { useEffect, useState } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';

// components
import { TextArea, TextAreaProps } from 'app/_libraries/_dls/components';

// helpers
import { useTranslation } from 'app/_libraries/_dls/hooks';
import classNames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface TextAreaControlProps
  extends CommonControlProps,
    Omit<TextAreaProps, 'id' | 'label'> {
  options?: {
    countDown?: boolean;
    maxLength?: number;
    className?: string;
    allowOverMaxLength?: boolean;
  };
}

export const TextAreaControl: React.FC<TextAreaControlProps> = ({
  input: { name, value: valueProp, onChange, onBlur },
  meta: { error: errorProp, valid, invalid, touched },
  id,
  dataTestId,
  enable,
  placeholder,
  options,
  label,
  ...props
}) => {
  const { t } = useTranslation();
  const [value, setValue] = useState<string>(valueProp ?? '');

  useEffect(() => {
    setValue(valueProp || '');
  }, [valueProp]);

  const handleOnChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const { value: newValue } = event.target;
    setValue(newValue);
  };

  const handleOnBlur = (event: React.FocusEvent<HTMLTextAreaElement>) => {
    const { value: newValue } = event.target;
    onChange(newValue);
    onBlur(newValue);
  };

  const restCharacter =
    options?.maxLength && options?.countDown
      ? options?.maxLength - value.length
      : undefined;

  const formatCharacterLeft = (restChar: number) => {
    if (restChar < 0)
      return t('txt_character_over_limit', { count: Math.abs(restChar) });
    return t('txt_character_left', { count: restChar });
  };

  const testId = genAmtId(dataTestId!, 'textarea-control', 'TextAreaControl');
  const characterLeftTestId = genAmtId(dataTestId!, 'textarea-control_character-left', 'TextAreaControl');

  return (
    <>
      {label && (
        <p className="fw-500 color-grey mb-8">
          {t(label)}
          <span className="color-red ml-4">*</span>
        </p>
      )}
      <TextArea
        id={id}
        dataTestId={testId}
        disabled={!enable}
        value={value}
        placeholder={t(placeholder)}
        className={options?.className}
        onChange={handleOnChange}
        onBlur={handleOnBlur}
        name={name}
        maxLength={options?.allowOverMaxLength ? undefined : options?.maxLength}
      />
      {restCharacter !== undefined && (
        <p
          className={classNames(
            'color-grey-l24 fs-12 mt-8',
            restCharacter < 0 && 'color-danger'
          )}
          data-testid={characterLeftTestId}
        >
          {formatCharacterLeft(restCharacter)}
        </p>
      )}
    </>
  );
};

export default TextAreaControl;
