import React from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';

// components
import { ColumnType, Grid } from 'app/_libraries/_dls/components';

// constants
import {
  NAME_GRID,
  CONFIG_GRID_OVERVIEW_ACCOUNT,
  CONFIG_GRID_OVERVIEW_COLLECTION
} from './constants';

// helpers
import get from 'lodash.get';
import isArray from 'lodash.isarray';
import { formatCommon } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';

interface SimpleGridControlLayoutProps {
  className: string;
}

export interface SimpleGridControlProps extends CommonControlProps {
  layoutProps?: SimpleGridControlLayoutProps;
  configType?: string;
  isFormat?: boolean;
  dataTestId?: string;
}

const SimpleGridControl: React.FC<SimpleGridControlProps> = ({
  input,
  id,
  isFormat,
  configType = '',
  layoutProps: { className = 'form-group-static' } = {},

  dataTestId,

  ...props
}) => {
  const { t } = useTranslation();
  const value = (input && input.value) || {};
  const columnsConfig = (type: string): ColumnType[] => {
    switch (type) {
      case NAME_GRID.overviewAccountHistory:
        return CONFIG_GRID_OVERVIEW_ACCOUNT;
      case NAME_GRID.overviewAccountCollection:
        return CONFIG_GRID_OVERVIEW_COLLECTION;
      default:
        return CONFIG_GRID_OVERVIEW_ACCOUNT;
    }
  };
  const formatData = (value: Record<string, string | number>) => {
    const amount = get(value, ['total', 'amount'], '');
    const amount1 = get(value, ['rows', 'amount1'], '');
    const amount2 = get(value, ['rows', 'amount2'], '');

    const mDash = <div className="form-group-static__text" />;

    return {
      total: {
        rowHeader: 'Total',
        amount: amount ? formatCommon(amount).currency(2) : mDash,
        number: get(value, ['total', 'number'], mDash)
      },
      rows: [
        {
          rowHeader: 'KEPT',
          amount: amount1 ? formatCommon(amount1).currency(2) : mDash,
          number: get(value, ['rows', 'number1'], mDash)
        },
        {
          rowHeader: 'BROKEN',
          amount: amount2 ? formatCommon(amount2).currency(2) : mDash,
          number: get(value, ['rows', 'number2'], mDash)
        }
      ]
    };
  };

  const columns = columnsConfig(configType).map(item => ({
    ...item,
    Header: t(item.Header)
  }));

  const data = isFormat ? formatData(value) : value;

  return (
    <div className={className} id={id}>
      {isFormat ? (
        <Grid
          dataTestId={dataTestId}
          columns={columns}
          data={get(data, ['rows'])}
          dataFooter={get(data, ['total'])}
        />
      ) : (
        <Grid
          dataTestId={dataTestId}
          columns={columns}
          data={isArray(data) ? data : []}
          loadElement={
            <tr>
              <td className="color-grey">{t('txt_no_data')}</td>
            </tr>
          }
        />
      )}
    </div>
  );
};

export default SimpleGridControl;
