import React from 'react';

// components
import SimpleGridControl from './index';

// RTL
import { render, screen } from '@testing-library/react';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';
import { NAME_GRID } from './constants';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text || '' });
});

describe('Test SimpleGridControl control', () => {
  it('render component', () => {
    render(
      <SimpleGridControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );

    expect(screen.getByRole('table')).toBeInTheDocument();
  });

  it('render component', () => {
    render(
      <SimpleGridControl
        id="id"
        configType={NAME_GRID.overviewAccountHistory}
        input={{ value: [] } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );

    expect(screen.getByRole('table')).toBeInTheDocument();
  });

  it('render component > isFormat = true', () => {
    render(
      <SimpleGridControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        isFormat={true}
      />
    );

    expect(screen.getByRole('table')).toBeInTheDocument();
  });

  it('render component > isFormat = true, having data', () => {
    render(
      <SimpleGridControl
        id="id"
        input={
          {
            value: {
              total: { amount: 500 },
              rows: { amount1: 200, amount2: 300 },
              Header: 'mock'
            },
          } as WrappedFieldInputProps
        }
        configType={NAME_GRID.overviewAccountCollection}
        meta={{} as WrappedFieldMetaProps}
        isFormat={true}
      />
    );

    expect(screen.getByText('$200.00')).toBeInTheDocument();
    expect(screen.getByText('$300.00')).toBeInTheDocument();
    expect(screen.getByText('$500.00')).toBeInTheDocument();
  });
});
