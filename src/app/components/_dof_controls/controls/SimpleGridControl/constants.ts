import { ColumnType } from 'app/_libraries/_dls/components';

export const CONFIG_GRID_OVERVIEW_ACCOUNT: ColumnType[] = [
  {
    id: 'cycle',
    Header: 'txt_cycle',
    accessor: 'cycle',
    cellProps: { style: { width: '100px' } }
  },
  {
    id: 'history',
    Header: 'txt_history',
    accessor: 'history',
    cellProps: { style: { width: '400px' } }
  }
];

export const CONFIG_GRID_OVERVIEW_COLLECTION: ColumnType[] = [
  {
    id: 'rowHeader',
    Footer: 'txt_total',
    accessor: 'rowHeader',
    cellProps: {
      style: {
        width: 100
      }
    },
    cellHeaderProps: { className: 'row-header' },
    cellBodyProps: { className: 'row-header' },
    cellFooterProps: { className: 'row-footer' }
  },
  {
    id: 'amount',
    Header: 'txt_amount',
    accessor: 'amount',
    cellProps: {
      style: {
        width: 220
      },
      className: 'text-right'
    }
  },
  {
    id: 'number',
    Header: 'txt_number',
    accessor: 'number'
  }
];

export const NAME_GRID = {
  overviewAccountHistory: 'overviewAccountHistory',
  overviewAccountCollection: 'overviewAccountCollection'
};
