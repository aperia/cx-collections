import React, { useEffect, useMemo, useRef, useState } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import isNumber from 'lodash.isnumber';
import isString from 'lodash.isstring';

// components
import {
  CheckBox,
  NumericProps,
  NumericTextBox,
  OnChangeNumericEvent
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

interface CheckBoxPropsType {
  readOnly: boolean;
}
export interface NumericWithCheckControlProps
  extends CommonControlProps,
    Omit<NumericProps, 'id' | 'label'> {
  forceInvalid?: boolean;
  checkboxLabel?: string;
  options?: {
    disabled?: boolean;
    min?: number | undefined;
    max?: number | undefined;
    autoFocus?: boolean;
    hasDivider?: boolean;
    keepValueAfterChecked?: boolean;
  };
  checkboxProps?: CheckBoxPropsType;
}

const NumericWithCheckControl: React.FC<NumericWithCheckControlProps> = ({
  input: { value: valueProp, onChange, onBlur, name },
  meta: { error, valid, invalid, touched },
  id,
  label,
  checkboxLabel,
  tooltip,
  enable,
  required,
  format,
  maxLength,
  readOnly,
  options,
  checkboxProps,
  dataTestId,
  ...props
}) => {
  const { t } = useTranslation();
  const inputRef = useRef<HTMLInputElement>(null);
  const [value, setValue] =
    useState<{ enable: boolean; rawValue: React.ReactText }>(valueProp);

  const [showError, setShowError] = useState(touched);

  const handleOnChange = (event: OnChangeNumericEvent) => {
    setValue(prev => ({ enable: prev.enable, rawValue: event.target.value }));
  };

  const handleOnBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    const newValue = { enable: value.enable, rawValue: event.target.value };
    onChange(newValue);
    onBlur(newValue);
    setShowError(true);
  };

  const handleChecked = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.persist();
    const checked = event.target.checked;
    if (options?.autoFocus && checked) {
      inputRef.current?.focus();
    }
    setValue(prev => {
      const newVal = {
        enable: checked,
        rawValue: options?.keepValueAfterChecked ? prev.rawValue : ''
      };
      onChange(newVal);
      return newVal;
    });
    setShowError(false);
  };

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  const renderMessageError = useMemo(() => {
    if (isString(error) || isNumber(error)) {
      return t(error);
    }
    return t(error?.messageCustom, { count: error?.count });
  }, [error, t]);

  return (
    <>
      {options?.hasDivider && <div className="divider-dashed mb-16" />}
      <div className="row align-items-center">
        <div className="col-6 col-lg-4">
          <CheckBox dataTestId={dataTestId}>
            <CheckBox.Input
              readOnly={checkboxProps?.readOnly}
              checked={value.enable}
              onChange={handleChecked}
            ></CheckBox.Input>
            <CheckBox.Label>{t(checkboxLabel)}</CheckBox.Label>
          </CheckBox>
        </div>
        <div className="col-6 col-lg-4 col-xl-3">
          <NumericTextBox
            id={id}
            dataTestId={dataTestId}
            ref={inputRef}
            label={t(label)}
            value={value.rawValue}
            format={format}
            maxLength={maxLength}
            required={value.enable && required}
            disabled={!value.enable}
            readOnly={checkboxProps?.readOnly}
            error={
              (showError &&
                value.enable &&
                enable &&
                required &&
                touched &&
                invalid) ||
              props?.forceInvalid
                ? {
                    message: t(renderMessageError),
                    status: invalid
                  }
                : undefined
            }
            onChange={handleOnChange}
            onBlur={handleOnBlur}
          />
        </div>
      </div>
    </>
  );
};

export default NumericWithCheckControl;
