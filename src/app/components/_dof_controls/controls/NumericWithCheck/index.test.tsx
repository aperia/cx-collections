import React from 'react';

// components
import NumericWithCheckControl from './index';

// RTL
import { act, fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';
import { queryByClass } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test NumericWithCheckControl control', () => {
  it('render component', () => {
    render(
      <NumericWithCheckControl
        id="id"
        input={
          {
            value: { enable: true, rawValue: 'rawValue' }
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        enable={true}
      />
    );

    expect(screen.getByRole('checkbox')).toBeInTheDocument();
    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });

  it('render component > error', () => {
    const wrapper = render(
      <NumericWithCheckControl
        id="id"
        input={
          {
            value: { enable: true, rawValue: 'rawValue' }
          } as WrappedFieldInputProps
        }
        meta={
          {
            error: 'Error Message',
            invalid: true,
            touched: true
          } as WrappedFieldMetaProps
        }
        options={{
          hasDivider: true
        }}
        enable={true}
        required
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      userEvent.click(inputElement);
    });

    expect(
      queryByClass(wrapper.container, /divider-dashed/)
    ).toBeInTheDocument();
    expect(screen.getByText('Error Message')).toBeInTheDocument();
  });

  it('trigger handleOnChange and handleOnBlur', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();

    render(
      <NumericWithCheckControl
        id="id"
        input={
          {
            value: { enable: true, rawValue: 'rawValue' },
            onChange: onChangeSpy,
            onBlur: onBlurSpy
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        enable={true}
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      fireEvent.change(inputElement, {
        target: { value: 'test' }
      });

      fireEvent.blur(inputElement, {
        target: { value: 'test' }
      });
    });

    expect(onChangeSpy).toBeCalled();
    expect(onBlurSpy).toBeCalled();
  });

  it('handleChecked', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();

    render(
      <NumericWithCheckControl
        id="id"
        input={
          {
            value: { enable: true, rawValue: 'rawValue' },
            onChange: onChangeSpy,
            onBlur: onBlurSpy
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        enable={true}
      />
    );

    const inputElement = screen.getByRole('checkbox');
    act(() => {
      fireEvent.click(inputElement, {
        target: { value: 'test' }
      });
    });

    expect(onChangeSpy).toBeCalled();
  });

  it('handleChecked', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();

    render(
      <NumericWithCheckControl
        id="id"
        input={
          {
            value: { enable: true, rawValue: 'rawValue' },
            onChange: onChangeSpy,
            onBlur: onBlurSpy
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        enable={true}
        options={{
          autoFocus: true,
          keepValueAfterChecked: true
        }}
      />
    );

    const inputElement = screen.getByRole('checkbox');
    act(() => {
      fireEvent.click(inputElement, {
        target: { checked: true }
      });
    });
    act(() => {
      userEvent.click(inputElement);
    });

    expect(onChangeSpy).toBeCalled();
  });
});
