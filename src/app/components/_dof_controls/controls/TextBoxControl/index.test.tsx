import React from 'react';
import TextBoxControl from './index';
import { act, fireEvent, render, screen } from '@testing-library/react';
import { getDefaultDOFMockProps, queryByClass } from 'app/test-utils';
import { WrappedFieldMetaProps } from 'redux-form';
import userEvent from '@testing-library/user-event';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test TextBoxControl component', () => {
  it('render component', () => {
    const props = getDefaultDOFMockProps();

    render(<TextBoxControl {...props} />);

    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });

  it('render component > error', () => {
    const props = getDefaultDOFMockProps();
    jest.useFakeTimers();

    render(
      <TextBoxControl
        {...props}
        meta={
          {
            error: 'Error Message',
            invalid: true,
            touched: true
          } as WrappedFieldMetaProps
        }
        options={{
          type: 'text',
          isError: true,
          minLength: 0,
          maxLength: 10
        }}
        enable={true}
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      userEvent.click(inputElement);
    });
    jest.runAllTimers();

    expect(screen.getByText('Error Message')).toBeInTheDocument();
  });

  it('render component > error', () => {
    const props = getDefaultDOFMockProps();
    jest.useFakeTimers();

    render(
      <TextBoxControl
        {...props}
        meta={
          {
            error: 'Error Message',
            invalid: true,
            touched: true
          } as WrappedFieldMetaProps
        }
        enable={true}
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      userEvent.click(inputElement);
    });
    jest.runAllTimers();

    expect(screen.getByText('Error Message')).toBeInTheDocument();
  });

  it('render component > error', () => {
    const props = getDefaultDOFMockProps();
    jest.useFakeTimers();

    const { container } = render(
      <TextBoxControl
        {...props}
        meta={{} as WrappedFieldMetaProps}
        options={{
          type: 'text',
          isError: true,
          minLength: 0,
          maxLength: 10
        }}
        enable={true}
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      userEvent.click(inputElement);
    });
    jest.runAllTimers();

    expect(queryByClass(container, /dls-error/)).toBeInTheDocument();
  });

  it('on blur > onChange and onBlur to be called', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    const props = getDefaultDOFMockProps({
      onChange: onChangeSpy,
      onBlur: onBlurSpy
    });

    render(<TextBoxControl {...props} />);

    const textbox = screen.getByRole('textbox');
    fireEvent.blur(textbox, {
      target: { value: 'test' }
    });

    expect(onChangeSpy).toBeCalledWith('test');
    expect(onBlurSpy).toBeCalledWith('test');
  });

  it('on blur > readOnly', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    const props = getDefaultDOFMockProps({
      onChange: onChangeSpy,
      onBlur: onBlurSpy
    });

    render(<TextBoxControl {...props} enable readOnly={true} />);

    const textbox = screen.getByRole('textbox');
    fireEvent.blur(textbox, {
      target: { value: 'test' }
    });

    expect(onBlurSpy).not.toBeCalled();
  });

  it('on change > set value successfully', () => {
    const props = getDefaultDOFMockProps();

    const wrapper = render(<TextBoxControl {...props} />);

    const textbox = screen.getByRole('textbox');
    fireEvent.change(textbox, {
      target: { value: 'test' }
    });

    expect(
      wrapper.container.querySelector('input[value="test"]')
    ).toBeInTheDocument();
  });

  it('on change > isUppercase', () => {
    const props = getDefaultDOFMockProps();

    const wrapper = render(
      <TextBoxControl
        {...props}
        options={{
          type: 'text',
          isUppercase: true,
          minLength: 0,
          maxLength: 10
        }}
      />
    );

    const textbox = screen.getByRole('textbox');
    fireEvent.change(textbox, {
      target: { value: 'test' }
    });

    expect(
      wrapper.container.querySelector('input[value="TEST"]')
    ).toBeInTheDocument();
  });

  it('on change > numberOnly', () => {
    const props = getDefaultDOFMockProps();

    const wrapper = render(
      <TextBoxControl
        {...props}
        options={{
          type: 'text',
          numberOnly: true,
          minLength: 0,
          maxLength: 10
        }}
      />
    );

    const textbox = screen.getByRole('textbox');
    fireEvent.change(textbox, {
      target: { value: 'a' }
    });

    expect(
      wrapper.container.querySelector('input[value="a"]')
    ).toBeInTheDocument();
  });

  it('on change > isAlphaNumeric', () => {
    const props = getDefaultDOFMockProps();

    const wrapper = render(
      <TextBoxControl
        {...props}
        options={{
          type: 'text',
          isAlphaNumeric: true,
          minLength: 0,
          maxLength: 10
        }}
      />
    );

    const textbox = wrapper.container.querySelector(
      'input'
    ) as HTMLInputElement;
    fireEvent.change(textbox, {
      target: { value: '#' }
    });

    expect(
      wrapper.container.querySelector('input[value="#"]')
    ).toBeInTheDocument();
  });

  it('on change > !isZip => return', () => {
    const props = getDefaultDOFMockProps();

    const wrapper = render(
      <TextBoxControl
        {...props}
        options={{
          type: 'text',
          isZip: true,
          minLength: 0,
          maxLength: 10
        }}
      />
    );

    const textbox = wrapper.container.querySelector(
      'input'
    ) as HTMLInputElement;
    fireEvent.change(textbox, {
      target: { value: 'xxx' }
    });

    expect(
      wrapper.container.querySelector('input[value="xxx"]')
    ).toBeInTheDocument();
  });

  it('on change > !isAlphaNumericAndSpace => return', () => {
    const props = getDefaultDOFMockProps();

    const wrapper = render(
      <TextBoxControl
        {...props}
        options={{
          type: 'text',
          isAlphaNumericAndSpace: true,
          minLength: 0,
          maxLength: 10
        }}
      />
    );

    const textbox = wrapper.container.querySelector(
      'input'
    ) as HTMLInputElement;
    fireEvent.change(textbox, {
      target: { value: '# 880 000' }
    });

    expect(
      wrapper.container.querySelector('input[value="# 880 000"]')
    ).toBeInTheDocument();
  });

  it('on change > alphabeticalOnly', () => {
    const props = getDefaultDOFMockProps();

    const wrapper = render(
      <TextBoxControl
        {...props}
        options={{
          type: 'text',
          alphabeticalOnly: true,
          minLength: 0,
          maxLength: 10
        }}
      />
    );

    const textbox = wrapper.container.querySelector(
      'input'
    ) as HTMLInputElement;
    fireEvent.change(textbox, {
      target: { value: '#' }
    });

    expect(
      wrapper.container.querySelector('input[value="#"]')
    ).toBeInTheDocument();
  });
});
