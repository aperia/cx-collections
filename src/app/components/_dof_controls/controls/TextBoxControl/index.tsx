import React, { useEffect, useState, useRef } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';

// Helper
import { stringValidate } from 'app/helpers';
import isUndefined from 'lodash.isundefined';
import pickBy from 'lodash.pickby';

// components
import {
  TextBox,
  TextBoxProps,
  TextBoxRef
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
export interface TextBoxControlProps
  extends CommonControlProps,
    Omit<TextBoxProps, 'id' | 'label'> {
  options?: {
    type: 'text' | 'number' | 'password';
    suffix?: string;
    numberOnly?: boolean;
    isZip?: boolean;
    alphabeticalOnly?: boolean;
    isAlphaNumeric?: boolean;
    isAlphaNumericAndSpace?: boolean;
    isUppercase?: boolean;
    className?: string;
    isError?: boolean;
    autoCompleteState?: 'no' | 'off' | 'on';
    maxLength: number;
    minLength: number;
    placeHolder?: string;
  };
}

export const TextBoxControl: React.FC<TextBoxControlProps> = ({
  input: { name, value: valueProp, onChange, onBlur },
  meta: { error: errorProp, valid, invalid, touched },
  id,
  dataTestId,
  label,
  enable,
  required,
  options,
  readOnly,
  placeholder,
  ...props
}) => {
  const [value, setValue] = useState<string>(valueProp);
  const textBoxRef = useRef<TextBoxRef | null>(null);
  const { t } = useTranslation();
  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    if (options?.numberOnly && !stringValidate(value).isNumber()) {
      return;
    }
    if (options?.isZip && !stringValidate(value).isZip()) {
      return;
    }
    if (options?.isAlphaNumeric && !stringValidate(value).isAlphanumeric()) {
      return;
    }
    if (options?.alphabeticalOnly && !stringValidate(value).isAlphabetical()) {
      return;
    }
    if (options?.isAlphaNumericAndSpace && !stringValidate(value).isAlphanumericAndSpace()) {
      return;
    }

    if (options?.isUppercase) {
      setValue(value.toUpperCase());
      return;
    }

    setValue(value);
  };

  const handleOnBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    if (!enable || readOnly) return;
    const { value: newValue } = event.target;
    onChange(newValue);
    onBlur(newValue);
  };

  const errorTooltipProps = {
    opened: !options?.isError || invalid ? undefined : false
  };

  if (textBoxRef.current?.inputElement) {
    textBoxRef.current.inputElement.autocomplete =
      options?.autoCompleteState || '';
  }
  return (
    <div className={options?.className}>
      <TextBox
        id={id}
        dataTestId={dataTestId}
        label={t(label)}
        ref={textBoxRef}
        disabled={!enable}
        value={value}
        type={options?.type}
        required={required}
        onChange={handleOnChange}
        onBlur={handleOnBlur}
        readOnly={readOnly}
        name={name}
        maxLength={options?.maxLength}
        minLength={options?.minLength}
        suffix={options?.suffix}
        placeholder={placeholder}
        error={
          (enable && touched && invalid) || options?.isError
            ? {
                message: t(errorProp) as string,
                status: Boolean(options?.isError) || invalid
              }
            : undefined
        }
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
      />
    </div>
  );
};

export default TextBoxControl;
