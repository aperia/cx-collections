import { TruncateText } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import classNames from 'classnames';
import React from 'react';

export interface DangerousHtmlControlProps extends CommonControlProps {
  options: {
    classNameProps: {
      container: string;
      text: string;
    };
  };
  dataTestId?: string;
}

const DangerousHtmlControl: React.FC<DangerousHtmlControlProps> = ({
  id,
  label,
  input: { value },
  dataTestId,
  options
}) => {
  const { t } = useTranslation();

  const { classNameProps } = options;

  return (
    <div
      className={classNames('form-group-static', classNameProps?.container)}
      id={id}
      data-testid={genAmtId(
        dataTestId!,
        'dangerous-html-control',
        'DangerousHtmlControl'
      )}
    >
      <span id={`${id}__label`} className="form-group-static__label">
        <TruncateText
          id={`${id}__label_text`}
          title={t(label)}
          dataTestId={dataTestId}
        >
          {t(label)}
        </TruncateText>
      </span>
      <div
        id={`${id}__text`}
        className={classNames('form-group-static__text white-space-pre-line', classNameProps?.text)}
        dangerouslySetInnerHTML={{
          __html: value
        }}
      ></div>
    </div>
  );
};

export default DangerousHtmlControl;
