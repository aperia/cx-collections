import React from 'react';

// RTL
import { render, screen } from '@testing-library/react';

// components
import DangerousHtmlControl from './index';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test DangerousHtmlControl controls', () => {
  it('render component', () => {
    const mockOptions = {
      classNameProps: {
        container: 'container',
        text: 'text'
      }
    };
    render(
      <DangerousHtmlControl
        id="id"
        label="Label"
        options={mockOptions}
        input={{ value: 'value' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );

    expect(screen.getByText('Label'));
    expect(screen.getByText('value'));
  });
});
