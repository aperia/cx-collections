import React from 'react';

// RTL
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

// components
import GroupRadioControl from './index';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

jest.mock('app/_libraries/_dls/hooks', () => {
  const hooks = jest.requireActual('app/_libraries/_dls/hooks');

  return {
    ...hooks,
    useTranslation: () => ({ t: (text: string) => text })
  };
});

describe('GroupRadioControl component', () => {
  const mockOnChange = jest.fn();
  const inputData = {
    value: {
      first: false,
      second: false
    },
    onChange: mockOnChange
  } as unknown as WrappedFieldInputProps;

  it('should render two radio input', () => {
    render(
      <GroupRadioControl
        id="test"
        input={inputData}
        meta={{} as WrappedFieldMetaProps}
      />
    );

    const radios = screen.getAllByRole('radio');
    expect(radios.length).toEqual(2);
  });

  it('should render two radio input', () => {
    render(
      <GroupRadioControl
        id="test"
        input={inputData}
        meta={{} as WrappedFieldMetaProps}
      />
    );

    const radios = screen.getAllByRole('radio');
    userEvent.click(radios[0]);
    userEvent.click(radios[1]);

    expect(mockOnChange).toBeCalled();
  });
});
