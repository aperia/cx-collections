import React from 'react';

// types
import { CommonControlProps } from 'app/_libraries/_dof/core';

// helpers
import classnames from 'classnames';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import { Radio } from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface GroupRadioControlProps extends CommonControlProps {
  dataTestId?: string;
  options?: {
    firstLabel?: string;
    secondLabel?: string;
    className?: string;
  };
}

const GroupRadioControl: React.FC<GroupRadioControlProps> = ({
  input: { name, value: valueProp, onChange },
  meta: { error, valid, invalid, touched },
  options = {},
  id,
  dataTestId,
  label,
  tooltip,
  readOnly,
  required,
  enable,
  ...props
}) => {
  const { t } = useTranslation();

  const { firstLabel, secondLabel, className } = options;

  return (
    <div id={id} className={classnames(className)}>
      <span className="mr-24 fw-500 color-grey">{t(label)}:</span>
      <Radio
        className="d-inline-block mr-24"
        dataTestId={genAmtId(
          dataTestId!,
          'group-radio_first_option',
          'GroupRadioControl'
        )}
      >
        <Radio.Input
          name={name}
          checked={valueProp?.first}
          onChange={() =>
            onChange({
              first: true,
              second: false
            })
          }
        />
        <Radio.Label>{t(firstLabel)}</Radio.Label>
      </Radio>

      <Radio
        className="d-inline-block"
        dataTestId={genAmtId(
          dataTestId!,
          'group-radio_second_option',
          'GroupRadioControl'
        )}
      >
        <Radio.Input
          name={name}
          checked={valueProp?.second}
          onChange={() =>
            onChange({
              first: false,
              second: true
            })
          }
        />
        <Radio.Label>{t(secondLabel)}</Radio.Label>
      </Radio>
    </div>
  );
};

export default GroupRadioControl;
