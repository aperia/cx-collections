import React from 'react';

// components
import ComboBoxControl from './index';

// helpers
import { queryAllByClass } from 'app/test-utils';

// RTL
import { act, fireEvent, render, screen } from '@testing-library/react';
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';
import userEvent from '@testing-library/user-event';

import * as entitlementsHelpers from 'app/entitlements/helpers';
import * as roleHelper from 'app/entitlements/helpers';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const mockData = [
  { value: 'value 1', description: 'description 1' },
  { value: 'value 2', description: 'description 2' }
];

describe('Test ComboBoxControl', () => {
  beforeEach(() => {
    jest.spyOn(roleHelper, 'checkPermission').mockReturnValue(true);
  });

  it('render component', () => {
    jest.useFakeTimers();
    const wrapper = render(
      <div>
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={{ value: '' } as WrappedFieldInputProps}
          meta={{} as WrappedFieldMetaProps}
          textField="description"
          keyField="value"
          data={mockData as [] | undefined}
          enable
        />
      </div>
    );

    const element = screen.getByRole('textbox');
    act(() => {
      userEvent.type(element!, 'd');
    });
    jest.runAllTimers();

    expect(screen.getByRole('textbox')).toBeInTheDocument();
    expect(
      queryAllByClass(wrapper.baseElement as HTMLElement, 'item').length
    ).toEqual(2);
  });

  it('render component > default', () => {
    jest.useFakeTimers();
    render(
      <div>
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={{ value: '' } as WrappedFieldInputProps}
          meta={{} as WrappedFieldMetaProps}
          enable
        />
      </div>
    );

    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });

  it('render component > options.combineValues = true', () => {
    jest.useFakeTimers();
    render(
      <div>
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={{ value: '' } as WrappedFieldInputProps}
          meta={{} as WrappedFieldMetaProps}
          textField="description"
          keyField="value"
          data={mockData as [] | undefined}
          options={{
            combineValues: true,
            isError: false
          }}
          enable
        />
      </div>
    );

    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });

  it('render component > options.combineValues = true, having uniqueField', () => {
    jest.useFakeTimers();
    render(
      <div>
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={{ value: '' } as WrappedFieldInputProps}
          meta={{} as WrappedFieldMetaProps}
          textField="description"
          keyField="value"
          data={mockData as [] | undefined}
          options={{
            combineValues: true,
            isError: false
          }}
          uniqueField="value"
          enable
        />
      </div>
    );

    const element = screen.getByRole('textbox');
    act(() => {
      userEvent.type(element!, 'd');
    });
    jest.runAllTimers();

    expect(screen.getByText('value 1 -')).toBeInTheDocument();
    expect(screen.getByText('value 2 -')).toBeInTheDocument();
  });

  it('render component > having value', () => {
    jest.useFakeTimers();
    const wrapper = render(
      <div>
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={{ value: mockData[0].value } as WrappedFieldInputProps}
          meta={{} as WrappedFieldMetaProps}
          textField="description"
          keyField="value"
          data={mockData as [] | undefined}
          enable
        />
      </div>
    );
    jest.runAllTimers();
    expect(
      wrapper.container.querySelector('input[value="description 1"]')
    ).toBeInTheDocument();
  });

  it('render component > having value, no keyField', () => {
    jest.useFakeTimers();
    const wrapper = render(
      <div>
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={{ value: mockData[0].value } as WrappedFieldInputProps}
          meta={{} as WrappedFieldMetaProps}
          textField="description"
          data={mockData as [] | undefined}
          enable
        />
      </div>
    );
    jest.runAllTimers();

    expect(
      wrapper.container.querySelector('input[value="value 1"]')
    ).toBeInTheDocument();
  });

  it('render component > error with meta', () => {
    jest.useFakeTimers();
    render(
      <div>
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={{ value: '' } as WrappedFieldInputProps}
          meta={
            {
              touched: true,
              invalid: true,
              error: 'Error Message'
            } as WrappedFieldMetaProps
          }
          textField="description"
          keyField="value"
          data={mockData as [] | undefined}
          enable
          required
        />
      </div>
    );

    const element = screen.getByRole('textbox');
    act(() => {
      userEvent.click(element!);
    });
    jest.runAllTimers();

    expect(screen.getByText('Error Message')).toBeInTheDocument();
  });

  it('render component > error with options', () => {
    jest.useFakeTimers();
    render(
      <div data-testid="ComboBoxControl_errorMessage">
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={{ value: '' } as WrappedFieldInputProps}
          meta={
            {
              touched: false,
              invalid: false,
              error: 'Error Message from options'
            } as WrappedFieldMetaProps
          }
          options={{
            isError: true
          }}
          textField="description"
          keyField="value"
          data={mockData as [] | undefined}
          enable
          required
        />
      </div>
    );

    const element = screen.getByRole('textbox');
    act(() => {
      userEvent.click(element!);
    });
    jest.runAllTimers();

    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });

  it('clicks on item', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    jest.useFakeTimers();
    const wrapper = render(
      <div>
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={
            {
              value: '',
              onChange: onChangeSpy,
              onBlur: onBlurSpy
            } as unknown as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          textField="description"
          keyField="value"
          data={mockData as [] | undefined}
          enable
        />
      </div>
    );

    const element = screen.getByRole('textbox');
    act(() => {
      userEvent.type(element!, 'd');
    });
    jest.runAllTimers();

    const items = queryAllByClass(wrapper.baseElement as HTMLElement, 'item');
    act(() => {
      userEvent.click(items[0]);
    });
    jest.runAllTimers();

    expect(onChangeSpy).toBeCalled();
    expect(onBlurSpy).toBeCalled();
  });

  it('clicks on item > readOnly', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    jest.useFakeTimers();
    const wrapper = render(
      <div>
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={
            {
              value: '',
              onChange: onChangeSpy,
              onBlur: onBlurSpy
            } as unknown as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          textField="description"
          keyField="value"
          data={mockData as [] | undefined}
          enable
          readOnly
        />
      </div>
    );

    const element = screen.getByRole('textbox');
    act(() => {
      userEvent.click(element!);
      fireEvent.blur(element);
    });
    jest.runAllTimers();

    expect(
      queryAllByClass(wrapper.baseElement as HTMLElement, 'item').length
    ).toEqual(0);
    expect(onChangeSpy).not.toBeCalled();
    expect(onBlurSpy).not.toBeCalled();
  });

  it('clicks on item > without keyField', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    jest.useFakeTimers();
    render(
      <div>
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={
            {
              value: undefined,
              onChange: onChangeSpy,
              onBlur: onBlurSpy
            } as unknown as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          textField="description"
          data={mockData as [] | undefined}
          enable
        />
      </div>
    );

    const element = screen.getByRole('textbox');
    act(() => {
      userEvent.type(element!, 'd');
    });
    jest.runAllTimers();

    act(() => {
      fireEvent.blur(element);
    });
    jest.runAllTimers();

    expect(onChangeSpy).toBeCalled();
    expect(onBlurSpy).toBeCalled();
  });

  it('render component > no permission', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(false);
    jest.useFakeTimers();
    render(
      <div>
        <ComboBoxControl
          dataTestId=""
          id="id"
          input={{ value: '' } as WrappedFieldInputProps}
          meta={{} as WrappedFieldMetaProps}
          textField="description"
          keyField="value"
          data={mockData as [] | undefined}
          enable
          entitlementConfig={{ entitlementCode: 'edit' }}
        />
      </div>
    );
  });
});
