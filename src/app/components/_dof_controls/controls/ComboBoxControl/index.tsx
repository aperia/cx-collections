import React, { useEffect, useMemo, useState } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';

// utils
import isUndefined from 'lodash.isundefined';

// components
import {
  ComboBox,
  ComboBoxProps,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import isEmpty from 'lodash.isempty';
import pickBy from 'lodash.pickby';
import { checkPermission } from 'app/entitlements';
import { useAccountDetail } from 'app/hooks';

export interface ComboBoxControlProps
  extends CommonControlProps,
    DOFEntitlement,
    Omit<ComboBoxProps, 'id' | 'label' | 'children'> {
  options?: { combineValues?: boolean; isError: boolean };
  uniqueField?: string;
  keyField?: string;
  dataTestId: string;
}

export const ComboBoxControl: React.FC<ComboBoxControlProps> = ({
  id,
  dataTestId,
  label,
  enable,
  data = [],
  input: { value: valueProp, onChange, onBlur },
  meta: { touched, error, invalid },
  textField = 'fieldValue',
  keyField,
  options,
  uniqueField,
  readOnly,
  required,
  entitlementConfig,

  ...props
}) => {
  const { t } = useTranslation();
  const [value, setValue] = useState<MagicKeyValue>(valueProp);
  const { storeId } = useAccountDetail();

  const handleOnChange = (event: DropdownBaseChangeEvent) => {
    setValue(event.target.value);
  };

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    if (!enable || readOnly) return;
    let truthyValue = isUndefined(value) ? null : value;

    if (keyField) {
      truthyValue = truthyValue && value?.[keyField];
    }
    onChange(truthyValue);
    onBlur(truthyValue);
  };

  const newData = useMemo(() => {
    if (!options?.combineValues || !uniqueField) {
      return data;
    }
    return data.map(item => ({
      ...(item as object),
      [textField]: `${item[uniqueField]} - ${item[textField]}`
    }));
  }, [data, options?.combineValues, textField, uniqueField]);

  // find the reference of binding props
  useEffect(() => {
    if (keyField && typeof valueProp === 'string') {
      const selectedValue =
        newData.find(item => item?.[keyField] === valueProp) || {};
      if (!isEmpty(selectedValue)) {
        return setValue(selectedValue);
      }
    }
    setValue(valueProp);
  }, [data, keyField, valueProp, newData]);

  const errorTooltipProps = {
    opened: !options?.isError || invalid ? undefined : false
  };

  if (
    entitlementConfig?.entitlementCode &&
    !checkPermission(entitlementConfig.entitlementCode, storeId)
  ) {
    return null;
  }

  return (
    <ComboBox
      id={id}
      dataTestId={`${dataTestId}-combobox-control`}
      label={t(label)}
      value={value}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      textField={textField}
      disabled={!enable}
      readOnly={readOnly}
      required={required}
      noResult={t('txt_no_results_found')}
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
      error={
        (enable && touched && invalid && required) || options?.isError
          ? {
              message: t(error),
              status: invalid || Boolean(options?.isError)
            }
          : undefined
      }
      errorTooltipProps={pickBy(errorTooltipProps, item => !isUndefined(item))}
      {...props}
    >
      {newData.map((item, index) => {
        return (
          <ComboBox.Item
            value={item}
            label={item[textField]}
            key={index}
            {...props}
          />
        );
      })}
    </ComboBox>
  );
};

export default ComboBoxControl;
