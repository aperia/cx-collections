import React, { useRef } from 'react';

// Types
import { CommonControlProps } from 'app/_libraries/_dof/core';

// Helpers
import {
  convertAPIDateToView,
  formatAPICurrency,
  formatCommon,
  formatContactPhone,
  formatMaskNumber,
  formatPercent
} from 'app/helpers';
import classNames from 'classnames';

// Constant
import {
  FormatTime,
  GroupTextFormat as FormatEnum,
  GroupTextFormatType
} from 'app/constants/enums';
import {
  COMMON_STATUS,
  TRANSACTION_CODE,
  PRESENTATION_INSTRUMENT_TYPE_CODE,
  RISK_IDENTIFICATION_SERVICE_INDICATOR,
  AUTHORIZATION_SOURCE_CODE,
  SOURCE_TRANSACTION_IDENTIFIER,
  MAIL_PHONE_INDICATOR,
  DUALITY_FLAG,
  AUTOMATED_TELLER_MACHINE_ATM_FLAG,
  POINT_OF_SALE_ENTRY_MODE,
  AUTHORIZATION_METHOD_CODE,
  AUTHORIZATION_FILTER_METHOD_CODE,
  HELD_STATEMENT_DESTINATION_CODE,
  NAME_MATCH_CODE
} from 'app/constants';

// Redux store
import isNil from 'lodash.isnil';
import isString from 'lodash.isstring';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { Badge, TruncateText } from 'app/_libraries/_dls/components';

// util
import dateTime from 'date-and-time';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface GroupTextControlProps extends CommonControlProps {
  dataTestId?: string;
  options?: {
    maxDot?: number;
    format?: FormatEnum;
    fractionDigits?: number;
    formatType?: GroupTextFormatType;
    classNameProps?: {
      container?: string;
      formatText?: string;
    };
    hiddenNoValue?: boolean;
    classNameValue?: string;
    className?: string;
    truncateLines?: number;
    outputAPIDate?: FormatTime;
    inputAPIDate?: string;
    inputDateFormat?: string;
    outputDateFormat?: string;
  };
}

interface TwoLinesAddressFormat {
  addressLineOne?: string;
  addressLineTwo?: string;
}

interface FiveLinesAddressFormat {
  addressLineOne?: string;
  addressLineTwo?: string;
  addressLineThree?: string;
  addressLineFour?: string;
  addressLineFive?: string;
}

interface TwoLinesHourFormat {
  hourLineOne?: string;
  hourLineTwo?: string;
}

const GroupTextControl: React.FC<GroupTextControlProps> = ({
  id,
  label,
  options = {},
  input: { value },
  dataTestId
}) => {
  const { t } = useTranslation();
  const {
    format = FormatEnum.Text,
    fractionDigits = 2,
    formatType,
    hiddenNoValue,
    classNameProps,
    className,
    classNameValue,
    truncateLines,
    outputAPIDate,
    inputAPIDate,
    inputDateFormat = 'MM-DD-YYYY',
    outputDateFormat = 'MM/DD/YYYY'
  } = options;
  const truncateFollowRef = useRef<HTMLDivElement | null>(null);

  const noTruncate = (val: string) => (
    <span id={id} className={classNames(className)}>
      {val}
    </span>
  );

  const truncateText = (val: string) => (
    <TruncateText
      id={`${id}__truncate-text`}
      title={val}
      lines={1}
      dataTestId={dataTestId}
    >
      {val}
    </TruncateText>
  );

  const truncateTextLessMore = (val: string) => (
    <TruncateText
      id={`${id}__truncate-less-more`}
      resizable
      ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
      ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
      className="text-break"
      title={val}
      lines={truncateLines}
      dataTestId={dataTestId}
    >
      {val}
    </TruncateText>
  );

  const renderMaskText = (val: string) => {
    const mask = formatMaskNumber(val, options?.maxDot);
    if (!mask) return <span className="form-group-static__text" />;

    return (
      <div className={classNames('data-value ', classNameProps?.formatText)}>
        <span className="custom-account-number">
          {mask.firstText}
          <span className="hide-account-icon">{mask.maskText}</span>
          {mask.lastText}
        </span>
      </div>
    );
  };

  const renderFormatMaskText = (text: string) => {
    const arr: number[] = [];
    for (let i = 0; i < text.length; i++) {
      if (isNaN(parseInt(text[i], 10))) {
        arr.push(i);
      }
    }
    let j, arrRender: string | React.ReactNode[];
    if (arr[0] < 1) {
      arrRender = [];
    } else {
      arrRender = [text.slice(0, arr[0])];
    }
    let start = arr[0];
    for (j = 0; j < arr.length; j++) {
      if (arr[j + 1] - arr[j] > 1 || j === arr.length - 1) {
        let maskText = '·';
        for (let i = 1; i < arr[j] + 1 - start; i++) {
          maskText = maskText + '·';
        }
        start = arr[j + 1];
        arrRender.push(<span className="hide-account-icon">{maskText}</span>);
        if (j < arr.length - 1) {
          arrRender.push(text.slice(arr[j] + 1, arr[j + 1]));
        }
      }
    }
    if (arr[arr.length - 1] < text.length - 1) {
      arrRender.push(text.slice(arr[arr.length - 1] + 1, text.length));
    }
    return (
      <span className="data-value">
        <span className="custom-account-number">
          {arrRender.map((item, index) => (
            <React.Fragment key={index}>{item}</React.Fragment>
          ))}
        </span>
      </span>
    );
  };

  const renderFormatType = (val: string, type?: GroupTextFormatType) => {
    let text = '';
    switch (type) {
      case GroupTextFormatType.TransactionCode: {
        text = TRANSACTION_CODE[val]
          ? `${val} - ${t(TRANSACTION_CODE[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.NameMatchCode: {
        text = NAME_MATCH_CODE[val]
          ? `${val} - ${t(NAME_MATCH_CODE[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.PresentationInstrumentTypeCode: {
        text = PRESENTATION_INSTRUMENT_TYPE_CODE[val]
          ? `${val} - (${val}) ${t(PRESENTATION_INSTRUMENT_TYPE_CODE[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.RiskIdentificationServiceIndicator: {
        text = RISK_IDENTIFICATION_SERVICE_INDICATOR[val]
          ? `${val} - (${val}) ${t(RISK_IDENTIFICATION_SERVICE_INDICATOR[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.AuthorizationSourceCode: {
        text = AUTHORIZATION_SOURCE_CODE[val]
          ? `${val} - (${val}) ${t(AUTHORIZATION_SOURCE_CODE[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.SourceTransactionIdentifier: {
        text = SOURCE_TRANSACTION_IDENTIFIER[val]
          ? `${val} - (${val}) ${t(SOURCE_TRANSACTION_IDENTIFIER[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.MailPhoneIndicator: {
        text = MAIL_PHONE_INDICATOR[val]
          ? `${val} - (${val}) ${t(MAIL_PHONE_INDICATOR[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.DualityFlag: {
        text = DUALITY_FLAG[val]
          ? `${val} - (${val}) ${t(DUALITY_FLAG[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.AutomatedTellerMachineAtmFlag: {
        text = AUTOMATED_TELLER_MACHINE_ATM_FLAG[val]
          ? `${val} - (${val}) ${t(AUTOMATED_TELLER_MACHINE_ATM_FLAG[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.PointOfSaleEntryMode: {
        text = POINT_OF_SALE_ENTRY_MODE[val]
          ? `${val} - (${val}) ${t(POINT_OF_SALE_ENTRY_MODE[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.AuthorizationMethodCode: {
        text = AUTHORIZATION_METHOD_CODE[val]
          ? `${val} - (${val}) ${t(AUTHORIZATION_METHOD_CODE[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.AuthorizationFilterCode: {
        text = AUTHORIZATION_FILTER_METHOD_CODE[val]
          ? `${val} - (${val}) ${t(AUTHORIZATION_FILTER_METHOD_CODE[val])}`
          : val;
        break;
      }
      case GroupTextFormatType.HeldStatementDestinationCode: {
        text = HELD_STATEMENT_DESTINATION_CODE[val]
          ? `${val} - ${t(HELD_STATEMENT_DESTINATION_CODE[val])}`
          : val;
        break;
      }
      default:
        text = '';
    }
    return (
      text && (
        <TruncateText
          id={`${id}__truncate-text`}
          title={text}
          lines={2}
          ellipsisMoreText="more"
          ellipsisLessText="less"
          dataTestId={dataTestId}
        >
          {text}
        </TruncateText>
      )
    );
  };

  const renderTwoLinesAddress = (val: TwoLinesAddressFormat) => {
    if (!(val.addressLineOne && val.addressLineTwo)) {
      return <span className="d-block form-group-static__text" />;
    }

    return (
      <>
        <span
          className="d-block form-group-static__text content-empty-none"
          id={`${id}__address`}
        >
          {val.addressLineOne}
        </span>
        <span
          className="d-block form-group-static__text content-empty-none"
          id={`${id}__state`}
        >
          {val.addressLineTwo}
        </span>
      </>
    );
  };

  const renderFiveLinesAddress = (val: FiveLinesAddressFormat) => {
    if (
      !(
        val.addressLineOne &&
        val.addressLineTwo &&
        val.addressLineThree &&
        val.addressLineFour &&
        val.addressLineFive
      )
    ) {
      return <span className="d-block form-group-static__text" />;
    }

    return (
      <>
        <span
          className="d-block form-group-static__text content-empty-none"
          id={`${id}__addresslineone`}
        >
          {val.addressLineOne}
        </span>
        <span
          className="d-block form-group-static__text content-empty-none"
          id={`${id}__addresslinetwo`}
        >
          {val.addressLineTwo}
        </span>
        <span
          className="d-block form-group-static__text content-empty-none"
          id={`${id}__addresslinethree`}
        >
          {val.addressLineThree}
        </span>
        <span
          className="d-block form-group-static__text content-empty-none"
          id={`${id}__addresslinefour`}
        >
          {val.addressLineFour}
        </span>
        <span
          className="d-block form-group-static__text content-empty-none"
          id={`${id}__addresslinefive`}
        >
          {val.addressLineFive}
        </span>
      </>
    );
  };

  const renderTwoLinesHour = (val: TwoLinesHourFormat) => {
    if (!(val.hourLineOne && val.hourLineTwo)) {
      return <span className="d-block form-group-static__text" />;
    }

    return (
      <>
        <span
          className="d-block form-group-static__text content-empty-none text-break"
          id={`${id}__address`}
        >
          {val.hourLineOne}
        </span>
        <span
          className="d-block form-group-static__text content-empty-none text-break"
          id={`${id}__state`}
        >
          {val.hourLineTwo}
        </span>
      </>
    );
  };

  const renderAmount = (val: string, isRealData = false) => {
    if (!val) {
      return undefined;
    }

    const content = isRealData
      ? formatAPICurrency(val)
      : formatCommon(val).currency(fractionDigits);
    return truncateText(content);
  };

  const renderStatusBadge = (val: string) => {
    if (isString(val) && val.trim().length === 0) {
      return (
        <Badge
          id={`${id}__badge`}
          color={COMMON_STATUS.Blank}
          textTransformNone
          dataTestId={dataTestId}
        />
      );
    }

    const color = COMMON_STATUS[val];
    const text = val;

    return (
      text && (
        <Badge
          id={`${id}__badge`}
          color={color}
          textTransformNone
          dataTestId={dataTestId}
        >
          <TruncateText
            id={`${id}__truncate-text`}
            title={text}
            followRef={truncateFollowRef}
            dataTestId={dataTestId}
          >
            {text}
          </TruncateText>
        </Badge>
      )
    );
  };

  // That function used generic type, so that we can't define the specifically type
  const formatGeneration = (val: any) => {
    switch (format) {
      case FormatEnum.Number:
        return formatCommon(val).number;
      case FormatEnum.Date:
        return formatCommon(val).time.date;
      case FormatEnum.ShortMonthYear:
        return formatCommon(val).time.shortMonthYear;
      case FormatEnum.ShortYearMonth:
        return formatCommon(val).time.shortYearMonth;
      case FormatEnum.ShortMonthShortYear:
        return formatCommon(val).time.shortMonthShortYear;
      case FormatEnum.Datetime:
        return formatCommon(val).time.datetime;
      case FormatEnum.MonthDate:
        return formatCommon(val).time.monthDate;
      case FormatEnum.Amount:
        return renderAmount(val);
      case FormatEnum.Status:
        return renderStatusBadge(val);
      case FormatEnum.TwoLinesAddress:
        return renderTwoLinesAddress(val);
      case FormatEnum.FiveLinesAddress:
        return renderFiveLinesAddress(val);
      case FormatEnum.FormatType:
        return renderFormatType(val, formatType);
      case FormatEnum.Mask:
        return renderMaskText(val);
      case FormatEnum.MaskText:
        return renderFormatMaskText(val);
      case FormatEnum.PhoneNumber:
        return formatContactPhone(val);
      case FormatEnum.Percent:
        return formatPercent(val, fractionDigits);
      case FormatEnum.CardNumber:
        return formatCommon(val).cardNumber;
      case FormatEnum.Truncate:
        return truncateText(val);
      case FormatEnum.TruncateLessMore:
        return truncateTextLessMore(val);
      case FormatEnum.Hour:
        return renderTwoLinesHour(val);
      case FormatEnum.Text:
        return ((val ? val : '') + '')?.trim();
      case FormatEnum.Label:
        return t(val);
      case FormatEnum.Api_Amount:
        return renderAmount(val, true);
      case FormatEnum.Api_Date:
        return convertAPIDateToView(val, inputAPIDate, outputAPIDate);
      case FormatEnum.CustomDate:
        return dateTime.format(
          dateTime.parse(val, inputDateFormat),
          outputDateFormat
        );

      default:
        return noTruncate(val);
    }
  };

  const textValue =
    !isNil(value) || format === FormatEnum.Status
      ? formatGeneration(value)
      : '';

  if (hiddenNoValue && value === '') {
    return null;
  }

  return (
    <div
      className={classNames(classNameProps?.container, 'form-group-static')}
      id={id}
    >
      <span
        id={`${id}__label`}
        className="form-group-static__label"
        data-testid={genAmtId(
          dataTestId!,
          'group-text-control_label',
          'GroupTextControl'
        )}
      >
        <TruncateText
          id={`${id}__label_text`}
          title={t(label)}
          dataTestId={dataTestId}
        >
          {t(label)}
        </TruncateText>
      </span>
      <div
        id={`${id}__text`}
        className={classNames('form-group-static__text', classNameValue)}
        ref={truncateFollowRef}
        data-testid={genAmtId(
          dataTestId!,
          'group-text-control_value',
          'GroupTextControl'
        )}
      >
        {isString(textValue) ? t(textValue) : textValue}
      </div>
    </div>
  );
};

export default GroupTextControl;
