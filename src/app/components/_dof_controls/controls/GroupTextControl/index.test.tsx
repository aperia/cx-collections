import React from 'react';
import dateTime from 'date-and-time';

// components
import GroupTextControl from './index';

// RTL
import { render, screen } from '@testing-library/react';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

// helpers
import { queryByClass, queryById } from 'app/test-utils';

// constants
import {
  FormatTime,
  GroupTextFormat,
  GroupTextFormatType
} from 'app/constants/enums';
import {
  NAME_MATCH_CODE,
  AUTHORIZATION_FILTER_METHOD_CODE,
  AUTHORIZATION_METHOD_CODE,
  AUTHORIZATION_SOURCE_CODE,
  AUTOMATED_TELLER_MACHINE_ATM_FLAG,
  DUALITY_FLAG,
  HELD_STATEMENT_DESTINATION_CODE,
  MAIL_PHONE_INDICATOR,
  POINT_OF_SALE_ENTRY_MODE,
  PRESENTATION_INSTRUMENT_TYPE_CODE,
  RISK_IDENTIFICATION_SERVICE_INDICATOR,
  SOURCE_TRANSACTION_IDENTIFIER,
  TRANSACTION_CODE
} from 'app/constants';
import { queryAllById } from 'app/_libraries/_dls/test-utils';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

jest.mock('react-redux', () => {
  return {
    ...(jest.requireActual('react-redux') as object),
    useDispatch: () => (x: Function) => jest.fn().mockImplementation(() => x())
  };
});

jest.mock('app/_libraries/_dls/hooks', () => {
  return {
    ...jest.requireActual('app/_libraries/_dls/hooks'),
    useTranslation: () => ({ t: (text: string) => text })
  };
});

describe('GroupTextControl component', () => {
  it('should show text', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: 'test'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
      />
    );

    const divElement = queryByClass(container, 'form-group-static__text');
    expect(divElement?.innerHTML).toEqual('test');
  });

  it('should show number', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '1'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Number
        }}
      />
    );

    const divElement = queryByClass(container, 'form-group-static__text');
    expect(divElement?.innerHTML).toEqual('1');
  });

  it('should show Date', () => {
    const value = new Date('03/31/2021 07:00');

    const { container } = render(
      <GroupTextControl
        id="test"
        input={{ value } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        options={{ format: GroupTextFormat.Date }}
      />
    );

    const divElement = queryByClass(container, 'form-group-static__text');
    expect(divElement?.innerHTML).toEqual(
      dateTime.format(value, FormatTime.Date, true)
    );
  });

  it('should show ShortMonthYear', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: new Date('03/31/2021')
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.ShortMonthYear
        }}
      />
    );

    const divElement = queryByClass(container, 'form-group-static__text');
    expect(divElement?.innerHTML).toEqual('03/2021');
  });

  it('should show ShortYearMonth', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: new Date('03/31/2021')
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.ShortYearMonth
        }}
      />
    );

    const divElement = queryByClass(container, 'form-group-static__text');
    expect(divElement?.innerHTML).toEqual('21/03');
  });

  it('should show ShortMonthShortYear', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: new Date('03/31/2021')
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.ShortMonthShortYear
        }}
      />
    );

    const divElement = queryByClass(container, 'form-group-static__text');
    expect(divElement?.innerHTML).toEqual('03/21');
  });

  it('should show Datetime', () => {
    const value = new Date('03/31/2021 07:00');

    const { container } = render(
      <GroupTextControl
        id="test"
        input={{ value } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        options={{ format: GroupTextFormat.Datetime }}
      />
    );

    const divElement = queryByClass(container, 'form-group-static__text');
    expect(divElement?.innerHTML).toEqual('03/31/2021 07:00 AM');
  });

  it('should show MonthDate', () => {
    const value = new Date('03/31/2021');

    const { container } = render(
      <GroupTextControl
        id="test"
        input={{ value } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        options={{ format: GroupTextFormat.MonthDate }}
      />
    );

    const divElement = queryByClass(container, 'form-group-static__text');
    expect(divElement?.innerHTML).toEqual(
      dateTime.format(value, FormatTime.MonthDate)
    );
  });

  it('should show Amount', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value: 1000
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Amount
        }}
      />
    );

    expect(screen.getByText('$1,000.00')).toBeInTheDocument();
  });

  it('should show Amount no data', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: ''
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Amount
        }}
      />
    );

    const divEle = queryByClass(container, 'form-group-static__text');

    expect(divEle?.innerHTML).toEqual('');
  });

  it('should show Status', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value: 'normal'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Status
        }}
      />
    );

    expect(screen.getByText('normal')).toBeInTheDocument();
  });

  it('should show TwoLinesAddress', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value: {
              addressLineOne: 'addressLineOne',
              addressLineTwo: 'addressLineTwo'
            }
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.TwoLinesAddress
        }}
      />
    );

    expect(screen.getByText('addressLineOne')).toBeInTheDocument();
    expect(screen.getByText('addressLineTwo')).toBeInTheDocument();
  });

  it('should show TwoLinesAddress no data', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: {}
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.TwoLinesAddress
        }}
      />
    );

    const divEle = queryByClass(container, 'form-group-static__text');
    expect(divEle?.innerHTML).toEqual(
      '<span class="d-block form-group-static__text"></span>'
    );
  });

  it('should show Hour', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: {
              hourLineOne: '1',
              hourLineTwo: '2'
            }
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Hour
        }}
      />
    );
    const lineOneEle = queryById(container, 'test__address');
    const lineTwoEle = queryById(container, 'test__state');
    expect(lineOneEle?.innerHTML).toEqual('1');
    expect(lineTwoEle?.innerHTML).toEqual('2');
  });

  it('should show Hour no data', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: {}
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Hour
        }}
      />
    );
    const divEle = queryById(container, 'test__text');
    expect(divEle?.innerHTML).toEqual(
      '<span class="d-block form-group-static__text"></span>'
    );
  });

  it('should show no truncate text', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value: 'test'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: 'other' as unknown as GroupTextFormat
        }}
      />
    );
    expect(screen.getByText('test')).toBeInTheDocument();
  });

  it('should show Truncate', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value: 'test'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Truncate
        }}
      />
    );

    expect(screen.getByText('test')).toBeInTheDocument();
  });

  it('should show CardNumber', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '123456789'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.CardNumber
        }}
      />
    );

    const divEle = queryByClass(container, 'form-group-static__text');
    expect(divEle?.innerHTML).toEqual('1-2345-6789');
  });

  it('should show Percent', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '10'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Percent
        }}
      />
    );

    const divEle = queryByClass(container, 'form-group-static__text');
    expect(divEle?.innerHTML).toEqual('10.00%');
  });

  it('should show PhoneNumber', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '1234567891'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.PhoneNumber
        }}
      />
    );

    const divEle = queryByClass(container, 'form-group-static__text');
    expect(divEle?.innerHTML).toEqual('(123) 456-7891');
  });

  it('should show MaskText', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '*****67891234567'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.MaskText
        }}
      />
    );

    const spanEle = queryByClass(container, 'hide-account-icon');
    expect(spanEle?.innerHTML).toEqual('·····');
  });

  it('GroupTextFormat.MaskText', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '23*****67891234567'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.MaskText
        }}
      />
    );

    const element = queryByClass(container, 'custom-account-number');

    expect(element?.innerHTML).toEqual(
      '23<span class="hide-account-icon">·····</span>67891234567'
    );
  });

  it('GroupTextFormat.MaskText', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '23'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.MaskText
        }}
      />
    );

    expect(screen.getByText('23')).toBeInTheDocument();
  });

  it('GroupTextFormat.MaskText', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '25*12312321313*****'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.MaskText
        }}
      />
    );
    const element = queryByClass(container, 'custom-account-number');
    expect(element?.innerHTML).toEqual(
      '25<span class="hide-account-icon">·</span>12312321313<span class="hide-account-icon">·····</span>'
    );
  });

  it('should show Mask', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '123****1234'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Mask
        }}
      />
    );
    const spanEle = queryByClass(container, 'custom-account-number');
    expect(spanEle?.innerHTML).toEqual(
      '123<span class="hide-account-icon">····</span>1234'
    );
  });

  it('should show FormatType - TransactionCode', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: 0
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.TransactionCode
          }}
        />
        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.TransactionCode
          }}
        />
      </>
    );

    expect(screen.getByText(`0 - ${TRANSACTION_CODE[0]}`)).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - PresentationInstrumentTypeCode', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: '01'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.PresentationInstrumentTypeCode
          }}
        />

        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.PresentationInstrumentTypeCode
          }}
        />
      </>
    );

    expect(
      screen.getByText(`01 - (01) ${PRESENTATION_INSTRUMENT_TYPE_CODE['01']}`)
    ).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - RiskIdentificationServiceIndicator', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: '1'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.RiskIdentificationServiceIndicator
          }}
        />
        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.RiskIdentificationServiceIndicator
          }}
        />
      </>
    );

    expect(
      screen.getByText(`1 - (1) ${RISK_IDENTIFICATION_SERVICE_INDICATOR['1']}`)
    ).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - AuthorizationSourceCode', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: 'A'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.AuthorizationSourceCode
          }}
        />

        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.AuthorizationSourceCode
          }}
        />
      </>
    );

    expect(
      screen.getByText(`A - (A) ${AUTHORIZATION_SOURCE_CODE['A']}`)
    ).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - SourceTransactionIdentifier', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: '004'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.SourceTransactionIdentifier
          }}
        />
        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.SourceTransactionIdentifier
          }}
        />
      </>
    );

    expect(
      screen.getByText(`004 - (004) ${SOURCE_TRANSACTION_IDENTIFIER['004']}`)
    ).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - NameMatchCode', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: 'N'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.NameMatchCode
          }}
        />
        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.NameMatchCode
          }}
        />
      </>
    );

    expect(screen.getByText(`N - ${NAME_MATCH_CODE['N']}`)).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - MailPhoneIndicator', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: '1'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.MailPhoneIndicator
          }}
        />

        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.MailPhoneIndicator
          }}
        />
      </>
    );

    expect(
      screen.getByText(`1 - (1) ${MAIL_PHONE_INDICATOR['1']}`)
    ).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - DualityFlag', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: 'M'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.DualityFlag
          }}
        />

        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.DualityFlag
          }}
        />
      </>
    );

    expect(
      screen.getByText(`M - (M) ${DUALITY_FLAG['M']}`)
    ).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - AutomatedTellerMachineAtmFlag', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: '0'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.AutomatedTellerMachineAtmFlag
          }}
        />

        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.AutomatedTellerMachineAtmFlag
          }}
        />
      </>
    );

    expect(
      screen.getByText(`0 - (0) ${AUTOMATED_TELLER_MACHINE_ATM_FLAG['0']}`)
    ).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - PointOfSaleEntryMode', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: '00'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.PointOfSaleEntryMode
          }}
        />

        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.PointOfSaleEntryMode
          }}
        />
      </>
    );

    expect(
      screen.getByText(`00 - (00) ${POINT_OF_SALE_ENTRY_MODE['00']}`)
    ).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - AuthorizationMethodCode', () => {
    const { container } = render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: 'C'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.AuthorizationMethodCode
          }}
        />
        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.AuthorizationMethodCode
          }}
        />
      </>
    );

    const spanEle = queryAllById(container, 'test__truncate-text')[0];
    const spanEle1 = queryAllById(container, 'test__truncate-text')[1];
    expect(spanEle?.innerHTML).toEqual(
      `C - (C) ${AUTHORIZATION_METHOD_CODE['C']}`
    );
    expect(spanEle1?.innerHTML).toEqual('abc');
  });

  it('should show FormatType - AuthorizationFilterCode', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: 'A'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.AuthorizationFilterCode
          }}
        />
        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.AuthorizationFilterCode
          }}
        />
      </>
    );

    expect(
      screen.getByText(`A - (A) ${AUTHORIZATION_FILTER_METHOD_CODE['A']}`)
    ).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - HeldStatementDestinationCode', () => {
    render(
      <>
        <GroupTextControl
          id="test"
          input={
            {
              value: 'C'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.HeldStatementDestinationCode
          }}
        />

        <GroupTextControl
          id="test"
          input={
            {
              value: 'abc'
            } as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            format: GroupTextFormat.FormatType,
            formatType: GroupTextFormatType.HeldStatementDestinationCode
          }}
        />
      </>
    );

    expect(
      screen.getByText(`C - ${HELD_STATEMENT_DESTINATION_CODE['C']}`)
    ).toBeInTheDocument();
    expect(screen.getByText('abc')).toBeInTheDocument();
  });

  it('should show FormatType - HeldStatementDestinationCode', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: 'C'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.FormatType,
          formatType: undefined
        }}
      />
    );

    const divEle = queryByClass(container, 'form-group-static__text');
    expect(divEle?.innerHTML).toEqual('');
  });

  it('should show Status no data', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: ''
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Status
        }}
      />
    );
    const spanEle = queryByClass(
      container,
      'badge badge-green text-transform-none'
    );
    expect(spanEle).toBeInTheDocument();
  });

  it('return empty > hiddenNoValue', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: ''
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          hiddenNoValue: true
        }}
      />
    );

    expect(container.innerHTML).toEqual('');
  });

  it('return empty > empty text', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: null
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{}}
      />
    );

    const divElement = queryByClass(container, 'form-group-static__text');
    expect(divElement?.innerHTML).toEqual('');
  });

  it('GroupTextFormat.Label', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value: 'value'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Label
        }}
      />
    );

    expect(screen.getByText('value')).toBeInTheDocument();
  });

  it('GroupTextFormat.FiveLinesAddress', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: 'value'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.FiveLinesAddress
        }}
      />
    );

    expect(
      container.querySelector('span[class="d-block form-group-static__text"]')
        ?.innerHTML
    ).toEqual('');
  });

  it('GroupTextFormat.FiveLinesAddress', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value: {
              addressLineOne: 'One',
              addressLineTwo: 'Two',
              addressLineThree: 'Three',
              addressLineFour: 'Four',
              addressLineFive: 'Five'
            }
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.FiveLinesAddress
        }}
      />
    );

    expect(screen.getByText('One')).toBeInTheDocument();
    expect(screen.getByText('Two')).toBeInTheDocument();
    expect(screen.getByText('Three')).toBeInTheDocument();
    expect(screen.getByText('Four')).toBeInTheDocument();
    expect(screen.getByText('Five')).toBeInTheDocument();
  });

  it('GroupTextFormat.Mask return empty dash', () => {
    const { container } = render(
      <GroupTextControl
        id="test"
        input={
          {
            value: ''
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Mask
        }}
      />
    );

    expect(
      container.querySelector('span[class="form-group-static__text"]')
    ).toBeInTheDocument();
  });

  it('GroupTextFormat.Api_Amount', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '1'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Api_Amount
        }}
      />
    );

    expect(screen.getByText('$1.00')).toBeInTheDocument();
  });

  it('GroupTextFormat.Api_Date', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '06/29/2021'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.Api_Date
        }}
      />
    );

    expect(screen.getByText('06/29/2021')).toBeInTheDocument();
  });

  it('GroupTextFormat.CustomDate', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value: '06-29-2021'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.CustomDate,
          inputDateFormat: 'MM-DD-YYYY'
        }}
      />
    );

    expect(screen.getByText('06/29/2021')).toBeInTheDocument();
  });

  it('GroupTextFormat.TruncateLessMore', () => {
    render(
      <GroupTextControl
        id="test"
        input={
          {
            value:
              'text very very very very very very very very very very very very very very very very very very long'
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          format: GroupTextFormat.TruncateLessMore
        }}
      />
    );

    expect(screen.getByText(I18N_COMMON_TEXT.MORE)).toBeInTheDocument();
  });
});
