import React, { useEffect, useState } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';

// utils
import { convertStringToNumberOnly } from 'app/helpers';
import isUndefined from 'lodash.isundefined';
import pickBy from 'lodash.pickby';

// components
import {
  conformToMask,
  MaskedTextBox,
  MaskedTextBoxProps
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface MaskedTextBoxControlProps
  extends CommonControlProps,
    Omit<MaskedTextBoxProps, 'id' | 'label' | 'data' | 'mask'> {
  mask?: Array<MaskPattern>;
  options: {
    isError?: boolean;
  };
}

const MaskedTextBoxControl: React.FC<MaskedTextBoxControlProps> = ({
  input: { name, value: valueProp, onChange, onBlur },
  meta: { error, valid, invalid, touched },
  id,
  dataTestId,
  placeholder,
  label,
  tooltip,
  readOnly,
  mask = [],
  required,
  options,
  enable,
  ...props
}) => {
  const { t } = useTranslation();
  // custom masked pattern
  const maskedPattern = mask.map(item => {
    if (!item.isRegex) return item.pattern || '';
    const pattern = item.pattern || '';
    try {
      // if the pattern is not regex, treat it as string
      const regexPattern = new RegExp(pattern);
      return regexPattern;
    } catch (e) {
      return pattern;
    }
  });

  const [value, setValue] = useState<string>(valueProp);

  const handleOnChange = (event: React.FormEvent<HTMLInputElement>) => {
    const { value } = event.target as MagicKeyValue;
    setValue(convertStringToNumberOnly(value));
  };

  const handleOnBlur = () => {
    onChange(value);
    onBlur(value);
  };

  const convertMaskedToRaw = (maskedValue: string) => {
    return conformToMask(maskedValue, maskedPattern).conformedValue;
  };

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  const errorTooltipProps = {
    opened: !options?.isError || invalid ? undefined : false
  };

  return (
    <MaskedTextBox
      id={id}
      dataTestId={dataTestId}
      name={name}
      label={t(label)}
      placeholder={t(placeholder)}
      readOnly={readOnly}
      mask={maskedPattern}
      required={enable && required}
      value={value ? convertMaskedToRaw(value) : undefined}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      error={
        (enable && touched && invalid) || options?.isError
          ? {
              message: t(error),
              status: invalid || options?.isError
            }
          : undefined
      }
      errorTooltipProps={pickBy(errorTooltipProps, item => !isUndefined(item))}
    />
  );
};

export default MaskedTextBoxControl;
