import React from 'react';

// components
import MaskedTextBoxControl from './index';

// RTL
import { act, fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

// helpers
import { queryByClass } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test MaskedTextBoxControl control', () => {
  it('render component', () => {
    render(
      <MaskedTextBoxControl
        id="id"
        options={{}}
        input={{ value: '123' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );

    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });

  it('render component > error', () => {
    jest.useFakeTimers();
    render(
      <MaskedTextBoxControl
        id="id"
        options={{}}
        input={{} as WrappedFieldInputProps}
        meta={
          {
            error: 'Error Message',
            invalid: true,
            touched: true
          } as WrappedFieldMetaProps
        }
        enable
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      userEvent.click(inputElement);
    });
    jest.runAllTimers();

    expect(screen.getByText('Error Message')).toBeInTheDocument();
  });

  it('render component > error', () => {
    jest.useFakeTimers();
    const { container } = render(
      <MaskedTextBoxControl
        id="id"
        options={{ isError: true }}
        input={{} as WrappedFieldInputProps}
        meta={
          {
            error: 'Error Message'
          } as WrappedFieldMetaProps
        }
        enable
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      userEvent.click(inputElement);
    });
    jest.runAllTimers();

    expect(queryByClass(container, /dls-error/)).toBeInTheDocument();
  });

  it('trigger handleOnBlur', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    jest.useFakeTimers();

    render(
      <MaskedTextBoxControl
        id="id"
        options={{}}
        input={
          {
            onChange: onChangeSpy,
            onBlur: onBlurSpy
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
      />
    );

    const inputElement = screen.getByRole('textbox');
    act(() => {
      fireEvent.blur(inputElement);
    });
    jest.runAllTimers();

    expect(onChangeSpy).toBeCalled();
    expect(onBlurSpy).toBeCalled();
  });

  it('render', () => {
    const mask = [
      {
        isRegex: true,
        pattern: '('
      },
      {
        isRegex: true,
        pattern: '\\d'
      },
      {
        isRegex: true,
        pattern: '\\d'
      },
      {
        isRegex: true,
        pattern: '\\d'
      },
      {
        isRegex: false,
        pattern: ')'
      },
      {
        isRegex: false,
        pattern: ' '
      },
      {
        isRegex: true,
        pattern: '\\d'
      },
      {
        isRegex: true,
        pattern: '\\d'
      },
      {
        isRegex: true,
        pattern: '\\d'
      },
      {
        isRegex: false,
        pattern: '-'
      },
      {
        isRegex: true,
        pattern: '\\d'
      },
      {
        isRegex: true,
        pattern: '\\d'
      },
      {
        isRegex: true,
        pattern: '\\d'
      },
      {
        isRegex: true,
        pattern: '\\d'
      },
      {
        isRegex: false,
        pattern: ''
      },
      {
        isRegex: true,
        pattern: ''
      },
      {
        isRegex: true,
        pattern: undefined
      }
    ];
    render(
      <MaskedTextBoxControl
        id="id"
        options={{}}
        input={{ value: '1234567890' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        mask={mask}
      />
    );

    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });
});
