import React from 'react';

// components
import DynamicGroupRadioControl from './index';

// types
import {
  FormState,
  WrappedFieldInputProps,
  WrappedFieldMetaProps
} from 'redux-form';

// helpers
import { renderMockStoreId } from 'app/test-utils';

// RTL
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

describe('DynamicGroupRadioControl component', () => {
  it('should show one radio input', () => {
    renderMockStoreId(
      <DynamicGroupRadioControl
        id="test"
        radios={[{ value: 'test', label: 'test' }]}
        input={{} as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );

    const allRadios = screen.getAllByRole('radio');
    expect(allRadios.length).toEqual(1);
  });

  it('should call onChange function', () => {
    const mockOnChange = jest.fn();

    renderMockStoreId(
      <DynamicGroupRadioControl
        id="test"
        radios={[
          { value: 'test', label: 'test' },
          { value: 'unit', label: 'unit' }
        ]}
        input={
          {
            value: 'test',
            onChange: mockOnChange
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        enable={true}
      />
    );

    const allRadios = screen.getAllByRole('radio');
    expect(allRadios.length).toEqual(2);

    userEvent.click(allRadios[1]);
    expect(mockOnChange).toBeCalled();
  });

  it('should show one radio input > layout col, default value', () => {
    const mockOnChange = jest.fn();
    renderMockStoreId(
      <DynamicGroupRadioControl
        id="test"
        radios={[{ value: 'test', label: 'test' }]}
        input={{ onChange: mockOnChange } as unknown as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        options={{
          layout: 'col'
        }}
        enable
      />
    );

    const radio = screen.getByRole('radio');
    userEvent.click(radio);

    expect(mockOnChange).toBeCalled();
  });

  it('should show one radio input > layout col, value', () => {
    const mockOnChange = jest.fn();
    renderMockStoreId(
      <DynamicGroupRadioControl
        id="test"
        radios={[{ value: 'test', label: 'test' }]}
        input={
          {
            value: 'value',
            onChange: mockOnChange
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          layout: 'col'
        }}
        enable
      />
    );

    const radio = screen.getByRole('radio');
    userEvent.click(radio);

    expect(mockOnChange).toBeCalled();
  });

  it('coverage useEffect with calling onChange', () => {
    const mockOnChange = jest.fn();
    renderMockStoreId(
      <DynamicGroupRadioControl
        id="test"
        radios={[{ value: 'test', label: 'test' }]}
        input={{ onChange: mockOnChange } as unknown as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        dependentValue="dependentValue"
      />
    );

    expect(mockOnChange).toBeCalledWith('');
  });

  it('coverage useEffect with calling onChange', () => {
    const mockOnChange = jest.fn();
    renderMockStoreId(
      <DynamicGroupRadioControl
        id="test"
        radios={[{ value: 'test', label: 'test' }]}
        input={
          {
            value: '',
            onChange: mockOnChange
          } as unknown as WrappedFieldInputProps
        }
        meta={{ form: 'dynamic' } as WrappedFieldMetaProps}
        dependentValue="dependentValue"
      />,
      {
        initialState: {
          form: {
            dynamic: {
              values: {
                dependentValue: 'test'
              }
            } as unknown as FormState
          }
        }
      }
    );

    expect(mockOnChange).toBeCalled();
  });
});
