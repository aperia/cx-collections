import React, { useEffect } from 'react';

// types
import { CommonControlProps } from 'app/_libraries/_dof/core';

// helpers
import classnames from 'classnames';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import { Radio } from 'app/_libraries/_dls/components';
import { useSelector } from 'react-redux';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface DynamicGroupRadioControlProps extends CommonControlProps {
  radios: { value: string; label: string }[];
  options?: {
    classNames?: string;
    layout?: string;
    valueDefault?: string;
  };
  dependentValue?: string;
  setValue?: string;
  dataTestId?: string;
}

const DynamicGroupRadioControl: React.FC<DynamicGroupRadioControlProps> = ({
  input: { name, value: valueProp, onChange },
  meta: { error, valid, invalid, touched, form },
  options = {},
  id,
  dataTestId,
  label,
  tooltip,
  readOnly,
  required,
  enable,
  radios,
  dependentValue = '',
  setValue,
  ...props
}) => {
  const { t } = useTranslation();

  const { classNames, layout, valueDefault } = options;

  const formData = useSelector<RootState, MagicKeyValue>(
    state => state?.form[form]
  );

  const dependentValueField = isEmpty(
    get(formData?.values, dependentValue, '')
  );

  const err = isEmpty(get(formData?.syncErrors, dependentValue, ''));

  const dependentDisable = isEmpty(dependentValue)
    ? !enable
    : dependentValueField || !err;

  useEffect(() => {
    if (!isEmpty(dependentValue) && dependentDisable) {
      onChange('');
    }
  }, [dependentDisable, dependentValue, onChange]);

  useEffect(() => {
    if (!isEmpty(dependentValue) && !dependentDisable && isEmpty(valueProp)) {
      onChange(setValue);
    }
  }, [dependentDisable, dependentValue, onChange, setValue, valueProp]);

  if (layout === 'col') {
    const valueRadio = isEmpty(valueProp) ? valueDefault : valueProp;
    return (
      <div id={id} className={classnames('w-100', classNames)}>
        <span
          className="fw-500 color-grey mb-16 d-block"
          data-testid={genAmtId(
            dataTestId!,
            'label',
            'DynamicGroupRadioControl'
          )}
        >
          {t(label)}:
        </span>
        <div className="d-flex">
          {radios.map(radio => (
            <Radio
              key={`${radio?.value}-${name}`}
              className="d-inline-block mr-24"
              dataTestId={genAmtId(
                dataTestId!,
                name,
                'DynamicGroupRadioControl'
              )}
            >
              <Radio.Input
                readOnly={readOnly}
                disabled={dependentDisable}
                id={`${radio?.value}-${name}`}
                checked={valueRadio === radio?.value}
                onChange={() => onChange(radio?.value)}
              />
              <Radio.Label>{t(radio?.label)}</Radio.Label>
            </Radio>
          ))}
        </div>
      </div>
    );
  }

  return (
    <div id={id} className={classnames('d-flex w-100', classNames)}>
      <span
        className="fw-500 color-grey w-20"
        data-testid={genAmtId(dataTestId!, 'label', 'DynamicGroupRadioControl')}
      >
        {t(label)}:
      </span>
      <div className="w-70">
        {radios.map(radio => (
          <Radio
            key={`${radio?.value}-${name}`}
            className="d-inline-block mr-24"
            dataTestId={genAmtId(dataTestId!, name, 'DynamicGroupRadioControl')}
          >
            <Radio.Input
              readOnly={readOnly}
              disabled={dependentDisable}
              id={`${radio?.value}-${name}`}
              checked={valueProp === radio?.value}
              onChange={() => onChange(radio?.value)}
            />
            <Radio.Label>{t(radio?.label)}</Radio.Label>
          </Radio>
        ))}
      </div>
    </div>
  );
};

export default DynamicGroupRadioControl;
