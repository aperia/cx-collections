import { INVALID_DATE } from 'app/constants';
// Const
import { FormatTime } from 'app/constants/enums';
import { checkPermission } from 'app/entitlements';
// Helper
import {
  addDays,
  addMonths,
  formatTimeDefault,
  isArrayHasValue,
  isValidDate
} from 'app/helpers';
import { useAccountDetail } from 'app/hooks';
// components
import {
  DatePicker,
  DatePickerChangeEvent,
  DatePickerProps,
  Tooltip
} from 'app/_libraries/_dls/components';
// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import DateTime from 'date-and-time';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import isUndefined from 'lodash.isundefined';
import React from 'react';
import { useSelector } from 'react-redux';

interface DisableRange {
  fromDate: Date;
  toDate: Date;
}
export interface DatePickerControlProps
  extends CommonControlProps,
    DOFEntitlement,
    Omit<DatePickerProps, 'id' | 'label'> {
  options?: {
    monthMin?: number;
    monthMax?: number;
    isMinToday?: boolean;
    isMaxToday: boolean;
    numberOverToday?: number;
    maxFromToday?: number;
    maxDateWithDays?: {
      date?: Date;
      numberOfMonth?: number;
    };
    isError?: boolean;
    min?: Date;
    minDateDisabledText?: string;
    maxDateDisabledText?: string;
    disableDateRangeText?: string;
    disableRange?: DisableRange[];
    inputFormatDate?: string;
    outputFormatDate?: string;
    dependentDay?: string;
    countDay?: number;
    typeHardship?: string;
    dependentValues?: string[];
    fieldDisplay?: {
      isDefault: boolean;
      value: string;
      field: string;
    };
  };
}

const DatePickerControl: React.FC<DatePickerControlProps> = ({
  input: { name, value: valueProp, onChange, onBlur },
  meta: { error, valid, invalid, touched, form },
  id,
  dataTestId,
  label,
  enable,
  readOnly,
  options,
  required,
  entitlementConfig,
  ...props
}) => {
  const {
    inputFormatDate,
    dependentDay = '',
    countDay,
    dependentValues = [],
    typeHardship,
    monthMin = 3,
    monthMax = 12,
    fieldDisplay
  } = options || {};
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();

  const formData = useSelector<RootState, MagicKeyValue>(
    state => state?.form[form]
  );

  // is disable hardship
  let disabled = false;
  if (!isEmpty(dependentValues)) {
    dependentValues.forEach(item => {
      const data = get(formData?.values, item, '');
      const err = get(formData?.syncErrors, item, '');
      disabled = isEmpty(`${data}`) || !isEmpty(err);
      if (disabled) return;
    });
  }

  const formatDefaultDate = (value: string) => {
    if (inputFormatDate) {
      try {
        // Bankruptcy return data '00000000'
        if (INVALID_DATE.includes(value)) return undefined;
        const parsedDate = DateTime.parse(value, inputFormatDate);
        if (isValidDate(parsedDate)) return parsedDate;
      } catch (error) {
        return new Date(value);
      }
    }

    if (isValidDate(value)) return new Date(value);

    return undefined;
  };

  const renderMinToDay = () => {
    if (options?.min) {
      const result = new Date(options?.min);
      result.setHours(0, 0, 0, 0);
      return result;
    }

    const date = new Date();
    if (options?.numberOverToday) {
      const result = new Date(
        date.setDate(date.getDate() + options.numberOverToday)
      );
      result.setHours(0, 0, 0, 0);
      return result;
    }
    if (options?.isMinToday) {
      date.setHours(0, 0, 0, 0);
      return date;
    }
    if (!isEmpty(dependentDay)) {
      const data = get(formData?.values, dependentDay);

      if (isEmpty(`${data}`)) return undefined;

      if (countDay) {
        const minDateWithCount = addDays(data, countDay);
        const result = new Date(minDateWithCount);
        result.setHours(0, 0, 0, 0);
        return result;
      }
      const minDate = addMonths(data, monthMin);

      const result = new Date(minDate);
      result.setHours(0, 0, 0, 0);
      return result;
    }
    if (typeHardship === 'rank' && !isEmpty(dependentValues)) {
      const minDate = get(formData?.values, dependentValues[0], new Date());
      const result = new Date(`${minDate}`);
      result.setHours(0, 0, 0, 0);
      return result;
    }
    return undefined;
  };

  const renderMaxToday = () => {
    let date = new Date();
    if (options?.isMaxToday) {
      const result = new Date();
      result.setHours(0, 0, 0, 0);
      return result;
    }

    if (options?.maxFromToday) {
      const result = new Date(
        date.setDate(date.getDate() + options.maxFromToday)
      );
      result.setHours(0, 0, 0, 0);
      return result;
    }

    if (
      options?.maxDateWithDays?.date &&
      options?.maxDateWithDays?.numberOfMonth
    ) {
      date = new Date(options?.maxDateWithDays?.date);

      const result = new Date(
        date.setMonth(date.getMonth() + options.maxDateWithDays.numberOfMonth)
      );
      result.setHours(0, 0, 0, 0);
      return result;
    }

    if (!isEmpty(dependentDay)) {
      if (monthMax === 0) return undefined;
      const data = get(formData?.values, dependentDay, '');
      if (isEmpty(`${data}`)) return undefined;
      const minDate = addMonths(data, monthMax);

      const result = new Date(minDate);
      result.setHours(0, 0, 0, 0);
      return result;
    }

    if (typeHardship === 'rank' && !isEmpty(dependentValues)) {
      const maxDate = get(formData?.values, dependentValues[1], new Date());

      const result = new Date(`${maxDate}`);
      result.setHours(0, 0, 0, 0);
      return result;
    }

    return undefined;
  };

  const handleOnChange = (event: DatePickerChangeEvent) => {
    const currentValue = event.target.value;
    onChange(isUndefined(currentValue) ? null : currentValue);
  };

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    if (disabled || readOnly) return;
    const currentValue = (event.target as MagicKeyValue).value;
    onBlur(isUndefined(currentValue) ? null : currentValue);
  };

  const handleFormatDate = (date: Date, type: FormatTime) => {
    return formatTimeDefault(date.toString(), type);
  };

  const handleDisableRange = (date: Date) => {
    const { YearMonthDay } = FormatTime;
    const formatDate = handleFormatDate(date, YearMonthDay)!;
    if (!options?.disableRange || !isArrayHasValue(options?.disableRange))
      return false;
    return options?.disableRange.some(item => {
      const { fromDate, toDate } = item;
      return (
        handleFormatDate(fromDate, YearMonthDay)! <= formatDate &&
        handleFormatDate(toDate, YearMonthDay)! >= formatDate
      );
    });
  };

  const handleDateTooltip = (date: Date) => {
    const minDate = renderMinToDay();
    const maxDate = renderMaxToday();
    const disabledRange = handleDisableRange(date);
    if (disabledRange && options?.disableDateRangeText) {
      return (
        <Tooltip
          element={t(options?.disableDateRangeText)}
          dataTestId={`${dataTestId}-disabled-date-range`}
        />
      );
    }
    if (minDate && date < minDate && options?.minDateDisabledText) {
      return (
        <Tooltip
          element={t(options?.minDateDisabledText)}
          dataTestId={`${dataTestId}-min-date-disabled`}
        />
      );
    }
    if (maxDate && date > maxDate && options?.maxDateDisabledText) {
      return (
        <Tooltip
          element={t(options?.maxDateDisabledText)}
          dataTestId={`${dataTestId}-max-date-disabled`}
        />
      );
    }
    return undefined;
  };

  // fieldDisplay for case Hardship
  if (fieldDisplay) {
    const dependValue = get(formData?.values, fieldDisplay.field);
    if (fieldDisplay.isDefault) {
      if (dependValue !== fieldDisplay.value && !isEmpty(dependValue))
        return null;
    } else {
      if (dependValue !== fieldDisplay.value) return null;
    }
  }

  if (
    entitlementConfig?.entitlementCode &&
    !checkPermission(entitlementConfig.entitlementCode, storeId)
  ) {
    return null;
  }

  return (
    <DatePicker
      id={id}
      dataTestId={dataTestId}
      name={name}
      value={formatDefaultDate(valueProp)}
      minDate={renderMinToDay()}
      maxDate={renderMaxToday()}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      label={t(label)}
      disabled={!enable || disabled}
      readOnly={readOnly}
      required={enable && !readOnly && required && !disabled}
      error={
        (!disabled && required && touched && invalid) || options?.isError
          ? {
              message: t(error),
              status: invalid || options?.isError
            }
          : undefined
      }
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
      disabledRange={options?.disableRange}
      dateTooltip={handleDateTooltip}
      {...props}
    />
  );
};

export default DatePickerControl;
