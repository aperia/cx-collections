import React from 'react';

// components
import DatePickerControl from './index';

// RTL
import { act, fireEvent, render, screen } from '@testing-library/react';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

// helpers
import {
  queryAllByClass,
  queryByClass,
  wrapperComponent
} from 'app/test-utils';
import userEvent from '@testing-library/user-event';

import * as entitlementsHelpers from 'app/entitlements/helpers';
import * as roleHelper from 'app/entitlements/helpers';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test DatePickerControl component', () => {
  beforeEach(() => {
    jest.spyOn(roleHelper, 'checkPermission').mockReturnValue(true);
  });

  it('render component', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{ value: '' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );
    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > error', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <div>
        <DatePickerControl
          id="id"
          input={{} as WrappedFieldInputProps}
          meta={
            {
              error: 'Error Message',
              touched: true,
              invalid: true
            } as WrappedFieldMetaProps
          }
          enable
          required
        />
      </div>
    );

    const { container } = render(component);
    jest.runAllTimers();

    const inputElement = screen.getByRole('textbox');
    act(() => {
      fireEvent.focus(inputElement);
      jest.runAllTimers();
    });

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > error', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <div>
        <DatePickerControl
          id="id"
          input={{} as WrappedFieldInputProps}
          meta={
            {
              error: 'Error Message'
            } as WrappedFieldMetaProps
          }
          options={{
            isError: true,
            isMaxToday: true
          }}
          enable
          required
        />
      </div>
    );

    const { container } = render(component);
    jest.runAllTimers();

    const inputElement = screen.getByRole('textbox');
    act(() => {
      fireEvent.focus(inputElement);
      jest.runAllTimers();
    });

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > maxFromToday', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{ value: '' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          maxFromToday: 1
        }}
        enable
        required
      />
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > maxDateWithDays', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{ value: '' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          maxDateWithDays: {
            date: new Date(),
            numberOfMonth: 1
          }
        }}
        enable
        required
      />
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > dependentDay', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          dependentDay: 'startDate',
          monthMax: 9
        }}
        enable
        required
      />,
      {
        form: {
          test: {
            values: {
              startDate: new Date()
            }
          }
        }
      }
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > dependentDay', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          dependentDay: 'startDate',
          monthMax: 0
        }}
        enable
        required
      />,
      {
        form: {
          test: {
            values: {
              startDate: new Date()
            }
          }
        }
      }
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > typeHardship', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          typeHardship: 'rank',
          dependentValues: ['startDate']
        }}
        enable
        required
      />,
      {
        form: {
          test: {
            values: {
              startDate: new Date()
            }
          }
        }
      }
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > disabled', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          typeHardship: 'rank',
          dependentValues: ['startDate']
        }}
        enable
        required
      />,
      {
        form: {
          test: {
            values: {
              startDate: ''
            }
          }
        }
      }
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(
      queryByClass(container, /dls-date-picker dls-disabled/)
    ).toBeInTheDocument();
  });

  it('render component > renderMinToDay - options?.min', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          min: new Date()
        }}
        enable
        required
      />
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > renderMinToDay - options?.numberOverToday', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          numberOverToday: 1
        }}
        enable
        required
      />
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > renderMinToDay - options?.isMinToday', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          isMinToday: true
        }}
        enable
        required
      />
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > renderMinToDay - dependentDay', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          dependentDay: 'startDate'
        }}
        enable
        required
      />,
      {
        form: {
          test: {
            values: {
              startDate: ''
            }
          }
        }
      }
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > renderMinToDay - dependentDay', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          countDay: 1,
          dependentDay: 'startDate'
        }}
        enable
        required
      />,
      {
        form: {
          test: {
            values: {
              startDate: new Date()
            }
          }
        }
      }
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > formatDefaultDate', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{ value: '00000000' } as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: true,
          inputFormatDate: 'MMDDYYYY'
        }}
        enable
        required
      />
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > formatDefaultDate', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{ value: '06292021' } as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: true,
          inputFormatDate: 'MMDDYYYY'
        }}
        enable
        required
      />
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > formatDefaultDate', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{ value: new Date('06/29/2021') } as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: true,
          inputFormatDate: 'MMDDYYYY'
        }}
        enable
        required
      />
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > formatDefaultDate', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{ value: '06/29/2021' } as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: true,
          inputFormatDate: 'MMDDYYYY'
        }}
        enable
        required
      />
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('render component > formatDefaultDate', () => {
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{ value: new Date('06/29/2021') } as WrappedFieldInputProps}
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: true
        }}
        enable
        required
      />
    );

    const { container } = render(component);
    jest.runAllTimers();

    expect(queryByClass(container, /dls-date-picker/)).toBeInTheDocument();
  });

  it('handleOnChange', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    jest.useFakeTimers();
    const component = wrapperComponent(
      <div>
        <DatePickerControl
          id="id"
          input={
            {
              value: new Date(),
              onChange: onChangeSpy,
              onBlur: onBlurSpy
            } as unknown as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          enable
          required
        />
      </div>
    );

    const { baseElement } = render(component);

    const input = screen.getByRole('textbox');
    act(() => {
      userEvent.click(input);
    });
    jest.runOnlyPendingTimers();

    const numbers = queryAllByClass(
      baseElement as HTMLElement,
      /react-calendar__tile react-calendar__month-view__days__day/
    );

    act(() => {
      userEvent.click(numbers[5]);
    });
    jest.runAllTimers();
    expect(onChangeSpy).toBeCalled();
    expect(onBlurSpy).toBeCalled();
  });

  it('handleOnChange > readOnly', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    jest.useFakeTimers();
    const component = wrapperComponent(
      <div>
        <DatePickerControl
          id="id"
          input={
            {
              value: new Date(),
              onChange: onChangeSpy,
              onBlur: onBlurSpy
            } as unknown as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          options={{
            isMaxToday: true,
            isMinToday: true
          }}
          enable
          required
          readOnly
        />
      </div>
    );

    render(component);

    const input = screen.getByRole('textbox');
    act(() => {
      userEvent.click(input);
    });
    jest.runOnlyPendingTimers();

    act(() => {
      fireEvent.blur(input);
    });
    jest.runAllTimers();
    expect(onChangeSpy).not.toBeCalled();
    expect(onBlurSpy).not.toBeCalled();
  });
});

describe('Test DatePickerControl component > no permission', () => {
  beforeEach(() => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
  });

  it('render component > no permission', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(false);
    jest.useFakeTimers();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={{ value: '' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        entitlementConfig={{ entitlementCode: 'edit' }}
      />
    );
    render(component);
    jest.runAllTimers();
  });
});
