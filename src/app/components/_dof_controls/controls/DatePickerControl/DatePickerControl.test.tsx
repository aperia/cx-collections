import React from 'react';

// components
import DatePickerControl from './index';

// RTL
import { fireEvent, render, screen } from '@testing-library/react';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

// helpers
import { wrapperComponent } from 'app/test-utils';
import {
  DatePickerChangeEvent,
  DatePickerProps
} from 'app/_libraries/_dls/components';
import * as roleHelper from 'app/entitlements/helpers';

jest.mock('app/_libraries/_dls/components', () => {
  return {
    ...jest.requireActual('app/_libraries/_dls/components'),
    DatePicker: (props: DatePickerProps) => {
      const today = new Date();
      const date = new Date();
      date.setDate(today.getDate() - 1);
      return (
        <>
          <div onClick={() => props.dateTooltip!(date)}>Date Tooltip</div>
          <div
            onClick={() =>
              props.onChange!({ target: {} } as DatePickerChangeEvent)
            }
          >
            onChange
          </div>
          <div
            onClick={() =>
              props.onBlur!({ target: {} } as React.FocusEvent<Element>)
            }
          >
            onBlur
          </div>
        </>
      );
    }
  };
});

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test DatePickerControl', () => {
  beforeEach(() => {
    jest.spyOn(roleHelper, 'checkPermission').mockReturnValue(true);
  });
  
  it('call onChange, onBlur with null value', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();

    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={
          {
            value: '',
            onChange: onChangeSpy,
            onBlur: onBlurSpy
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
      />
    );

    render(component);

    fireEvent.click(screen.getByText('Date Tooltip'));
    fireEvent.click(screen.getByText('onChange'));
    fireEvent.click(screen.getByText('onBlur'));

    expect(onChangeSpy).toBeCalled();
    expect(onBlurSpy).toBeCalled();
  });

  it('disableRange, disableDateRangeText', () => {
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={
          {
            value: ''
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          isMaxToday: true,
          disableRange: [
            {
              fromDate: new Date('01/01/2021'),
              toDate: new Date()
            }
          ],
          disableDateRangeText: 'disableDateRangeText'
        }}
      />
    );

    render(component);

    fireEvent.click(screen.getByText('Date Tooltip'));

    expect(screen.getByText('Date Tooltip')).toBeInTheDocument();
  });

  it('minDateDisabledText', () => {
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={
          {
            value: ''
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          isMaxToday: true,
          isMinToday: true,
          minDateDisabledText: 'minDateDisabledText'
        }}
      />
    );

    render(component);

    fireEvent.click(screen.getByText('Date Tooltip'));

    expect(screen.getByText('Date Tooltip')).toBeInTheDocument();
  });

  it('maxDateDisabledText', () => {
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={
          {
            value: ''
          } as unknown as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
        options={{
          isMaxToday: false,
          maxFromToday: -5,
          isMinToday: true,
          maxDateDisabledText: 'maxDateDisabledText'
        }}
      />
    );

    render(component);

    fireEvent.click(screen.getByText('Date Tooltip'));

    expect(screen.getByText('Date Tooltip')).toBeInTheDocument();
  });

  it('isDefault = false, dependValue !== fieldDisplay.value', () => {
    const date = new Date();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={
          {
            value: ''
          } as unknown as WrappedFieldInputProps
        }
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: true,
          fieldDisplay: {
            isDefault: false,
            value: '',
            field: 'startDate'
          }
        }}
      />,
      {
        form: {
          test: {
            values: {
              startDate: date
            }
          }
        }
      }
    );

    const { container } = render(component);

    expect(container.innerHTML).toEqual('');
  });

  it('isDefault = false, dependValue === fieldDisplay.value', () => {
    const date = new Date();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={
          {
            value: ''
          } as unknown as WrappedFieldInputProps
        }
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: true,
          fieldDisplay: {
            isDefault: false,
            value: date.toString(),
            field: 'startDate'
          }
        }}
      />,
      {
        form: {
          test: {
            values: {
              startDate: date.toString()
            }
          }
        }
      }
    );

    render(component);

    expect(screen.getByText('Date Tooltip')).toBeInTheDocument();
  });

  it('isDefault = true, dependValue === fieldDisplay.value', () => {
    const date = new Date();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={
          {
            value: ''
          } as unknown as WrappedFieldInputProps
        }
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: true,
          fieldDisplay: {
            isDefault: true,
            value: date.toString(),
            field: 'startDate'
          }
        }}
      />,
      {
        form: {
          test: {
            values: {
              startDate: date.toString()
            }
          }
        }
      }
    );

    render(component);

    expect(screen.getByText('Date Tooltip')).toBeInTheDocument();
  });

  it('isDefault = true, dependValue !== fieldDisplay.value', () => {
    const date = new Date();
    const component = wrapperComponent(
      <DatePickerControl
        id="id"
        input={
          {
            value: ''
          } as unknown as WrappedFieldInputProps
        }
        meta={{ form: 'test' } as WrappedFieldMetaProps}
        options={{
          isMaxToday: true,
          fieldDisplay: {
            isDefault: true,
            value: 'a',
            field: 'startDate'
          }
        }}
      />,
      {
        form: {
          test: {
            values: {
              startDate: date.toString()
            }
          }
        }
      }
    );

    const { container } = render(component);

    expect(container.innerHTML).toEqual('');
  });
});
