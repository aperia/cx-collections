import React from 'react';

// components
import DateRangePickerControl from './index';

// RTL
import { act, render, screen } from '@testing-library/react';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

// helpers
import { queryAllByClass, queryByClass } from 'app/test-utils';
import userEvent from '@testing-library/user-event';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test DateRangePickerControl control', () => {
  it('render component', () => {
    jest.useFakeTimers();
    const { container } = render(
      <DateRangePickerControl
        id="id"
        input={{ value: '' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );
    jest.runAllTimers();

    expect(
      queryByClass(container, /dls-date-range-picker/)
    ).toBeInTheDocument();
  });

  it('render error', () => {
    jest.useFakeTimers();
    const { container } = render(
      <div>
        <DateRangePickerControl
          id="id"
          input={{ value: '' } as WrappedFieldInputProps}
          meta={
            {
              touched: true,
              invalid: true,
              error: 'Error Message'
            } as WrappedFieldMetaProps
          }
          required
          enable
        />
      </div>
    );
    jest.runAllTimers();

    const elements = queryAllByClass(container, 'text-field-container');

    act(() => {
      userEvent.click(elements[0]);
      jest.runAllTimers();
    });

    expect(screen.getByText('Error Message')).toBeInTheDocument();
  });

  it('render error from options argument', () => {
    jest.useFakeTimers();
    const { container } = render(
      <div>
        <DateRangePickerControl
          id="id"
          input={{ value: '' } as WrappedFieldInputProps}
          meta={
            {
              touched: false,
              invalid: false,
              error: ''
            } as WrappedFieldMetaProps
          }
          options={{ messageForceValid: 'Error options', isForceInvalid: true }}
          required
          enable
        />
      </div>
    );
    jest.runAllTimers();

    const elements = queryAllByClass(container, 'text-field-container');

    act(() => {
      userEvent.click(elements[0]);
      jest.runAllTimers();
    });

    expect(screen.getByText('Error options')).toBeInTheDocument();
  });

  it('render component > isMaxToday', () => {
    jest.useFakeTimers();
    const { container } = render(
      <DateRangePickerControl
        id="id"
        input={{ value: '' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        options={{ isMaxToday: true }}
      />
    );
    jest.runAllTimers();

    expect(
      queryByClass(container, /dls-date-range-picker/)
    ).toBeInTheDocument();
  });

  it('run onChange', () => {
    const onChangeSpy = jest.fn();
    jest.useFakeTimers();
    const { container, baseElement } = render(
      <div>
        <DateRangePickerControl
          id="id"
          input={
            {
              value: '',
              onChange: onChangeSpy
            } as unknown as WrappedFieldInputProps
          }
          meta={{} as WrappedFieldMetaProps}
          enable
        />
      </div>
    );
    jest.runAllTimers();

    const elements = queryAllByClass(container, 'text-field-container');

    act(() => {
      userEvent.click(elements[0]);
      jest.runAllTimers();
    });

    const numbers = queryAllByClass(
      baseElement as HTMLElement,
      /react-calendar__tile react-calendar__month-view__days__day/
    );
    act(() => {
      userEvent.click(numbers[5]);
      userEvent.click(numbers[6]);
      jest.runAllTimers();
    });

    expect(onChangeSpy).toBeCalled();
  });
});
