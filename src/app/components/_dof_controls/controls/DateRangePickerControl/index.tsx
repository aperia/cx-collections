import React from 'react';

// types
import { CommonControlProps } from 'app/_libraries/_dof/core';

// components
import {
  DateRangePicker,
  DateRangePickerChangeEvent
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface DateRangePickerControlProps extends CommonControlProps {
  dataTestId?: string;

  options?: {
    isMaxToday?: boolean;
    isForceInvalid?: boolean;
    messageForceValid?: string;
  };
}

const DateRangePickerControl: React.FC<DateRangePickerControlProps> = ({
  input: { name, value: valueProp, onChange, onBlur },
  meta: { error, valid, invalid, touched },
  id,
  dataTestId,
  label,
  tooltip,
  readOnly,
  required,
  enable,
  options,
  ...props
}) => {
  const { t } = useTranslation();
  const handleOnChange = (event: DateRangePickerChangeEvent) => {
    const { value } = event.target;
    onChange(value);
  };

  const renderMaxDate = () => {
    if (options?.isMaxToday) {
      return new Date();
    }
    return undefined;
  };

  return (
    <DateRangePicker
      name={name}
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
      label={t(label)}
      value={valueProp}
      onChange={handleOnChange}
      disabled={!enable}
      readOnly={readOnly}
      required={required}
      error={
        (enable && required && touched && invalid) || options?.isForceInvalid
          ? {
              message: t(error) || t(options?.messageForceValid),
              status: invalid || options?.isForceInvalid
            }
          : undefined
      }
      maxDate={renderMaxDate()}
      dataTestId={genAmtId(dataTestId!, 'date-range-picker-control', 'DateRangePickerControl')}
      {...props}
    />
  );
};

export default DateRangePickerControl;
