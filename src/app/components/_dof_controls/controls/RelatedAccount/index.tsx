import React from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { TruncateText } from 'app/_libraries/_dls/components';
import classNames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';

// Hook
import { useAccountDetail } from 'app/hooks';

// Redux
import { relatedAccountActions } from 'pages/AccountDetails/RelatedAccount/_redux/reducer';

// Helper
import { checkPermission } from 'app/entitlements';

export interface RelatedAccountControlProps
  extends CommonControlProps,
    DOFEntitlement {
  dataTestId?: string;
}

export const RelatedAccount: React.FC<RelatedAccountControlProps> = ({
  input: { value },
  meta: {},
  id,
  label,
  enable,
  required,
  readOnly,
  dataTestId,
  entitlementConfig,
  ...props
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();

  const handleOnClick = () => {
    if (!enable) return;
    dispatch(
      relatedAccountActions.triggerOpenRelatedAccount({
        translateFn: t
      })
    );
  };

  if (
    entitlementConfig?.entitlementCode &&
    !checkPermission(entitlementConfig.entitlementCode, storeId)
  ) {
    return null;
  }

  return (
    <div
      className={'form-group-static'}
      id={id}
      data-testid={genAmtId(dataTestId!, 'related-account', 'RelatedAccount')}
    >
      <span
        id={`${id}__label`}
        className="form-group-static__label"
        data-testid={genAmtId(
          dataTestId!,
          'related-account_label',
          'RelatedAccount'
        )}
      >
        <TruncateText
          id={`${id}__label_text`}
          title={t(label)}
          dataTestId={dataTestId}
        >
          {t(label)}
        </TruncateText>
      </span>
      <div
        id={`${id}__text`}
        className={classNames('form-group-static__text')}
        data-testid={genAmtId(
          dataTestId!,
          'related-account_text',
          'RelatedAccount'
        )}
      >
        <span
          className={classNames('d-inline-block', {
            'link text-decoration-none': enable
          })}
          onClick={handleOnClick}
        >
          {value}
        </span>
      </div>
    </div>
  );
};

export default RelatedAccount;
