import React from 'react';

// components
import RelatedAccount from './index';

// RTL
import { fireEvent, render, screen } from '@testing-library/react';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

// helpers
import {
  mockActionCreator,
  queryByClass,
  wrapperComponent
} from 'app/test-utils';

// actions
import { relatedAccountActions } from 'pages/AccountDetails/RelatedAccount/_redux/reducer';

import { contactEntitlement } from 'app/entitlements';
import * as entitlementsHelpers from 'app/entitlements/helpers';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const relatedAccountActionsSpy = mockActionCreator(relatedAccountActions);

beforeEach(() => {
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});

describe('Test RelatedAccount control', () => {
  it('render component without commission', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(false);

    const component = wrapperComponent(
      <RelatedAccount
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        entitlementConfig={{ entitlementCode: 'AAA' }}
      />
    );
    const { container } = render(component);

    expect(
      queryByClass(container, 'dls-truncate-text')
    ).not.toBeInTheDocument();
  });

  it('render component', () => {
    const component = wrapperComponent(
      <RelatedAccount
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );
    const { container } = render(component);

    expect(queryByClass(container, 'dls-truncate-text')).toBeInTheDocument();
  });

  it('clicks on text > not call', () => {
    const triggerOpenRelatedAccount = relatedAccountActionsSpy(
      'triggerOpenRelatedAccount'
    );
    const component = wrapperComponent(
      <RelatedAccount
        id="id"
        enable={false}
        input={{ value: 'value' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );
    render(component);

    const element = screen.getByText('value');
    fireEvent.click(element!);

    expect(triggerOpenRelatedAccount).not.toBeCalled();
  });

  it('clicks on text', () => {
    const triggerOpenRelatedAccount = relatedAccountActionsSpy(
      'triggerOpenRelatedAccount'
    );
    const component = wrapperComponent(
      <RelatedAccount
        id="id"
        enable={true}
        input={{ value: 'value' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );
    render(component);

    const element = screen.getByText('value');
    fireEvent.click(element!);

    expect(triggerOpenRelatedAccount).toBeCalled();
  });

  it('return null when entitlementConfig is empty', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(false);
    const component = wrapperComponent(
      <RelatedAccount
        id="id"
        enable={true}
        input={{ value: 'value' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        entitlementConfig={{ entitlementCode: 'AAA' }}
      />
    );
    const { container } = render(component);

    expect(queryByClass(container, 'dls-truncate-text')).toBeNull();
  });

  it('checkPermission not valid', () => {
    jest.spyOn(contactEntitlement, 'takeEntitlement').mockReturnValue({
      registerAll: jest.fn(),
      takeEntitlement: false
    } as never);
    const component = wrapperComponent(
      <RelatedAccount
        id="id"
        enable={true}
        input={{ value: 'value' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        entitlementConfig={{ entitlementCode: 'AAA' } as never}
      />
    );
    render(component);
  });
});
