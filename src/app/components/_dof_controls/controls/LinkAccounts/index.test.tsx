import React from 'react';

// RTL
import userEvent from '@testing-library/user-event';

// helpers
import {
  mockActionCreator,
  queryByClass,
  renderMockStoreId
} from 'app/test-utils';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

// components
import LinkAccounts from './index';

// actions
import { tabActions } from 'pages/__commons/TabBar/_redux';

const tabActionsSpy = mockActionCreator(tabActions);

describe('Test LinkAccounts control', () => {
  it('should show AddTab', () => {
    const { wrapper } = renderMockStoreId(
      <LinkAccounts
        id="test"
        input={
          {
            value: {
              accMaskedValue: '*****123',
              accEValue: 'accEValue',
              primaryName: 'test'
            }
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
      />
    );

    const element = queryByClass(wrapper.container, 'custom-account-number');

    expect(element?.innerHTML).toEqual(
      '<span class="hide-account-icon">·····</span>123'
    );
  });

  it('click on link', () => {
    const addTab = tabActionsSpy('addTab');
    const { wrapper } = renderMockStoreId(
      <LinkAccounts
        id="test"
        input={
          {
            value: {
              accMaskedValue: '*****123',
              accEValue: 'accEValue',
              primaryName: 'test'
            }
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
      />
    );

    const element = queryByClass(wrapper.container, 'data-value');
    userEvent.click(element!);

    expect(addTab).toBeCalled();
  });

  it('click on link > not having accEValue', () => {
    const addTab = tabActionsSpy('addTab');
    const { wrapper } = renderMockStoreId(
      <LinkAccounts
        id="test"
        input={
          {
            value: {
              accMaskedValue: '*****123',
              primaryName: 'test'
            }
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
      />
    );

    const element = queryByClass(wrapper.container, 'data-value');
    userEvent.click(element!);

    expect(addTab).not.toBeCalled();
  });

  it('empty accMaskedValue', () => {
    const { wrapper } = renderMockStoreId(
      <LinkAccounts
        id="test"
        input={
          {
            value: {
              accMaskedValue: '',
              accEValue: 'accEValue',
              primaryName: 'test'
            }
          } as WrappedFieldInputProps
        }
        meta={{} as WrappedFieldMetaProps}
      />
    );

    expect(
      queryByClass(wrapper.container, 'data-value')
    ).not.toBeInTheDocument();
  });
});
