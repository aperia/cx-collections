import React from 'react';
import classNames from 'classnames';

// types
import { CommonControlProps } from 'app/_libraries/_dof/core';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch } from 'react-redux';

// components
import { TruncateText } from 'app/_libraries/_dls/components';

// Actions
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { formatMaskNumber } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface LinkAccountsControlProps extends CommonControlProps {
  options?: {
    classNameProps?: {
      container?: string;
      formatText?: string;
    };
    classNameValue?: string;
  };
  dataTestId?: string;
}

const LinkAccounts: React.FC<LinkAccountsControlProps> = ({
  input: { name, value: valueProp, onChange },
  meta: { error, valid, invalid, touched },
  options = {},
  id,
  label,
  tooltip,
  readOnly,
  required,
  enable,
  dataTestId,
  ...props
}) => {
  const { classNameProps, classNameValue } = options;

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const renderAddTab = (val: {
    accMaskedValue: string;
    accEValue: string;
    primaryName: string;
    memberSequenceIdentifier: string;
    socialSecurityIdentifier: string;
  }) => {
    const {
      accMaskedValue,
      accEValue,
      primaryName,
      memberSequenceIdentifier,
      socialSecurityIdentifier
    } = val;

    const mask = formatMaskNumber(accMaskedValue) || '';

    const handleOnClick = () => {
      if (!accEValue) return;

      let title = '';
      title =
        mask &&
        `${primaryName} - ${mask?.firstText + mask?.maskText + mask?.lastText}`;

      dispatch(
        tabActions.addTab({
          title,
          storeId: accEValue,
          accEValue: accEValue,
          tabType: 'accountDetail',
          iconName: 'account-details',
          props: {
            primaryName,
            accEValue: accEValue,
            storeId: accEValue,
            accNbr: accMaskedValue,
            memberSequenceIdentifier,
            socialSecurityIdentifier
          }
        })
      );
    };

    return (
      mask && (
        <div
          id={`${accMaskedValue}-add-tab`}
          onClick={handleOnClick}
          className={classNames('data-value', classNameProps?.formatText)}
        >
          <span className="custom-account-number">
            {mask?.firstText}
            <span className="hide-account-icon">{mask?.maskText}</span>
            {mask?.lastText}
          </span>
        </div>
      )
    );
  };

  return (
    <div
      className={classNames(classNameProps?.container, 'form-group-static')}
      id={id}
      data-testid={genAmtId(
        dataTestId!,
        'link-account-control',
        'LinkAccounts'
      )}
    >
      <span id={`${id}__label`} className="form-group-static__label">
        <TruncateText
          id={`${id}__label_text`}
          title={t(label)}
          dataTestId={dataTestId}
        >
          {t(label)}
        </TruncateText>
      </span>
      <div
        id={`${id}__text`}
        className={classNames('form-group-static__text', classNameValue)}
      >
        {renderAddTab(valueProp)}
      </div>
    </div>
  );
};

export default LinkAccounts;
