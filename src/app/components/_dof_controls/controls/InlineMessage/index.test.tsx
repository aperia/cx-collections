import React from 'react';

// components
import InlineMessage from './index';

// RTL
import { render, screen } from '@testing-library/react';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('InlineMessage dof control', () => {
  it('should not show component when value is undefined', () => {
    const { container } = render(
      <InlineMessage
        id="test"
        input={{} as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        props={{
          className: ''
        }}
        variant=""
      />
    );

    expect(container.innerHTML).toEqual('');
  });

  it('should not show component when value is undefined', () => {
    const { container } = render(
      <InlineMessage
        id="test"
        input={{} as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        props={{
          className: ''
        }}
      />
    );

    expect(container.innerHTML).toEqual('');
  });

  it('should not show component when value is empty', () => {
    const { container } = render(
      <InlineMessage
        id="test"
        input={{ value: [] } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        props={{
          className: ''
        }}
        variant=""
      />
    );

    expect(container.innerHTML).toEqual('');
  });

  it('should show component when value is string', () => {
    render(
      <InlineMessage
        id="test"
        input={{ value: 'Message 1\nMessage 2' } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        props={{
          className: ''
        }}
        variant=""
      />
    );

    expect(screen.getByText('Message 1')).toBeInTheDocument();
    expect(screen.getByText('Message 2')).toBeInTheDocument();
  });

  it('should show component when value is string array', () => {
    render(
      <InlineMessage
        id="test"
        input={{ value: ['Test'] } as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
        props={{
          className: ''
        }}
        variant=""
      />
    );

    expect(screen.getByText('Test')).toBeInTheDocument();
  });
});
