import React, { useMemo, useCallback } from 'react';

// types
import { CommonControlProps } from 'app/_libraries/_dof/core';
import { useTranslation } from 'app/_libraries/_dls/hooks';
// components
import { Radio } from 'app/_libraries/_dls/components';

// helpers
import classnames from 'classnames';
import { isArrayHasValue } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface IRadio {
  description: string;
  className: string;
  value: string;
  readOnly: boolean;
  disabled: boolean;
}

export interface GroupRadioControlProps_V2 extends CommonControlProps {
  radios: IRadio[];
  options?: {
    className?: string;
  };
  dataTestId?: string;
}

const GroupRadioControl_V2: React.FC<GroupRadioControlProps_V2> = ({
  input: { name, value: valueProp, onChange },
  meta: { error, valid, invalid, touched },
  options,
  radios,
  id,
  label,
  tooltip,
  readOnly,
  required,
  enable,
  dataTestId,
  ...props
}) => {
  const { t } = useTranslation();

  const handleChange = useCallback(
    data => {
      onChange(data);
    },
    [onChange]
  );

  const renderRadioInput = useMemo(() => {
    if (!isArrayHasValue(radios)) return null;
    return radios.map((item, index) => {
      const checked = valueProp === item.value ? true : false;
      return (
        <Radio
          key={index}
          className={classnames(item.className)}
          dataTestId={genAmtId(
            `${item.value.toString()}_${dataTestId || ''}`,
            'group-radio-control_v2_item',
            'GroupRadioControl_V2'
          )}
        >
          <Radio.Input
            id={item.value.toString()}
            name={name}
            onChange={() => handleChange(item.value)}
            checked={checked}
            readOnly={item.readOnly}
            disabled={item.disabled}
          />
          <Radio.Label>{t(item.description)}</Radio.Label>
        </Radio>
      );
    });
  }, [radios, name, dataTestId, t, valueProp, handleChange]);

  return (
    <div id={id} className={classnames(options?.className)}>
      {label ? (
        <div className="d-flex">
          <h6
            className="color-grey"
            data-testid={genAmtId(
              dataTestId!,
              'group-radio-control_v2_label',
              'GroupRadioControl_V2'
            )}
          >
            {t(label)}
          </h6>
          {required && (
            <span className="asterisk color-danger ml-4 mt-n4">*</span>
          )}
        </div>
      ) : null}
      <div
        className="custom-group-radio"
        data-testid={genAmtId(
          dataTestId!,
          'group-radio-control_v2_group',
          'GroupRadioControl_V2'
        )}
      >
        {renderRadioInput}
      </div>
    </div>
  );
};

export default GroupRadioControl_V2;
