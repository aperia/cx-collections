import React from 'react';

// RTL
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

// components
import GroupRadioControl_V2, { IRadio } from './index';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

// helpers
import { queryByClass } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks', () => {
  const hooks = jest.requireActual('app/_libraries/_dls/hooks');

  return {
    ...hooks,
    useTranslation: () => ({ t: (text: string) => text })
  };
});

describe('GroupRadioControl_V2 component', () => {
  const radiosData = [
    { description: 'Y - Valid', value: 'Y - Valid' },
    { description: 'N - Invalid', value: 'N - Invalid' }
  ] as IRadio[];
  const mockOnChange = jest.fn();
  const inputData = {
    value: 'Y - Valid',
    onChange: mockOnChange
  } as unknown as WrappedFieldInputProps;

  it('should not be shoed', () => {
    const { container } = render(
      <GroupRadioControl_V2
        id="test"
        input={inputData}
        meta={{} as WrappedFieldMetaProps}
        radios={[] as IRadio[]}
      />
    );

    const divElement = queryByClass(container, 'custom-group-radio');
    expect(divElement?.childElementCount).toEqual(0);
  });

  it('should show full information', () => {
    const { container } = render(
      <GroupRadioControl_V2
        id="test"
        label="Label"
        required
        input={inputData}
        meta={{} as WrappedFieldMetaProps}
        radios={radiosData}
      />
    );
    const divElement = queryByClass(container, 'custom-group-radio');
    expect(divElement?.childElementCount).toBeGreaterThan(1);

    const radio = screen.getAllByRole('radio');
    userEvent.click(radio[1]);

    expect(mockOnChange).toBeCalled();
  });

  it('should show full information', () => {
    const { container } = render(
      <GroupRadioControl_V2
        id="test"
        label="Label"
        required
        input={inputData}
        meta={{} as WrappedFieldMetaProps}
        radios={radiosData}
      />
    );
    const divElement = queryByClass(container, 'custom-group-radio');
    expect(divElement?.childElementCount).toBeGreaterThan(1);

    const radio = screen.getAllByRole('radio');
    userEvent.click(radio[1]);

    expect(mockOnChange).toBeCalled();
  });
});
