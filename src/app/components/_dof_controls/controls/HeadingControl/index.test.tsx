import React from 'react';

// components
import HeadingControl from './index';

// RTL
import { render, screen } from '@testing-library/react';

// helpers
import { getDefaultDOFMockProps } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks', () => ({
  ...jest.requireActual('app/_libraries/_dls/hooks'),
  useTranslation: () => ({ t: (text: string) => text })
}));

jest.mock('app/_libraries/_dls/components', () => ({
  ...jest.requireActual('app/_libraries/_dls/components'),
  Tooltip: ({ children }: { children: React.ReactNode }) => (
    <div data-testid="tooltip">{children}</div>
  )
}));

const props = getDefaultDOFMockProps();

describe('Test HeadingControl component', () => {
  it('render default h5 tag', () => {
    const wrapper = render(<HeadingControl {...props} />);

    expect(wrapper.container.querySelector('h5')).toBeInTheDocument();
  });

  it('render tooltip', () => {
    render(<HeadingControl {...props} tooltip="Test tooltip" />);

    expect(screen.getByTestId('tooltip')).toBeInTheDocument();
    expect(screen.getByTestId('tooltip').innerHTML).toEqual(
      '<i class="icon icon-information size-5x color-grey-l32"></i>'
    );
  });
});
