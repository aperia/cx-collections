import React from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import { Icon } from 'app/_libraries/_dls/components';
import classnames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface AlertMessageControlProps extends CommonControlProps {
  dataTestId?: string;
  options?: {
    message?: string;
    multiLinesMessage?: boolean;
  };
}

const AlertMessage: React.FC<AlertMessageControlProps> = ({
  input: { value },
  id,
  dataTestId,
  options
}) => {
  const testId = genAmtId(
    dataTestId!,
    'alert-message-control',
    'AlertMessageControl'
  );

  if (!value && !options?.message) return null;

  if (value && value.length > 0 && options?.multiLinesMessage) {
    return (
      <div className="d-flex" data-testid={testId}>
        <div className="color-orange-d08 mr-8 mt-i-4">
          <Icon
            name="warning"
            size="4x"
            dataTestId={`${dataTestId}-icon-warning`}
          />
        </div>
        <div>
          {value.map((message: string, index: number) => {
            const isFirstIndex = index === 0;
            return (
              <p
                key={index}
                className={classnames('fs-12 color-grey-l16', {
                  'mt-16': !isFirstIndex
                })}
              >
                {message}
              </p>
            );
          })}
        </div>
      </div>
    );
  }
  return (
    <div className="d-flex mt-16" data-testid={testId}>
      <div className="color-orange-d08 mr-8 mt-i-4">
        <Icon
          name="warning"
          size="4x"
          dataTestId={`${dataTestId}-icon-warning`}
        />
      </div>
      <p id={id} className="color-grey fs-12">
        {value || options?.message}
      </p>
    </div>
  );
};

export default AlertMessage;
