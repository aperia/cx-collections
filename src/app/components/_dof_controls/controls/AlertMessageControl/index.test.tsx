import React from 'react';

// RTL
import { render, screen } from '@testing-library/react';

// helpers
import { getDefaultDOFMockProps } from 'app/test-utils';

// components
import AlertMessageControl, { AlertMessageControlProps } from './index';

const mockSingleMsg = 'alert message';
const mockMultipleMsg = ['alert message', 'alert message 2'];

const renderComponent = (dataTest: AlertMessageControlProps) => {
  return render(<AlertMessageControl {...dataTest} />);
};

describe('AlertMessageControl', () => {
  it('should render single message', () => {
    const dataTest = getDefaultDOFMockProps({
      value: mockSingleMsg
    });

    renderComponent(dataTest);

    expect(screen.getByText(mockSingleMsg)).toBeInTheDocument();
  });

  it('should render multiLines Message', () => {
    const dataTest = getDefaultDOFMockProps({
      value: mockMultipleMsg,
      options: { multiLinesMessage: true }
    });

    renderComponent(dataTest);

    expect(screen.getByText(mockMultipleMsg[0])).toBeInTheDocument();
    expect(screen.getByText(mockMultipleMsg[1])).toBeInTheDocument();
  });

  it('should render null', () => {
    const dataTest = getDefaultDOFMockProps({});

    const { container } = renderComponent(dataTest);

    expect(container.firstChild).toEqual(null);
  });

  it('should render with option message and without value', () => {
    const dataTest = getDefaultDOFMockProps({
      options: { message: mockSingleMsg }
    });

    renderComponent(dataTest);

    expect(screen.getByText(mockSingleMsg)).toBeInTheDocument();
  });
});
