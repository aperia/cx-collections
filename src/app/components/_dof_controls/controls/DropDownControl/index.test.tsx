import React from 'react';

// components
import DropDownControl from './index';

// helpers
import { queryByClass, renderMockStore } from 'app/test-utils';

// RTL
import { act, fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  form: {
    formName: {
      registeredFields: [],
      values: { dependentValue: 'dependentValue', emptyValue: '' }
    },
    formName1: {
      registeredFields: [],
      values: { dependentValue: '', emptyValue: 'empty' }
    },
    formName2: {
      registeredFields: [],
      values: {
        dependentValue: new Date(),
        emptyValue: {
          other: 'a'
        }
      }
    }
  }
};

const data = [
  { id: '1', value: 'Test' },
  { id: '2', value: 'Unit test' },
  { id: '3', value: 'Unit test' }
];

describe('Test DropDownControl', () => {
  it('render component', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <DropDownControl
        id="id"
        meta={{ form: 'formName' } as WrappedFieldMetaProps}
        input={{} as WrappedFieldInputProps}
      />,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).toBeInTheDocument();
  });

  it('render component', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <DropDownControl
        id="id"
        meta={{ form: 'formName' } as WrappedFieldMetaProps}
        input={{} as WrappedFieldInputProps}
        data={data as [] | undefined}
      />,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).toBeInTheDocument();
  });

  it('render component > dependentValues, combineValues, uniqueField', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <DropDownControl
        id="id"
        meta={{ form: 'formName' } as WrappedFieldMetaProps}
        input={{ value: {} } as WrappedFieldInputProps}
        data={data as [] | undefined}
        dependentValues={['id', 'value']}
        uniqueField="id"
        options={{
          combineValues: true
        }}
      />,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).toBeInTheDocument();
  });

  it('render component > dependentValues, combineValues', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <DropDownControl
        id="id"
        meta={{ form: 'formName' } as WrappedFieldMetaProps}
        input={{ value: {} } as WrappedFieldInputProps}
        data={data as [] | undefined}
        dependentValues={['id', 'value']}
        options={{
          combineValues: true
        }}
      />,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).toBeInTheDocument();
  });

  it('render component > error', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <div>
        <DropDownControl
          id="id"
          meta={
            {
              form: 'formName',
              error: 'Error Message',
              invalid: true,
              touched: true
            } as WrappedFieldMetaProps
          }
          input={{} as WrappedFieldInputProps}
          fieldDisplay={{
            isDefault: true,
            value: 'dependentValue',
            field: 'dependentValue'
          }}
          enable
          required
        />
      </div>,
      { initialState }
    );
    jest.runAllTimers();

    const element = queryByClass(wrapper.container, 'text-field-container');
    act(() => {
      userEvent.click(element!);
    });
    jest.runAllTimers();

    expect(screen.getByText('Error Message')).toBeInTheDocument();
  });

  it('render component > error from options argument', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <div>
        <DropDownControl
          id="id"
          meta={
            {
              form: 'formName',
              error: 'Error Message',
              invalid: false,
              touched: true
            } as WrappedFieldMetaProps
          }
          input={{} as WrappedFieldInputProps}
          fieldDisplay={{
            isDefault: true,
            value: 'dependentValue',
            field: 'dependentValue'
          }}
          options={{
            isError: true
          }}
          enable
          required
        />
      </div>,
      { initialState }
    );
    jest.runAllTimers();

    const element = queryByClass(wrapper.container, 'text-field-container');
    act(() => {
      userEvent.click(element!);
    });
    jest.runAllTimers();

    expect(screen.getByText('txt_no_results_found')).toBeInTheDocument();
  });

  it('render component > handleOnBlur, handleOnChange', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <div>
        <DropDownControl
          id="id"
          meta={{ form: 'formName' } as WrappedFieldMetaProps}
          input={
            {
              value: {},
              onChange: onChangeSpy,
              onBlur: onBlurSpy
            } as unknown as WrappedFieldInputProps
          }
          data={data as [] | undefined}
          options={{
            combineValues: true
          }}
          textField="value"
          uniqueField="id"
          enable
        />
      </div>,
      { initialState }
    );
    jest.runAllTimers();

    const element = queryByClass(wrapper.container, 'text-field-container');
    act(() => {
      userEvent.click(element!);
    });
    jest.runAllTimers();

    act(() => {
      userEvent.click(screen.getByText('1 - Test'));
    });
    jest.runAllTimers();

    expect(onChangeSpy).toBeCalled();
    expect(onBlurSpy).toBeCalled();
  });

  it('render component > handleOnBlur, handleOnChange', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <div>
        <DropDownControl
          id="id"
          meta={{ form: 'formName' } as WrappedFieldMetaProps}
          input={
            {
              value: {},
              onChange: onChangeSpy,
              onBlur: onBlurSpy
            } as unknown as WrappedFieldInputProps
          }
          data={data as [] | undefined}
          options={{
            combineValues: true
          }}
          textField="value"
          uniqueField="id"
          enable
          readOnly
        />
      </div>,
      { initialState }
    );
    jest.runAllTimers();

    const element = queryByClass(wrapper.container, 'input');
    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    act(() => {
      fireEvent.blur(element!);
    });
    jest.runAllTimers();

    expect(onChangeSpy).not.toBeCalled();
    expect(onBlurSpy).not.toBeCalled();
  });

  it('render component > fieldDisplay', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <div>
        <DropDownControl
          id="id"
          meta={
            {
              form: 'formName'
            } as WrappedFieldMetaProps
          }
          input={{} as WrappedFieldInputProps}
          fieldDisplay={{
            isDefault: false,
            value: 'dependentValue',
            field: 'emptyValue'
          }}
          enable
          required
        />
      </div>,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).not.toBeInTheDocument();
  });

  it('render component > fieldDisplay', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <div>
        <DropDownControl
          id="id"
          meta={
            {
              form: 'formName'
            } as WrappedFieldMetaProps
          }
          input={{} as WrappedFieldInputProps}
          fieldDisplay={{
            isDefault: false,
            value: 'dependentValue',
            field: 'dependentValue'
          }}
          enable
          required
        />
      </div>,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).toBeInTheDocument();
  });

  it('render component > fieldDisplay', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <div>
        <DropDownControl
          id="id"
          meta={
            {
              form: 'formName1'
            } as WrappedFieldMetaProps
          }
          input={{} as WrappedFieldInputProps}
          fieldDisplay={{
            isDefault: true,
            value: 'dependentValue',
            field: 'emptyValue'
          }}
          enable
          required
        />
      </div>,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).not.toBeInTheDocument();
  });

  it('dependentValues > valid date data', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <DropDownControl
        id="id"
        meta={{ form: 'formName2' } as WrappedFieldMetaProps}
        input={{ value: {} } as WrappedFieldInputProps}
        data={data as [] | undefined}
        dependentValues={['dependentValue', 'value']}
        uniqueField="id"
        options={{
          combineValues: true
        }}
      />,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).toBeInTheDocument();
  });

  it('dependentValue', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <DropDownControl
        id="id"
        meta={{ form: 'formName2' } as WrappedFieldMetaProps}
        input={{ value: {} } as WrappedFieldInputProps}
        data={data as [] | undefined}
        dependentValue="dependentValue"
        uniqueField="id"
        options={{
          combineValues: true
        }}
      />,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).toBeInTheDocument();
  });

  it('dependentValue', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <DropDownControl
        id="id"
        meta={{ form: 'formName2' } as WrappedFieldMetaProps}
        input={{ value: {} } as WrappedFieldInputProps}
        data={data as [] | undefined}
        dependentValue="emptyValue"
        uniqueField="id"
        options={{
          combineValues: true
        }}
      />,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).toBeInTheDocument();
  });

  it('uniqueField, having value', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <DropDownControl
        id="id"
        meta={{ form: 'formName2' } as WrappedFieldMetaProps}
        input={{ value: '3' } as WrappedFieldInputProps}
        data={data as [] | undefined}
        uniqueField="id"
      />,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).toBeInTheDocument();
  });

  it('uniqueField, having value', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(
      <DropDownControl
        id="id"
        meta={{ form: 'formName2' } as WrappedFieldMetaProps}
        input={{ value: '4' } as WrappedFieldInputProps}
        data={data as [] | undefined}
        uniqueField="id"
      />,
      { initialState }
    );
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, /dls-dropdown-list/)
    ).toBeInTheDocument();
  });
});
