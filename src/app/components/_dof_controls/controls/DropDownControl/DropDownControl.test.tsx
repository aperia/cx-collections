import React from 'react';
// components
import DropDownControl from './index';
// helpers
import { renderMockStore } from 'app/test-utils';
// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';
// RTL
import { fireEvent, screen } from '@testing-library/react';

jest.mock('app/_libraries/_dls/components/DropdownList', () =>
  jest.requireActual('app/test-utils/mocks/MockDropdownList')
);

HTMLCanvasElement.prototype.getContext = jest.fn();
const initialState: Partial<RootState> = {
  form: {
    formName: {
      registeredFields: [],
      values: { dependentValue: 'dependentValue', emptyValue: '' }
    },
    formName1: {
      registeredFields: [],
      values: { dependentValue: '', emptyValue: 'empty' }
    },
    formName2: {
      registeredFields: [],
      values: {
        dependentValue: new Date(),
        emptyValue: {
          other: 'a'
        }
      }
    }
  }
};

const data = [
  { id: '1', value: 'Test' },
  { id: '2', value: 'Unit test' },
  { id: '3', value: 'Unit test' },
  { id: '4', value: null }
];

describe('Test DropDownControl', () => {
  it('handleOnChange > value = null', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    jest.useFakeTimers();
    renderMockStore(
      <div>
        <DropDownControl
          id="id"
          meta={{ form: 'formName' } as WrappedFieldMetaProps}
          input={
            {
              value: {},
              onChange: onChangeSpy,
              onBlur: onBlurSpy
            } as unknown as WrappedFieldInputProps
          }
          data={data as [] | undefined}
          options={{
            combineValues: true
          }}
          textField="value"
          uniqueField="id"
          enable
        />
      </div>,
      { initialState }
    );
    jest.runAllTimers();

    fireEvent.change(screen.getByTestId('DropdownList.onChange'), {
      target: { value: 'undefined', valueObject: null }
    });
    jest.runAllTimers();
    expect(onChangeSpy).not.toBeCalled();
  });
});
