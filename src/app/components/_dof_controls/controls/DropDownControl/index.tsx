import React, { useEffect, useState } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';

// components
import {
  DropdownBaseChangeEvent,
  DropdownListProps,
  DropdownList
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import isString from 'lodash.isstring';
import isEqual from 'lodash.isequal';
import isNull from 'lodash.isnull';

// redux
import { useSelector } from 'react-redux';
import { lengthMonth } from 'pages/HardShip/helpers';
import { isInvalidDateValue } from 'app/helpers';
import isUndefined from 'lodash.isundefined';
import pickBy from 'lodash.pickby';

export interface DropDownControlProps
  extends CommonControlProps,
    Omit<DropdownListProps, 'id' | 'label' | 'children'> {
  options?: {
    combineValues?: boolean;
    isHaveNoneSelection?: boolean;
    isError?: boolean;
  };
  uniqueField?: string;
  dependentValue?: string;
  dependentValues?: string[];
  dependentType?: string;
  fieldDisplay?: {
    isDefault: boolean;
    value: string;
    field: string;
  };
  className?: string;
}

export const DropDownControl: React.FC<DropDownControlProps> = props => {
  const {
    label,
    enable,
    data = [],
    input: { name, value: valueProp, onChange, onBlur },
    meta: { error, invalid, touched, form },
    readOnly,
    required,
    textField = 'fieldValue',
    options,
    uniqueField = '',
    dependentValue = '',
    dependentValues = [],
    dependentType,
    fieldDisplay,
    dataTestId,
    className
  } = props;

  const [value, setValue] = useState<any>(valueProp);

  const { t } = useTranslation();

  const formData = useSelector<RootState, MagicKeyValue>(
    state => state?.form[form]
  );

  const dependentValueField = isEmpty(
    get(formData?.values, dependentValue, '')
  );

  // cover case Hardship Form
  let isDisableDependent = false;
  let lengthData = 0;

  if (!isEmpty(dependentValues)) {
    lengthData = lengthMonth(formData?.values, dependentValues, dependentType);

    dependentValues.forEach(item => {
      const formValues = get(formData?.values, item, '');
      const formError = get(formData?.syncErrors, item, '');
      if (isDisableDependent) return;
      isDisableDependent =
        isInvalidDateValue(formValues) || !isEmpty(formError);
    });
  }

  const err = isEmpty(get(formData?.syncErrors, dependentValue, ''));

  const disable = isEmpty(dependentValue)
    ? !enable
    : dependentValueField || !err;

  const handleOnChange = (event: DropdownBaseChangeEvent) => {
    const { value: dropdownValue } = event.target;
    if (isNull(dropdownValue)) return setValue(null);
    setValue(dropdownValue);
  };

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    if (disabled || readOnly) return;
    onChange(value);
    onBlur(value);
  };

  useEffect(() => {
    if (isString(valueProp)) {
      const item =
        uniqueField && data?.find(val => val[uniqueField] === valueProp);
      setValue(item);
    } else {
      uniqueField
        ? setValue(
            data?.find(val => val[uniqueField] === valueProp[uniqueField])
          )
        : setValue(data?.find(val => isEqual(val, valueProp)));
    }
  }, [valueProp, data, uniqueField]);

  const disabled = disable || isDisableDependent; // cover case Hardship Form

  // fieldDisplay for case Hardship
  if (fieldDisplay) {
    const dependValue = get(formData?.values, fieldDisplay.field);
    if (fieldDisplay.isDefault) {
      if (dependValue !== fieldDisplay.value && !isEmpty(dependValue))
        return null;
    } else {
      if (dependValue !== fieldDisplay.value) return null;
    }
  }

  const isShowAsterisk = !(disabled || readOnly);

  const { combineValues, isHaveNoneSelection } = options || {};
  const mappingValue = data?.find(val => isEqual(val, value));

  const errorTooltipProps = {
    opened: !options?.isError || invalid ? undefined : false
  };

  const formatCombineValues = (
    uniqueFieldValue?: string,
    textFieldValue?: string
  ) => {
    if (isUndefined(uniqueFieldValue) && isUndefined(textFieldValue)) return '';
    if (combineValues && uniqueField) {
      if (isUndefined(textFieldValue) || isEmpty(textFieldValue))
        return uniqueFieldValue;
      return `${uniqueFieldValue} - ${t(textFieldValue)}`;
    }
    return t(textFieldValue);
  };

  return (
    <DropdownList
      dataTestId={dataTestId}
      className={className}
      optional={isHaveNoneSelection}
      name={name}
      label={t(label)}
      disabled={disabled}
      readOnly={readOnly}
      textField={textField}
      textFieldRender={itemValue => {
        return (
          <>
            {formatCombineValues(
              itemValue?.[uniqueField],
              itemValue?.[textField]
            )}
          </>
        );
      }}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      value={uniqueField && data?.length ? mappingValue : value}
      required={isShowAsterisk && enable && required}
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
      error={
        (isShowAsterisk && enable && required && touched && invalid) ||
        options?.isError
          ? {
              message: t(error),
              status: invalid || Boolean(options?.isError)
            }
          : undefined
      }
      errorTooltipProps={pickBy(errorTooltipProps, item => !isUndefined(item))}
      noResult={t('txt_no_results_found')}
    >
      {!isEmpty(dependentValues)
        ? data?.slice(0, lengthData + 1).map((item, index) => {
            const dropDownLbl = formatCombineValues(
              item[uniqueField],
              item[textField]
            );
            return (
              <DropdownList.Item value={item} label={dropDownLbl} key={index} />
            );
          })
        : data?.map((item, index) => {
            const dropDownLbl = formatCombineValues(
              item[uniqueField],
              item[textField]
            );
            return (
              <DropdownList.Item value={item} label={dropDownLbl} key={index} />
            );
          })}
    </DropdownList>
  );
};

export default DropDownControl;
