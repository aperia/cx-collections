import React from 'react';

// types
import { CommonControlProps } from 'app/_libraries/_dof/core';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { Radio } from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface DateTypeRadioControlProps extends CommonControlProps {
  dataTestId?: string;
}

const DateTypeRadioControl: React.FC<DateTypeRadioControlProps> = ({
  input: { name, value: valueProp, onChange, onBlur },
  meta: { error, valid, invalid, touched },
  id,
  dataTestId,
  label,
  tooltip,
  readOnly,
  required,
  enable,
  ...props
}) => {
  const { t } = useTranslation();

  return (
    <div id={id} className="mb-16">
      <span className="mr-24 fw-500 color-grey">{t(label)}:</span>
      <Radio
        className="d-inline-block mr-24"
        dataTestId={genAmtId(
          dataTestId!,
          'date-type-radio_specific-date',
          'DateTypeRadioControl'
        )}
      >
        <Radio.Input
          name="specificDate"
          checked={valueProp?.specificDate}
          onChange={() =>
            onChange({
              dateRange: false,
              specificDate: true
            })
          }
        ></Radio.Input>
        <Radio.Label>{t('txt_specific_date')}</Radio.Label>
      </Radio>
      <Radio
        className="d-inline-block"
        dataTestId={genAmtId(
          dataTestId!,
          'date-type-radio_date-range',
          'DateTypeRadioControl'
        )}
      >
        <Radio.Input
          name="dateRange"
          checked={valueProp?.dateRange}
          onChange={() =>
            onChange({
              dateRange: true,
              specificDate: false
            })
          }
        ></Radio.Input>
        <Radio.Label>{t('txt_date_range')}</Radio.Label>
      </Radio>
    </div>
  );
};

export default DateTypeRadioControl;
