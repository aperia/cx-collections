import React from 'react';

// RTL
import { act, render, screen } from '@testing-library/react';

// components
import DateTypeRadioControl from './index';

// types
import { WrappedFieldInputProps, WrappedFieldMetaProps } from 'redux-form';
import userEvent from '@testing-library/user-event';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test DateTypeRadioControl control', () => {
  it('render component', () => {
    render(
      <DateTypeRadioControl
        id="id"
        input={{} as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );

    expect(screen.getByText('txt_specific_date')).toBeInTheDocument();
    expect(screen.getByText('txt_date_range')).toBeInTheDocument();
    expect(screen.getAllByRole('radio').length).toEqual(2);
  });

  it('clicks on radio', () => {
    const onChangeSpy = jest.fn();

    render(
      <DateTypeRadioControl
        id="id"
        input={{ onChange: onChangeSpy } as unknown as WrappedFieldInputProps}
        meta={{} as WrappedFieldMetaProps}
      />
    );

    const inputElements = screen.getAllByRole('radio');
    act(() => {
      userEvent.click(inputElements[0]);
      userEvent.click(inputElements[1]);
    });

    expect(onChangeSpy).toBeCalled();
  });
});
