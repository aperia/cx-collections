import { Bubble, TruncateText } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React from 'react';

export interface BubbleControlProps extends CommonControlProps {
  assignName?: string;
  dataTestId?: string;
  className?: string;
}
export const BubbleControl: React.FC<BubbleControlProps> = ({
  input: { value },
  id,
  dataTestId,
  className = '',
  label,
  ...props
}) => {
  const { t } = useTranslation();
  return (
    <div
      className={className}
      id={id}
      data-testid={genAmtId(dataTestId!, 'bubble-control', 'BubbleControl')}
    >
      <div>
        <TruncateText
          id={`${id}__label_text`}
          title={t(label)}
          dataTestId={dataTestId}
          className="text-uppercase"
        >
          {t(label)}
        </TruncateText>
      </div>
      <Bubble small name={value} />
      <span className="bubble-name">{value}</span>
    </div>
  );
};

export default BubbleControl;
