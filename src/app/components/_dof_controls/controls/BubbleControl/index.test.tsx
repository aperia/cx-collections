import React from 'react';
import { render, screen } from '@testing-library/react';
import 'app/test-utils/mocks/MockToolTip';
import 'app/test-utils/mocks/MockCheckboxInput';
import { BubbleControl } from './index';
import { getDefaultDOFMockProps } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('BubbleControl', () => {
  it('Should render', () => {
    const mockProps = getDefaultDOFMockProps();
    const label = 'label';
    render(<BubbleControl {...mockProps} label={label} />);
    expect(screen.getByText(label)).toBeInTheDocument();
  });
});
