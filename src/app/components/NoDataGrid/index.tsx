import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { Icon, IconProps } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { isString } from 'lodash';
import React from 'react';

export interface NoDataGridProps {
  iconName?: IconProps['name'];
  text?: React.ReactNode;
  children?: React.ReactNode;
  dataTestId?: string;
}

export const NoDataGrid: React.FC<NoDataGridProps> = ({
  iconName = 'file',
  text = I18N_COMMON_TEXT.NO_COMPANIES_TO_DISPLAY,
  children,
  dataTestId
}) => {
  const { t } = useTranslation();
  return (
    <div
      className="text-center my-80"
      data-testid={genAmtId(dataTestId, 'no-data-grid', 'NoDataGrid')}
    >
      <Icon
        name={iconName}
        className="fs-80 color-light-l12"
        dataTestId={dataTestId}
      />
      {isString(text) ? (
        <p
          className="mt-20 color-grey"
          data-testid={genAmtId(dataTestId, 'no-data-grid_text', 'NoDataGrid')}
        >
          {t(text)}
        </p>
      ) : (
        text
      )}

      {children}
    </div>
  );
};
