import React from 'react';
// components
import { NoDataGrid } from './index';
// RTL
import { render, screen } from '@testing-library/react';
// helpers
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { queryByClass } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test NoDataGrid component', () => {
  it('render component > default', () => {
    render(<NoDataGrid />);
    expect(
      screen.getByText(I18N_COMMON_TEXT.NO_COMPANIES_TO_DISPLAY)
    ).toBeInTheDocument();
  });

  it('render component > with props', () => {
    const expectText = 'Test';
    const { container } = render(
      <NoDataGrid text={expectText} iconName="add-file" />
    );
    expect(queryByClass(container, /add-file/)).toBeInTheDocument();
    expect(screen.getByText(expectText)).toBeInTheDocument();
  });

  it('render with text Element', () => {
    const expectText = <p>Text</p>;
    const { container } = render(
      <NoDataGrid text={expectText} iconName="add-file" />
    );
    expect(queryByClass(container, /add-file/)).toBeInTheDocument();
    expect(screen.getByText('Text')).toBeInTheDocument();
  });
});
