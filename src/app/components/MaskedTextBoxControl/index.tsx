import React from 'react';
import { MaskedTextBoxControl as MaskedTextBoxControlDLS } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';

const MaskedTextBoxControl: React.FC<any> = ({
  placeholder,
  SSN_MASK_INPUT_REGEX,
  dataTestId,
  ...props
}) => {
  const { t } = useTranslation();
  return (
    <MaskedTextBoxControlDLS
      dataTestId={dataTestId}
      placeholder={t(placeholder)}
      mask={SSN_MASK_INPUT_REGEX}
      {...props}
    />
  );
};

export default MaskedTextBoxControl;
