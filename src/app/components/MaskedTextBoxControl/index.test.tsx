import React from 'react';
import MaskedTextBoxControl from './index';
import { render, screen } from '@testing-library/react';
import { SSN_MASK_INPUT_REGEX } from 'pages/AccountSearch/constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test MaskedTextBoxControl component', () => {
  it('render default component', () => {
    render(
      <MaskedTextBoxControl
        placeholder="Placeholder"
        SSN_MASK_INPUT_REGEX={SSN_MASK_INPUT_REGEX}
      />
    );

    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });
});
