import React, { useMemo } from 'react';
import { REF_DATA_TYPE } from 'pages/AccountSearch/Home/types';
import { useSelector } from 'react-redux';
import isEmpty from 'lodash.isempty';

import ComboBox from 'app/_libraries/_dls/components/ComboBox';
import ComboboxControl, {
  ComboBoxControlProps
} from 'app/_libraries/_dls/components/AttributeSearch/controls/ComboboxControl';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface ComboboxAttributeSearchControlProps
  extends ComboBoxControlProps {
  placeholder?: string;
}

const ComboBoxControl: React.FC<ComboboxAttributeSearchControlProps> = ({
  placeholder,
  ...props
}) => {
  const { t } = useTranslation();

  const stateRefData = useSelector<RootState, RefDataValue[] | undefined>(
    state => state.refData[REF_DATA_TYPE.STATE]?.data
  );

  const comboboxItems = useMemo<any>(() => {
    return stateRefData?.map((item: RefDataValue, index: number) => (
      <ComboBox.Item key={index} label={item.value} value={item} />
    ));
  }, [stateRefData]);

  return (
    <>
      {!isEmpty(stateRefData) && (
        <ComboboxControl
          textField="value"
          itemKey="value"
          placeholder={t(placeholder)}
          noResult={t('txt_no_results_found')}
          {...props}
        >
          {comboboxItems}
        </ComboboxControl>
      )}
    </>
  );
};

export default ComboBoxControl;
