import { queryAllByClass, queryBy, renderMockStoreId } from 'app/test-utils';
import React from 'react';
import ComboBoxControl, { ComboboxAttributeSearchControlProps } from './index';
import { act } from '@testing-library/react';

HTMLCanvasElement.prototype.getContext = jest.fn();
const data: RefDataValue[] = [
  {
    countryCode: 'USA',
    description: 'APO/FPO Florida',
    value: 'AA'
  } as RefDataValue
];

const initialState: Partial<RootState> = {
  refData: {
    stateRefData: {
      data
    }
  }
};

const props = {} as ComboboxAttributeSearchControlProps;

describe('Test ComboBoxControl component', () => {
  it('Should have render component', () => {
    const placeholder = 'test-place-holder';
    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(
      <ComboBoxControl placeholder={placeholder} {...props} />,
      {
        initialState
      }
    );
    jest.runAllTimers();
    const queryInput = queryBy(wrapper.container, 'placeholder', placeholder);
    act(() => {
      queryInput!.focus();
      queryInput!.click();
    });
    const queryItem = queryAllByClass(
      wrapper.baseElement as HTMLElement,
      /item/
    );
    expect(queryItem.length).toEqual(data.length);
  });
});
