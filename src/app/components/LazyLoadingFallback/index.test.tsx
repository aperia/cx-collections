import React from 'react';
import LazyLoadingFallback from './index';
import { render } from '@testing-library/react';

describe('Test LazyLoadingFallback component', () => {
  it('should render default component', () => {
    const wrapper = render(<LazyLoadingFallback />);

    expect(
      wrapper.container.querySelector('div[class="vh-100 loading"]')
    ).toBeInTheDocument();
  });

  it('should render component', () => {
    const wrapper = render(<LazyLoadingFallback classNames="class-name" />);

    expect(
      wrapper.container.querySelector('div[class="vh-100 loading class-name"]')
    ).toBeInTheDocument();
  });
});
