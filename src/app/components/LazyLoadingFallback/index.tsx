import classNames from 'classnames';
import React from 'react';

export interface LazyLoadingFallbackProps {
  classNames?: string;
}

const LazyLoadingFallback: React.FC<LazyLoadingFallbackProps> = ({
  classNames: classNamesProp
}) => {
  return <div className={classNames('vh-100 loading', classNamesProp)}></div>;
};

export default LazyLoadingFallback;
