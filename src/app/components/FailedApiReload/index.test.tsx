import React from 'react';
import FailedApiReload from './index';
import { render, screen } from '@testing-library/react';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test FailedApiReload component', () => {
  it('render component', () => {
    const onReloadSpy = jest.fn();

    const wrapper = render(<FailedApiReload id="id" onReload={onReloadSpy} />);
    const reloadBtn = screen.getByText('txt_reload');
    reloadBtn?.click();

    expect(
      wrapper.container.querySelector('p[class="color-grey-d20"]')?.innerHTML
    ).toEqual('txt_data_load_unsuccessful_click_reload_to_try_again');
    expect(
      wrapper.container.querySelector('p[class="fs-12 color-grey mt-4"]')
        ?.innerHTML
    ).toEqual('txt_failure_ocurred_on');
    expect(screen.getByText('txt_reload')).toBeInTheDocument();
    expect(onReloadSpy).toBeCalled();
  });
});
