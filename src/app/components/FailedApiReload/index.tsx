import React, { FC } from 'react';
import classNames from 'classnames';
import { format } from 'date-and-time';
import { Button } from 'app/_libraries/_dls/components';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

interface FailedApiReloadProps {
  id: string;
  dataTestId?: string;
  className?: string;
  dataLoadText?: string;
  onReload: () => void;
}
const FailedApiReload: FC<FailedApiReloadProps> = ({
  id,
  dataTestId,
  className = 'mt-24',
  onReload,
  dataLoadText = 'txt_data_load_unsuccessful_click_reload_to_try_again'
}) => {
  const { t } = useTranslation();
  const date = format(new Date(), 'MM/DD/YYYY hh:mm:ss A');

  return (
    <div className={classNames('text-center', className)}>
      <p
        className="color-grey-d20"
        data-testid={genAmtId(dataTestId!, 'failure-text', 'FailedApiReload')}
      >
        {t(dataLoadText)}
      </p>
      <p
        className="fs-12 color-grey mt-4"
        data-testid={genAmtId(dataTestId!, 'failure-date', 'FailedApiReload')}
      >
        {t('txt_failure_ocurred_on', { date })}
      </p>
      <Button
        id={`${id}_Button`}
        dataTestId={genAmtId(dataTestId!, 'reload-btn', 'FailedApiReload')}
        onClick={onReload}
        className="mt-24"
        size="sm"
      >
        {t(I18N_COMMON_TEXT.RELOAD)}
      </Button>
    </div>
  );
};

export default FailedApiReload;
