import { renderMockStoreId } from 'app/test-utils';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import React from 'react';
import TextViewGroup from './index';

describe('Render TextViewGroup', () => {
  it('should render component', () => {
    const { wrapper } = renderMockStoreId(<TextViewGroup />);
    const queryClass = queryByClass(wrapper.container, /d-flex/);
    expect(queryClass).toBeInTheDocument();
  });

  it('should render component > case 2', () => {
    const { wrapper } = renderMockStoreId(<TextViewGroup label="Mock label" />);
    const queryClass = queryByClass(wrapper.container, /d-flex/);
    expect(queryClass).toBeInTheDocument();
  });
  it('should render component > case 3', () => {
    const fnRender = () => <div>Hello</div>;
    
    const { wrapper } = renderMockStoreId(
      <TextViewGroup label={fnRender}>{}</TextViewGroup>
    );
    const queryClass = queryByClass(wrapper.container, /d-flex/);
    expect(queryClass).toBeInTheDocument();
  });

  it('should render component > case 4', () => {
    
    const { wrapper } = renderMockStoreId(
      <TextViewGroup label={4}>{}</TextViewGroup>
    );
    const queryClass = queryByClass(wrapper.container, /d-flex/);
    expect(queryClass).toBeInTheDocument();
  });
});
