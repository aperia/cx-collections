import React from 'react';

// helpers
import classnames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface TextViewGroupProps {
  label?: React.ReactNode;
  content?: React.ReactNode;
  labelClassName?: string;
  contentClassName?: string;
  testId?: string;
}
const TextViewGroup: React.FC<TextViewGroupProps> = ({
  testId,
  label,
  content,
  labelClassName,
  contentClassName
}) => {
  const renders = (renderContent: React.ReactNode) => {
    if (!renderContent) return null;

    switch (typeof renderContent) {
      case 'string':
        return renderContent;
      case 'function':
        return renderContent();
      default:
        return renderContent;
    }
  };
  return (
    <div
      data-testid={genAmtId(testId, `${testId}_container`, '')}
      className="d-flex"
    >
      <span
        data-testid={genAmtId(testId, `${testId}_label`, '')}
        className={classnames(labelClassName)}
      >
        {renders(label)}
      </span>
      <span
        data-testid={genAmtId(testId, `${testId}_content`, '')}
        className={classnames(contentClassName)}
      >
        {renders(content)}
      </span>
    </div>
  );
};

export default TextViewGroup;
