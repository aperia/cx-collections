import React, { useEffect, useState } from 'react';
import classNames from 'classnames';

// DOF
import { App as DOFApp, DataApiContext } from 'app/_libraries/_dof/core';
import 'app/_libraries/_dof/layouts';
import 'registerControls';

// Components
import ESSConfig from 'pages/__commons/ESSConfig';

// Reducers
import { reducerMappingList } from './storeConfig';

// Middleware
import { refData } from 'app/components/_dof_controls/ref-data';

// config
import axios from 'axios';

// Helper
import { DATA_SOURCE } from 'app/helpers';
import { useSaveSessionStorage } from 'app/hooks';

export interface AppProps {}

window.debug = true;

export const urlConfigs = [
  'dof/views/divider.json',

  'dof/views/accountCollectionDelinquent.json',
  'dof/views/accountCollectionDelinquencyHistory.json',
  'dof/views/accountCollectionOther.json',
  'dof/views/accountCollectionDelinquencyCycleSummary.json',
  'dof/views/accountCollectionCollectionHistory.json',
  'dof/views/accountCollectionPromiseDetails.json',

  'dof/views/accountInformationCreditLife.json',
  'dof/views/accountInformationLastNonMonetary.json',
  'dof/views/accountInformationCrossReference.json',
  'dof/views/accountInformationCreditLineChange.json',
  'dof/views/accountInformationFlags.json',
  'dof/views/accountInformationOther.json',
  'dof/views/accountInformationDisputed.json',
  'dof/views/accountInformationCycleToDate.json',
  'dof/views/accountInformationAccounts.json',
  'dof/views/accountInformationCardInformation.json',
  'dof/views/accountInformationHistory.json',
  'dof/views/accountInformationActivityHistory.json',

  'dof/fields/accountDetailStatementDetail.json',
  'dof/views/accountDetailStatementDetailInfo.json',
  'dof/views/accountDetailStatementDetailContent.json',
  'dof/fields/pendingAuthorization.json',
  'dof/views/pendingAuthorizationDetailInfo.json',
  'dof/views/pendingAuthorizationDetailMerchant.json',
  'dof/views/pendingAuthorizationDetailOther.json',
  'dof/fields/accountOverview.json',
  'dof/fields/accountSnapshot.json',
  'dof/fields/promiseToPay.json',
  'dof/fields/chronicleSearchForm.json',
  'dof/views/chronicleSearchForm.json',
  'dof/fields/addMemoCISForm.json',
  'dof/views/searchResultList.json',
  'dof/views/accountDetailSnapshotAccountInformation.json',
  'dof/views/accountDetailSnapshotHistoricalInformation.json',
  'dof/fields/hardshipForm.json',
  'dof/fields/hardshipDetail.json',
  'dof/views/hardshipDetail.json',
  'dof/views/hardshipForm.json',
  'dof/views/longTermHardshipForm.json',
  'dof/views/longTermHardshipDetail.json',
  'dof/views/longTermHardshipBasicDetail.json',

  'dof/fields/hardshipMilitaryDeploymentForm.json',
  'dof/views/hardshipMilitaryDeploymentForm.json',
  'dof/views/hardshipMilitaryDeploymentDetail.json',
  'dof/views/hardshipMilitaryDeploymentDetail1.json',

  'dof/fields/memoChronicle.json',
  'dof/views/addMemoChronicle.json',
  'dof/views/editMemoChronicle.json',
  'dof/views/addMemoCISForm.json',
  'dof/fields/accountDetailStatementOverview.json',
  'dof/views/accountDetailStatementOverview.json',

  'dof/views/deceasedPartyInformation.json',
  'dof/views/deceasedPartyMainInformation.json',

  'dof/fields/bankInfo.json',
  'dof/views/bankInfo.json',

  'dof/fields/addBankAccount.json',
  'dof/views/addBankAccountSaving.json',
  'dof/views/addBankAccountChecking.json',
  'dof/views/confirmAddBankAccount.json',
  'dof/views/addCardInfoView.json',
  'dof/views/confirmAddDebitAccount.json',

  'dof/views/editMemoCISForm.json',
  'dof/fields/paymentDetail.json',
  'dof/views/paymentDetail.json',
  'dof/fields/paymentHistoryDetail.json',
  'dof/views/paymentHistoryDetail.json',

  'dof/views/overviewInfomation.json',
  'dof/fields/bankruptcy.json',
  'dof/views/bankruptcy.json',

  'dof/fields/deceasedResults.json',
  'dof/fields/bankruptcyResults.json',
  'dof/views/bankruptcyAdditionInfo.json',
  'dof/views/bankruptcyMainInfo.json',
  'dof/views/bankruptcyFillingInfo.json',
  'dof/views/bankruptcyAdditionalAttorneyInfo.json',
  'dof/views/bankruptcyTrusteeInfo.json',
  'dof/views/bankruptcyClerkCourtInfo.json',
  'dof/views/deceasedSettlementInfo.json',
  'dof/views/deceasedPrimaryPartyInfo.json',
  'dof/views/deceasedSecondaryPartyInfo.json',

  'dof/fields/deceased.json',
  'dof/views/primaryDeceasedPartyMainInformation.json',
  'dof/views/secondaryDeceasedPartyMainInformation.json',
  'dof/views/bothDeceasedPartyMainInformation.json',
  'dof/views/primaryDeceasedAdditionalInformation.json',
  'dof/views/bothDeceasedAdditionalInformation.json',
  'dof/fields/achInformation.json',
  'dof/views/demandPaymentInformation.json',
  'dof/views/achPayment.json',
  'dof/views/settlement.json',
  'dof/views/deceasedParty.json',

  'dof/fields/promiseToPayHistory.json',
  'dof/views/ptpHistory.json',

  // card maintenance
  'dof/fields/cardholderMaintenanceInfo.json',
  'dof/views/cardholderMaintenanceInfo.json',
  'dof/fields/cardholderMaintenanceAddress.json',
  'dof/views/cardholderMaintenanceAddress.json',
  'dof/fields/cardholderMaintenanceStandardAddress.json',
  'dof/views/cardholderMaintenanceStandardAddress.json',
  'dof/fields/cardholderMaintenanceEmail.json',
  'dof/views/cardholderMaintenanceEmail.json',
  'dof/fields/cardholderMaintenanceTelephoneMore.json',
  'dof/views/cardholderMaintenanceTelephoneMore.json',
  'dof/fields/editPhone.json',
  'dof/views/editPhone.json',
  'dof/fields/editEmail.json',
  'dof/views/editEmail.json',

  // address standard
  'dof/fields/expandedAddress.json',
  'dof/views/expandedAddress.json',

  // phone address
  'dof/fields/clientPhone.json',
  'dof/views/clientPhone.json',
  'dof/views/clientAddress.json',

  // CCCS
  'dof/fields/cccs.json',
  'dof/views/cccsEnter.json',
  'dof/views/cccsInfo.json',
  'dof/fields/company.json',
  'dof/views/company.json',
  'dof/views/cccsSelectReason.json',

  // adjustment
  'dof/fields/adjustmentDetailOriginal.json',
  'dof/views/adjustmentDetailOriginal.json',
  'dof/fields/adjustTransaction.json',
  'dof/views/adjustTransaction.json',
  'dof/views/addMemoAdjustmentCIS.json',
  'dof/views/addMemoAdjustmentChronicle.json',

  // general information
  'dof/fields/generalInformation.json',
  'dof/views/generalInformation.json',
  'dof/views/generalInformationEditForm.json',

  // caller authentication
  'dof/fields/callerAuthentication.json',
  'dof/views/callerAuthentication.json',

  // Snapshot
  'dof/views/relatedAccountSnapshot.json',
  'dof/fields/relatedAccountList.json',
  'dof/views/relatedAccountList.json',

  // client configuration
  'dof/views/clientConfigTopInfo.json',
  'dof/fields/clientConfigTopInfo.json',
  'dof/views/clientConfigReasonCode.json',
  'dof/fields/clientConfigReasonCode.json',
  'dof/views/clientConfigCodeToText.json',
  'dof/fields/clientConfigCodeToText.json',
  'dof/views/clientConfigPromiseToPay.json',
  'dof/fields/clientConfigPromiseToPay.json',
  'dof/fields/clientConfigCallResult.json',
  'dof/views/clientConfigCallResult.json',
  'dof/fields/clientConfigCallResultInfo.json',
  'dof/views/clientConfigCallResultInfo.json',
  'dof/views/clientConfigCallResultEdit.json',
  'dof/views/clientConfigCallResultEditActionCodeType.json',
  'dof/fields/clientConfigDebtCompaniesForm.json',
  'dof/views/clientConfigDebtCompaniesForm.json',
  'dof/fields/entitlementSettingsSnapshot.json',
  'dof/views/entitlementSettingsSnapshot.json',
  'dof/views/clientConfigReference.json',
  'dof/fields/clientConfigReference.json',
  'dof/views/sendLetterConfig.json',
  'dof/views/frequencyConfig.json',
  'dof/fields/clientConfigMemos.json',
  'dof/views/clientConfigMemos.json',

  // incarceration
  'dof/views/incarcerationCallResults.json',
  'dof/fields/incarcerationCallResults.json',
  'dof/views/incarcerationForm.json',
  'dof/fields/incarcerationForm.json',

  // hardship
  'dof/views/hardshipNaturalDisaster.json',
  'dof/views/hardshipNaturalDetail1.json',
  'dof/views/hardshipNaturalDetail.json',
  'dof/fields/clientConfigAddAdjustment.json',
  'dof/views/clientConfigAddAdjustment.json',

  // collector productivity
  'dof/fields/productivityStatistics.json',
  'dof/views/productivityStatistics.json',
  'dof/fields/breakStatistics.json',
  'dof/views/breakStatistics.json',

  // exception handling
  'dof/fields/caseRecord.json',

  // custom add code reference
  'dof/fields/clientConfigReferenceCustom.json',
  'dof/views/clientConfigReferenceCustom.json',

  // settlement
  'dof/fields/settlementInformation.json',
  'dof/views/settlementInformation.json',

  'dof/fields/promiseToPayDetailSingle.json',
  'dof/views/promiseToPayDetailSingle.json',

  'dof/fields/promiseToPayDetailRecurring.json',
  'dof/views/promiseToPayDetailRecurring.json',

  'dof/fields/queueTypeInformation.json',
  'dof/views/queueTypeInformation.json',
  
];

const App: React.FC<AppProps> = props => {
  const [isLoadingConfig, setLoadingConfig] = useState<boolean>(true);

  const handleGetConfig = async () => {
    try {
      const mainUrls = ['config/main.json', 'config/envConfig.json'];
      const listPromises = mainUrls.map(item => {
        return axios.get(item);
      });

      const response = await Promise.all(listPromises);

      const [mainConfigResponse, envConfigResponse] = response;
      const mainConfig = mainConfigResponse.data as AppConfiguration;

      const { commonConfig, defaultMockAPI, defaultENV, orgList } =
        envConfigResponse.data as ENVConfig;

      const environments = Object.keys(commonConfig) as Array<ENVIRONMENT_TYPE>;

      const selectedEnv =
        environments.find(env => {
          const collectionsUrl = commonConfig[env]?.collectionsUrl;
          if (env === 'PROD' || env === 'CAT' || env === 'QA') {
            return collectionsUrl === window.location.hostname;
          }
          return new RegExp(window.location.hostname, 'i').test(collectionsUrl);
        }) || defaultENV;

      window.appConfig = {
        ...mainConfig,
        orgList,
        commonConfig: {
          ...commonConfig[selectedEnv],
          defaultMockAPI,
          selectedEnv
        }
      };
    } catch (e) {
      console.log(e);
    } finally {
      setLoadingConfig(false);
    }
  };

  // get dof url config
  const getUrlConfigs = () => {
    if (process.env.NODE_ENV === 'production') {
      return ['dof/dof.json'];
    }
    return urlConfigs;
  };

  useEffect(() => {
    handleGetConfig();
  }, []);

  useSaveSessionStorage();

  if (isLoadingConfig)
    return (
      <div
        className={classNames({
          loading: isLoadingConfig,
          'vh-100': isLoadingConfig
        })}
      ></div>
    );

  return (
    <DataApiContext.Provider value={refData}>
      <DOFApp
        customReducers={reducerMappingList}
        urlConfigs={getUrlConfigs()}
        customDataSource={DATA_SOURCE}
      >
        <ESSConfig />
      </DOFApp>
    </DataApiContext.Provider>
  );
};

export default App;
