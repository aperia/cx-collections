import React, { Fragment, useEffect } from 'react';

// components
import Header from 'pages/__commons/Header';
import ConfirmModal from 'pages/__commons/ConfirmModal';
import ToastNotifications from 'pages/__commons/ToastNotifications';
import SharedInfo from 'pages/__commons/SharedInfo';
import CallerAuthentication from 'pages/CallerAuthenticate';
import Entitlement from 'pages/__commons/Entitlement';
import UserSessionWarning from 'pages/UserSessionWarning';
import ApiErrorNotification from 'pages/ApiErrorNotification';
import { useDispatch, useSelector } from 'react-redux';
import { accessConfigActions } from 'pages/__commons/AccessConfig/_redux/reducer';
import { selectAccessConfigLoading } from 'pages/__commons/AccessConfig/_redux/selector';
import { isUndefined } from 'lodash';
import classNames from 'classnames';

export interface AppContainerProps {}

const AppContainer: React.FC<AppContainerProps> = () => {
  const dispatch = useDispatch();
  const isLoadingConfig = useSelector(selectAccessConfigLoading);

  useEffect(() => {
    dispatch(accessConfigActions.getAccessConfig());
  }, [dispatch]);

  if (isLoadingConfig || isUndefined(isLoadingConfig))
    return (
      <div
        className={classNames({
          loading: isLoadingConfig,
          'vh-100': isLoadingConfig
        })}
      ></div>
    );

  return (
    <Fragment>
      <Header />
      <Entitlement />
      <ConfirmModal />
      <ToastNotifications />
      <SharedInfo />
      <CallerAuthentication />
      <ApiErrorNotification />
      <UserSessionWarning />
    </Fragment>
  );
};

export default AppContainer;
