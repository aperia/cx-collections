# A guide to unit testing in Collections

---

## Run tests

- Run all tests

```
  npm run test
```

- Run all tests with coverage

```
  npm run test:coverage
```

- Run coverage with a file

```
  npm run test:coverage [path]
```

## Test Component

- To render component using `renderMockStoreId(component, { initialState = {}, mapping, middleware })`

```
  renderMockStoreId(<CollectionFormResults />, {
    initialState: generateInternalStatusCode(mockInternalStatusCode)
  });
```

- To spy an action using `mockActionCreator(actionObject)` and do not need to mock `dispatch()`

```
  const bankruptcyActionsSpy = mockActionCreator(bankruptcyActions);
```

- To wait component render using `jest.useFakeTimers()` and `jest.runAllTimers()`. Ex: component haves DropDown, Combobox, Modal.

```
  jest.useFakeTimers();
  renderMockStoreId(<CollectionFormResults />, {
    initialState: generateInternalStatusCode(mockInternalStatusCode)
  });
  jest.runAllTimers();
```

## Common Mock

- Mock HTMLCanvasElement

```
  HTMLCanvasElement.prototype.getContext = jest.fn();
```

- Mock View component

```
  jest.mock('app/_libraries/_dof/core/View', () =>
    jest.requireActual('app/test-utils/mocks/MockView')
  );
```

## Test Reducers

- This is a demo to test a reducers.

```
  import { actions, reducer } from './reducers';

  describe('Test A reducers', () => {
    const initialState = {};
    it('should call action demo', () => {
      const state = reducer(
        initialState,
        actions.demo({ storeId })
      );
      expect(state[storeId]).toEqual({ success: true });
    });
  })

```

## Test Selectors

- Use `selectorWrapper(initialState)(selector)`

```
  describe('Test selectData', () => {
    it('should return data and default data', () => {
      const { data, emptyData } = selectorWrapper(initialState)(
        selectData
      );
      expect(data).toEqual({ success: true });
      expect(emptyData).toBeNull();
    });
  });
```

## Test Async Thunk action

### Simple Async Thunk Action

- This is a simple async thunk action

```
  export const getData = createAsyncThunk<>('type', async (args,    thunkAPI) => {
    const { data } = await promiseToPayService.getPromiseToPayData(requestBody);

    return data;
  });
```

- This is the way testing.

```
  it('return data', async () => {
    const getData = jest
      .spyOn(service, 'getData')
      .mockResolvedValue({ ...responseDefault, data:    mockPromiseToPayData });

    const store = createStore(rootReducer, {
      mapping: {}
    });

    const response = await getData(mockRequestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(expectedValue);
  });
```

### Complicated Async Thunk Action

- This is an example:

```
  export const triggerGetAllData = createAsyncThunk<>('type', async (args,    thunkAPI) => {
    const result = await dispatch(getData());
    if (isFulfilled(result)) {
      // do something
    }
    if (isRejected(result)) {
      // do something
    }
    return data;
  });
```

- This is the way testing: using `byPassFulfilled(result)` or `byPassRejected(result)`

```
  const addToastActionSpy = toastSpy('addToast');
  await triggerGetAllData({})(
    byPassFulfilled(asyncThunk.fulfilled.type, {
      data: {
        messages: [{ status: 'success' }]
      }
    }),
    store.getState,
    {}
  );
  expect(addToastActionSpy).toBeCalled();
```

```
  const addToastActionSpy = toastSpy('addToast');
  await triggerGetAllData({})(
    byPassRejected(asyncThunk.rejected.type, {
      data: {
        messages: [{ status: 'success' }]
      }
    }),
    store.getState,
    {}
  );
  expect(addToastActionSpy).toBeCalled();
```
